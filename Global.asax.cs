﻿using BusinessServiceFramework;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingBusinessService.Models;
using ReportingDomainModel;
using ReportingWeb.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ReportingWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private Timer _timer;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;

            string JQueryVer = "1.7.1";
            System.Web.UI.ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new System.Web.UI.ScriptResourceDefinition
            {
                Path = "~/Scripts/jquery-" + JQueryVer + ".min.js",
                DebugPath = "~/Scripts/jquery-" + JQueryVer + ".js",
                CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".min.js",
                CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".js",
                CdnSupportsSecureConnection = true,
                LoadSuccessExpression = "window.jQuery"
            });
        }

        private DeveloperSettings GetDeveloperSettings()
        {
            string developerFilePath = HostingEnvironment.MapPath("~/App_Data/DeveloperSettings.xml");
            return CommonUtilities.GetObjectFromXml(developerFilePath, typeof(DeveloperSettings)) as DeveloperSettings;
        }

        private GeneralSettings GetGeneralSettings()
        {
            string generalFilePath = HostingEnvironment.MapPath("~/App_Data/GeneralSettings.xml");
            return CommonUtilities.GetObjectFromXml(generalFilePath, typeof(GeneralSettings)) as GeneralSettings;
        }

        private Info GetInfo()
        {
            string storeInfoFilePath = HostingEnvironment.MapPath("~/App_Data/Info.xml");
            return CommonUtilities.GetObjectFromXml(storeInfoFilePath, typeof(Info)) as Info;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
#if RELEASE
            try
            {
                bool isAjax = string.Equals("XMLHttpRequest", Context.Request.Headers["x-requested-with"], StringComparison.OrdinalIgnoreCase);

                if (!isAjax)
                {

                    Exception exception = Server.GetLastError();
                    Response.Clear();
                    Response.ContentType = "text/html";

                    HttpException httpException = exception as HttpException;

                    Response.TrySkipIisCustomErrors = true;

                    string errorCode = string.Empty;

                    if (httpException != null)
                    {

                        //BadRequest = 400,
                        //Unauthorized = 401,
                        //Forbidden = 403,
                        //NotFound = 404,
                        //Conflict = 409,
                        //InternalServerError = 500,
                        //ServiceUnavailable = 503

                        switch (httpException.GetHttpCode())
                        {
                            case 400:
                                errorCode = HttpErrorCode.BadRequest.ToString();
                                break;
                            case 401:
                                errorCode = HttpErrorCode.Unauthorized.ToString();
                                break;
                            case 403:
                                errorCode = HttpErrorCode.Forbidden.ToString();
                                break;
                            case 404:
                                errorCode = HttpErrorCode.NotFound.ToString();
                                break;
                            case 409:
                                errorCode = HttpErrorCode.Conflict.ToString();
                                break;
                            case 500:
                                errorCode = HttpErrorCode.InternalServerError.ToString();
                                break;
                            case 503:
                                errorCode = HttpErrorCode.ServiceUnavailable.ToString();
                                break;
                            default:

                                break;
                        }

                    }
                    else
                    {
                        this.SendEmailAdmin(exception.ToString());
                    }

                    // clear error on server
                    Server.ClearError();

                    if (!string.IsNullOrEmpty(exception.Message))
                    {
                        if (string.IsNullOrEmpty(errorCode))
                        {
                            errorCode = " ";
                        }

                        byte[] plainParamBytes = Encoding.UTF8.GetBytes(errorCode + "||" + Server.UrlEncode(exception.Message));
                        string encryptParam = Convert.ToBase64String(plainParamBytes);

                        RouteData routeData = new RouteData();
                        routeData.Values.Add("controller", "Error");
                        routeData.Values.Add("action", "Index");

                        routeData.Values.Add("value", encryptParam);

                        IController controller = new ErrorController();

                        var httpContext = new HttpContextWrapper(Context);
                        //httpContext.Response.Clear();

                        controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
                    }
                }
            }
            catch (Exception err)
            {
                throw err;
            }

#endif
        }

        private void SendEmailAdmin(string msg)
        {
            DeveloperSettings devSettings = this.GetDeveloperSettings();
            GeneralSettings generalSettings = this.GetGeneralSettings();
            Info info = this.GetInfo();

            string title = info.Name + " - " + FrameworkResources.Error;

            string path = info.Theme.ToLower() + "_error_template.html";
            path = Server.MapPath("~/TemplateFiles/" + path);

            StreamReader reader = new StreamReader(path);
            string template = reader.ReadToEnd();

            template = template.Replace("[FullName]", devSettings.AdminEmail);
            template = template.Replace("[Message]", msg);

            List<string> emailList = new List<string>();
            emailList.Add("jade.notifications@gmail.com");

            new Mailer(devSettings.SMTPServerType, devSettings, 0).SendEmailAsync(generalSettings.SenderEmail, emailList, title, template, true, string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, new UrlHelper(HttpContext.Current.Request.RequestContext, RouteTable.Routes).Content("~")), this.GetInfo(), this.GetDeveloperSettings());

        }
    }
}
