﻿using ReportingBusinessService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Helpers
{
    public static class DisplayExtensions
    {
        private static string currencyFormat = "#,##0;(#,##0);0";
        private static string shortDateFormat = "dd MMMM yyyy";
        private static string longDateFormat = "dd MMMM yyyy HH:mm";
        private static string timeFormat = "HH:mm";
        private static string serializationDateFormat = "dd/MM/yyyy";

        // Currency
        public static IHtmlString CurrencyJustify(this HtmlHelper helper, decimal value)
        {
            Info storeInfo = helper.ViewContext.Controller.ViewBag.Info;
            currencyFormat = helper.ViewContext.Controller.ViewBag.DeveloperSettings.CurrencyFormat ?? currencyFormat;

            return new MvcHtmlString(String.Format("<span class=\"pull-left\">{0}</span><span class=\"pull-right\">{1}</span>", storeInfo.CurrencySymbol, value.ToString(currencyFormat)));
        }


        public static IHtmlString Currency(this HtmlHelper helper, decimal value)
        {
            Info storeInfo = helper.ViewContext.Controller.ViewBag.Info;
            currencyFormat = helper.ViewContext.Controller.ViewBag.DeveloperSettings.CurrencyFormat ?? currencyFormat;


            return new MvcHtmlString(String.Format("{0} {1}", storeInfo.CurrencySymbol,
                value.ToString(currencyFormat)));
        }

        // Point
        public static IHtmlString Point(this HtmlHelper helper, decimal value)
        {
            currencyFormat = helper.ViewContext.Controller.ViewBag.DeveloperSettings.CurrencyFormat ?? currencyFormat;

            return new MvcHtmlString(String.Format("{0}",
                value.ToString(currencyFormat)));
        }

        // Percentage
        public static IHtmlString Percentage(this HtmlHelper helper, decimal value)
        {
            return new MvcHtmlString(String.Format("{0}",
                value.ToString("P0")));
        }

        // ShortDateTime
        public static IHtmlString ShortDate(this HtmlHelper helper, DateTime value)
        {
            shortDateFormat = helper.ViewContext.Controller.ViewBag.DeveloperSettings.ShortDateFormat ?? shortDateFormat;

            return new MvcHtmlString(String.Format("{0}",
                value.ToString(shortDateFormat)));
        }

        // LongDateTime
        public static IHtmlString LongDate(this HtmlHelper helper, DateTime value)
        {
            longDateFormat = helper.ViewContext.Controller.ViewBag.DeveloperSettings.LongDateFormat ?? longDateFormat;

            return new MvcHtmlString(String.Format("{0}",
                value.ToString(longDateFormat)));
        }

        public static IHtmlString LongDateAlt(this HtmlHelper helper, DateTime value)
        {
            return new MvcHtmlString(String.Format("{0}",
                value.ToString("dd MMM yyyy HH:mm")));
        }

        public static IHtmlString Time(this HtmlHelper helper, DateTime value)
        {
            return new MvcHtmlString(String.Format("{0}",
                value.ToString(timeFormat)));
        }

        //NumericShortDate
        public static IHtmlString NumericShortDate(this HtmlHelper helper, DateTime value)
        {
            serializationDateFormat = helper.ViewContext.Controller.ViewBag.DeveloperSettings.SerializationDateFormat ?? serializationDateFormat;

            return new MvcHtmlString(String.Format("{0}",
                value.ToString(serializationDateFormat)));
        }
    }
}