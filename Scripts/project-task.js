﻿$(document).on("ready", function () {
    onParentTaskAutoComplete();
    onProjectAutoComplete();
});

function onParentTaskAutoComplete() {

    $("#ParentName").autocomplete({
        source: function (request, response) {
            
            $('#ParentTaskID').val('');

            $.getJSON(url_parent_tasks, {
                term: request.term
            }, function (data) {

                if (data.length == 0) {
                    data = [];
                    data.push({
                        itemID: '',
                        itemName: '',
                    });
                }

                response($.map(data, function (el) {
                    return {
                        value: el.itemName,
                        label: el.itemName,
                        id: el.itemID,
                        name: el.itemName,
                    };
                }));
            });
        },
        minLength: 0,
        select: function (event, ui) {
            $('#ParentTaskID').val(ui.item.id);
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {

        var liElem = $('<li>')
            .attr('taskID', item.id);

        if (item.id > 0) {
            $(liElem).append('<a>' + item.name + '</a>');
        }
        else {
            $(liElem).addClass("ui-state-disabled").css('text-align', 'center').append(generalResources.EmptyData);
        }

        return $(liElem).appendTo(ul);
    };
}

function onProjectAutoComplete() {

    $("#ProjectName").autocomplete({
        source: function (request, response) {

            $('#ProjectID').val('');

            $.getJSON(url_projects, {
                term: request.term
            }, function (data) {

                if (data.length == 0) {
                    data = [];
                    data.push({
                        itemID: '',
                        itemName: '',
                    });
                }

                response($.map(data, function (el) {
                    return {
                        value: el.itemName,
                        label: el.itemName,
                        id: el.itemID,
                        name: el.itemName,
                    };
                }));
            });
        },
        minLength: 0,
        select: function (event, ui) {
            $('#ProjectID').val(ui.item.id);
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {

        var liElem = $('<li>')
            .attr('projectID', item.id);

        if (item.id > 0) {
            $(liElem).append('<a>' + item.name + '</a>');
        }
        else {
            $(liElem).addClass("ui-state-disabled").css('text-align', 'center').append(generalResources.EmptyData);
        }

        return $(liElem).appendTo(ul);
    };
}


