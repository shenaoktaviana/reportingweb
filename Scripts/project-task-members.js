﻿$(document).on("ready", function () {
    $(document).on('click', '.button_approve', function () {
        onApproveButtonClicked($(this))
    });
    $(document).on('click', '.button_reject', function () {
        onRejectButtonClicked($(this))
    });
});

function onApproveButtonClicked(element) {
    var data = {
        itemID: element.attr('id'),
        statusTask: "APPROVED",
        reason: null
    };
    
    var submitButtonText = customResources.ConfirmApprove;
    $('.delete-confirmation').text(submitButtonText);

    dialog.init({
        type: 'dialog',
        dialogSize: 'modal-md',
        headerText: customResources.Approve,
        submitButtonText: customResources.Approve,
        contentId: 'modal-delete-confirmation',
        onSubmitButtonClicked: function () {


            $('.loader:first').off('ajaxSuccess');
            $('.loader:first').off('ajaxFail');

            $('.loader:first').trigger('loadAjax', [url_approveReject, data]);

            $('.loader:first').on('ajaxSuccess', function (event, result) {

                $("#div-alert-success").children().not(".glyphicon").remove();
                $("#div-alert-error").children().not(".glyphicon").remove();

                $(".alert-success").addClass('hidden');
                $(".alert-danger").addClass('hidden');

                if (result.status == "OK") {
                    if ($('.table.table-striped tobody tr').length == 1 && prev_paging_page > 1) {
                        getData(prev_paging_page - 1);
                    }
                    else {
                        getData(prev_paging_page);
                    }

                    initRowAction();

                    $(".alert-success").removeClass('hidden');
                    $("#div-alert-success").append("<span>" + result.message + "</span>");
                }
                else {
                    $(".alert-danger").removeClass('hidden');
                    $("#div-alert-error").append("<span>" + result.message + "</span>");
                }
            });

            $('.loader:first').on('ajaxFail', function (event, data) {
                alert(data);
            });

            bindCustomPostEvent();
        }
    });
    dialog.show();
}

function onRejectButtonClicked(element) {
        var submitButtonText = customResources.ConfirmReject;
        $('.reject-confirmation').text(submitButtonText);

        var data = {
            itemID: element.attr('id'),
            statusTask: "REJECTED",
            reason: null
        };

        dialog.init({
            type: 'dialog',
            dialogSize: 'modal-md',
            headerText: customResources.Reject,
            submitButtonText: customResources.Reject,
            contentId: 'modal-reject-confirmation',
            onSubmitButtonClicked: function () {

                var reason = $('.reject-reason').val();
                data.reason = reason;

                $('.loader:first').off('ajaxSuccess');
                $('.loader:first').off('ajaxFail');

                $('.loader:first').trigger('loadAjax', [url_approveReject, data]);

                $('.loader:first').on('ajaxSuccess', function (event, result) {

                    $("#div-alert-success").children().not(".glyphicon").remove();
                    $("#div-alert-error").children().not(".glyphicon").remove();

                    $(".alert-success").addClass('hidden');
                    $(".alert-danger").addClass('hidden');

                    if (result.status == "OK") {
                        if ($('.table.table-striped tobody tr').length == 1 && prev_paging_page > 1) {
                            getData(prev_paging_page - 1);
                        }
                        else {
                            getData(prev_paging_page);
                        }

                        initRowAction();

                        $(".alert-success").removeClass('hidden');
                        $("#div-alert-success").append("<span>" + result.message + "</span>");
                    }
                    else {
                        $(".alert-danger").removeClass('hidden');
                        $("#div-alert-error").append("<span>" + result.message + "</span>");
                    }
                });

                $('.loader:first').on('ajaxFail', function (event, data) {
                    alert(data);
                });

                bindCustomPostEvent();
            }
        });
        dialog.show();
}
