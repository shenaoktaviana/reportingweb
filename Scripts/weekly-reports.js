﻿var selectedUsers = [];
var userTerm = "";

$(document).on("ready", function () {
    initDatePickerFields();
    onMultipleDropdownAutoComplete();
    onMultipleDropdownChanged();
    onPreviewClicked();
    onPrintClicked();
    onResetClicked();
    $(".filter-button").addClass("hidden");
    $("#search-container").removeClass("collapse");
});

function initDatePickerFields() {
    $("#DateToDate").prop("disabled", "disabled");

    var dateNow = new Date();
    var minDate = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate());

    $("#DateFromDate").datepicker({
        dateFormat: shortDateFormat,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        onClose: function (dateText, inst) {

            try {
                formatDate($(this).val());

                var fromDateValue = $(this).datepicker("getDate");
                fromDateValue.setDate(fromDateValue.getDate() + 4);
                $("#DateToDate").datepicker('setDate', fromDateValue);

            } catch (e) {
                setDefaultDatePicker($(this));
            }
        }
    });

    $("#DateToDate").datepicker({
        dateFormat: shortDateFormat,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        onClose: function (dateText, inst) {

            try {
                formatDate($(this).val());
            } catch (e) {
                setDefaultDatePicker($(this));
            }
        }
    });
}

function onMultipleDropdownAutoComplete() {
    onUserAutoComplete();
}

function onUserAutoComplete() {

    $("#user_dropdown_chosen .chosen-search-input").autocomplete({
        source: function (request, response) {
            $.getJSON(url_get_users, {
                value: request.term,
                selectedData: selectedUsers.join(',')
            }, function (data) {
                userTerm = request.term;
                if (data.length == 0) {
                    data = [];
                    data.push({
                        value: '',
                        label: '',
                        userID: 0
                    });
                }

                $("#user-dropdown").find('option').not(':selected').remove();

                response($.map(data, function (el) {
                    return {
                        value: el.Name,
                        label: el.Name,
                        userID: el.UserID
                    };
                }));

            });
        },
        minLength: 0,
        select: function (event, ui) {

        }
    }).autocomplete("instance")._renderItem = function (ul, item) {

        var listElement = $('<option></option>').attr({
            "value": item.userID
        }).html(item.label);

        if (item.userID > 0) {
            $("#user-dropdown").append(listElement);
            $("#user-dropdown").trigger("chosen:updated")
        }

        return $("#user_dropdown_chosen .chosen-search-input").val(userTerm);;
    };

}

function onMultipleDropdownChanged() {
    $("#user-dropdown").on("change", function () {
        selectedUsers = [];

        $("#user-dropdown").find('option').each(function () {
            if ($(this).prop("selected")) {
                $(this).attr("selected", "");
                selectedUsers.push(parseInt($(this).attr("value")));
            }
        });

        if (selectedUsers.length > 1) {
            $('#all-checkbox-item').prop("disabled", "");
        } else {
            $('#all-checkbox-item').prop("disabled", "disabled");
            $('#all-checkbox-item').prop("checked", "");
        }

    });
}

function onPreviewClicked() {
    $("#button-preview").on("click", function () {
        generateReport(false);
    });
}

function generateReport(isPrint) {

    hideAlertBoxes();
    $('.report-container *').remove();

    var startDateValue = $("#DateFromDate").datepicker("getDate");
    var endDateValue = $("#DateToDate").datepicker("getDate");

    var startDate = null;
    var endDate = null;

    if (startDateValue != null) {
        startDate = startDateValue.getFullYear() + "-" + (startDateValue.getMonth() + 1) + "-" + startDateValue.getDate();
    }

    if (endDateValue != null) {
        endDate = endDateValue.getFullYear() + "-" + (endDateValue.getMonth() + 1) + "-" + endDateValue.getDate();
    }

    var data = {
        startDate: startDate,
        endDate: endDate,
        selectedUsers: selectedUsers.join(','),
        isPrint: isPrint,
        type: reportType
    };

    $('.loader:first').trigger('loadAjax', [url_get_preview, data]);

    $('.loader:first').off("ajaxSuccess");
    $('.loader:first').on('ajaxSuccess', function (event, result) {

        if (result.Status == "success") {

            showMessageOnAlertBox(result.Message, $(".alert#div-alert-success"));

            var path = result.Data;

            var w = window.open(path, "_blank");
            w.focus();

            if (isPrint) {
                w.print();
            }
        }
        else {
            showMessageOnAlertBox(result.Message, $(".alert#div-alert-error"));
        }

    });

    $('.loader:first').off("ajaxFail");
    $('.loader:first').on('ajaxFail', function (event, data) {
        processCommonAjaxError(data);
    });

    bindCustomPostEvent();
}

function onPrintClicked() {
    $("#button-print").on("click", function () {
        generateReport(true);
    });
}

function onResetClicked() {
    $("#button-reset-report").on("click", function () {
        hideAlertBoxes();
        $('.report-container *').remove();
        selectedUsers = [];

        $(".div-search").find('input[type="text"]').each(function () {
            $(this).val("");
        });

        $(".div-search").find('select').each(function () {
            $(this).find('option').remove();
            $(this).trigger("chosen:updated");
        });
    });
}