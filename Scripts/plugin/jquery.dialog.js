﻿(function ($) {

    function dialogSettings() {
        this.dialogSize = "normal";
        this.type = "type";
        this.headerText = "";
        this.contentId = "";
        this.submitButtonText = "";
        this.onBeforeModalShown = function () { };
        this.onAfterModalShown = function () { };
        this.onBeforeModalHidden = function () { };
        this.onModalHidden = function () { };
        this.onSubmitButtonClicked = function () { };
        this.isSubmitting = false;
        this.keyboard = true;
    }

    $.fn.dialogControl = function (options) {
        if (this.length == 0) {
            return this;
        }
        var settings = new dialogSettings();
        var el = this;
        el.settings = settings; 

        var init = function (options) {

            if (options)
                settings = $.extend(settings, options);

            switch (settings.type) {
                case "dialog":
                    $(".modal-header").removeClass("hide");
                    $("#modal-title").html(settings.headerText);
                    $(".modal-footer").removeClass("hide");
                    $("#modal-loading").addClass("hide");
                    $("#modal-content").removeClass("hide");
                    $("#modal-content").html($("#" + settings.contentId).html());

                    if (settings.submitButtonText != "")
                        $("#btn-submit").html(settings.submitButtonText);

                    $("#btn-submit").off("click");
                    $("#btn-submit").on("click", function () {
                        settings.isSubmitting = true;
                        settings.onSubmitButtonClicked();
                        $(this).modal('hide');
                        //settings.isSubmitting = false;
                    });

                    if (settings.dialogSize != "normal") {
                        $(".modal-dialog").addClass(settings.dialogSize);
                    }
                    else {
                        $(".modal-dialog").removeClass();
                        $('#modal div[role="document"]').addClass('modal-dialog');
                    }
                    break;

                case "custom":
                    $("#modal-content").html($("#" + settings.contentId).html());
                    break;

                case "loader":
                default:
                    $(".modal-header").addClass("hide");
                    $(".modal-footer").addClass("hide");
                    $("#modal-loading").removeClass("hide");
                    $("#modal-content").addClass("hide");

                    if (settings.dialogSize != "normal") {
                        $(".modal-dialog").addClass(settings.dialogSize);
                    }
                    break;
            }
        };

        el.init = function (options) {
            init(options);
        };

        el.show = function (options) {
            if (options)
                init(options);

            if (settings.onBeforeModalShown)
                settings.onBeforeModalShown();

            if ($(this).data('bs.modal') != undefined) {
                var mdl = $(this).data('bs.modal');
                mdl.options.keyboard = settings.keyboard;
                mdl.escape();
            }

            $(this).modal({ "backdrop": "static", "keyboard": settings.keyboard });

            if (settings.onAfterModalShown)
            {
                $(this).on("shown.bs.modal", function () {
                    settings.onAfterModalShown();
                });
            }
        };
        
        el.ajaxHide = function () {

            this.hideInnerLoader();

            if (settings && settings.type == "dialog") {
                if (settings.isSubmitting) {
                    this.hide();
                }
            }
            else {
                this.hide();
            }
        };

        el.hide = function () {
            $(this).modal('hide');
        };

        el.showInnerLoader = function () {
            $("#modal-inner-loader").removeClass("hide");
        };

        el.hideInnerLoader = function () {
            $("#modal-inner-loader").addClass("hide");
        };

        el.reset = function () {
            settings.dialogSize = "normal";
            settings.type = "type";
            settings.headerText = "";
            settings.contentId = "";
            settings.submitButtonText = "";
            settings.onBeforeModalShown = function () { };
            settings.onAfterModalShown = function () { };
            settings.onBeforeModalHidden = function () { };
            settings.onModalHidden = function () { };
            settings.onSubmitButtonClicked = function () { };
            settings.isSubmitting = false;
            settings.keyboard = true;
        }

        init();

        return this;
    };

})(jQuery);

$(document).ready(function () {
    $('#modal').on('hide.bs.modal', function (e) {
        if (!dialog.settings.isSubmitting) {
            dialog.settings.onBeforeModalHidden(e);
        }
    });

    $('#modal').on('hidden.bs.modal', function () {
        dialog.settings.isSubmitting = false;
        dialog.settings.type = "loader"; //reset type
        dialog.settings.onModalHidden();
    });
});