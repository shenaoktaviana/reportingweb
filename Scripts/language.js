﻿$(document).on("ready", function () {
    initFlagIcon();
})

function initFlagIcon() {
    $('#Culture').on("change",function () {
        var iconImg = $('#Icon').siblings("div");
        $(iconImg).removeClass();

        var nation = $('#Culture').val().split('-').pop().toLowerCase();
        var language = $('#Culture').val().split('-').shift().toLowerCase();

        $(iconImg).addClass('img-responsive icon-uploaded flag flag-' + nation);
        $('#Icon').val(nation);
    });
}