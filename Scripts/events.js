﻿function getData(page) {
    var filters = [];
    var sort = "LatestInserted";
    var data = {
        page: parseInt(page),
        filters: filters,
        sort: sort
    };

    $('.loader:first').trigger('loadAjax', [url_data, data]);
    $('.loader:first').off("ajaxSuccess");
    $('.loader:first').on('ajaxSuccess', function (event, result) {

        var refreshPaging = false;
        total_row = result.Total;

        if (total_row != pagingObj.number) {
            pagingObj.setNumber(total_row);
            refreshPaging = true;
        }

        prev_paging_page = parseInt(page);
        pagingObj.setPage(refreshPaging ? null : prev_paging_page);
        
        if (total_row == 0) {
            var divNoData = generalResources.EmptyData;
            $('.event-col').html(divNoData);
        }
        else {
            $.each(result.Data, function (i, obj) {
                $('.event-col').html(obj);
            });
        }
    });
    $('.loader:first').off("ajaxFail");
    $('.loader:first').on('ajaxFail', function (event, data) {
        processCommonAjaxError(data);
    });

    bindCustomPostEvent();
    $(window).scrollTop(0);
}