﻿$(document).ready(function () {
    initDatePickerFields();
    initInsertButton();
    initExportButton();
    initSearchButton();
    initResetButton();
    initSortDropdown();
    initActiveControls();
    initSubDataCollapsed();
    initFilterData();
    initUpgradePlanButton();
});

function initUpgradePlanButton() {
    $('#button-upgradeplan').click(function () {
        window.location.href = url_upgradeplan;
    });
}

function initFilterData() {
    $(".filter-button").on('click', function () {
        var icon = $(this).find(".glyphicon");

        if ($(icon).hasClass("glyphicon-chevron-down")) {
            $(icon).removeClass("glyphicon-chevron-down");
            $(icon).addClass("glyphicon-chevron-up");
        }
        else if ($(icon).hasClass("glyphicon-chevron-up")) {
            $(icon).addClass("glyphicon-chevron-down");
            $(icon).removeClass("glyphicon-chevron-up");
        }
    });
}

function initSubDataCollapsed() {

    //$("[data-toggle='toggle']").bootstrapToggle('destroy')
    //$("[data-toggle='toggle']").bootstrapToggle();

    $('.sub-data-collapse').each(function (i, obj) {
        //var item = $(this);
        var id = $(obj).attr("id");
        var link = $('a[href="#' + id + '"]');

        $(obj).on('show.bs.collapse', function () {

            console.log("Clicked ID: " + id);

            hideShowCollapseIcon(link);
        });

        $(obj).on('hidden.bs.collapse', function () {

            console.log("Clicked ID: " + id);

            hideShowCollapseIcon(link);
            $(".container.body-content").css({ "height": "" });
        });
    });
}

function hideShowCollapseIcon(link) {
    $(link).find('img').each(function () {
        var icon = $(this);

        if ($(icon).hasClass("hidden")) {
            $(icon).removeClass("hidden");
        }
        else {
            $(icon).addClass("hidden");
        }

    });
}

function initInsertButton() {
    $('#button-insert').click(function () {
        window.location.href = url_insert;
    });
}

function initExportButton() {
    $('#button-export').click(function () {

        hideAlertBoxes();

        var filters = [];

        var textInfo = "";

        $.each(modelFilters, function (i, filter) {
            var options = getFilters(filter, textInfo);
            if (options[0] != null)
                filters.push(options[0]);
        });

        var data = {
            filters: filters,
        };

        $('.loader:first').off('ajaxSuccess');
        $('.loader:first').off('ajaxFail');

        $('.loader:first').trigger('loadAjax', [url_export, data]);

        $('.loader:first').on('ajaxSuccess', function (event, result) {

            if (result.Status == "success") {

                showMessageOnAlertBox(result.Message, $(".alert#div-alert-success"));

                var path = result.Data;
                //var w = window.download(path, "_blank");
                var w = window.open(path, "_blank");
                //w.focus();

                var data = {
                    filePath: path
                };

                $('.loader:first').off('ajaxSuccess');
                $('.loader:first').off('ajaxFail');
                $('.loader:first').trigger('loadAjax', [url_delete_export, data]);
            }
            else {
                showMessageOnAlertBox(result.Message, $(".alert#div-alert-error"));
            }
        });

        $('.loader:first').on('ajaxFail', function (event, data) {
            alert(data);
        });


        bindCustomPostEvent();
    });
}

function initSortDropdown() {
    $('#Sort').change(function () {
        prev_paging_page = 1;
        getData(1);
    });
}

function initSearchButton() {
    $('#button-search').on("customClick", function () {

        $.each(modelFilters, function (i, filter) {
            if (filter.ControlType == 4) {
                $('#' + filter.Name + 'from-search').val($('#' + filter.Name + "FromDate").val());
                $('#' + filter.Name + 'to-search').val($('#' + filter.Name + "ToDate").val());
            }
            else {
                $('#' + filter.Name + '-search').val($('#' + filter.Name).val());
            }
        });

        prev_paging_page = 1;

        getData(1);
    });
}

function initResetButton() {
    $('#button-reset').on("customClick", function () {

        $.each(modelFilters, function (i, filter) {

            switch (filter.ControlType) {
                case 4:
                    $('#' + filter.Name + 'from-search').val('');
                    $('#' + filter.Name + "FromDate").val('');
                    $('#' + filter.Name + 'to-search').val('');
                    $('#' + filter.Name + "ToDate").val('');
                    break;

                case 3:
                    $('#' + filter.Name + '-search').val('');
                    $('#' + filter.Name).removeAttr('selected');
                    $('#' + filter.Name + ' option:selected').removeAttr('selected');
                    break;

                default:
                    $('#' + filter.Name + '-search').val('');
                    $('#' + filter.Name).val('');
                    $('select[id*="-custom"] option').removeAttr('selected');
                    $('input[id*="-custom-search"]').val('');
                    break;
            }
        });

        prev_paging_page = 1;

        getData(1);
    });
}

function initDatePickerFields() {
    var dateNow = new Date();
    var minDate = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate());

    $.each(modelFilters, function (i, filter) {

        switch (filter.ControlType) {
            case 2:
                $("#" + filter.Name).datepicker({
                    dateFormat: shortDateFormat,
                    changeMonth: true,
                    changeYear: true,
                    date: minDate,
                    onClose: function (dateText, inst) {

                        try {
                            formatDate($(this).val());

                        } catch (e) {
                            setDefaultDatePicker($(this));
                        }
                    }
                });
                break;

            case 4:
                $("#" + filter.Name + "FromDate").datepicker({
                    dateFormat: shortDateFormat,
                    changeMonth: true,
                    changeYear: true,
                    date: minDate,
                    onClose: function (dateText, inst) {

                        try {
                            formatDate($(this).val());

                            var value = $(this).datepicker("getDate");
                            var endDateValue = $("#" + filter.Name + "ToDate").datepicker("getDate");

                            if (value > endDateValue && endDateValue != null) {
                                setDefaultDatePicker($(this));
                            }

                        } catch (e) {
                            setDefaultDatePicker($(this));
                        }
                    }
                });

                $("#" + filter.Name + "ToDate").datepicker({
                    dateFormat: shortDateFormat,
                    changeMonth: true,
                    changeYear: true,
                    onClose: function (dateText, inst) {

                        try {
                            formatDate($(this).val());

                            var value = $(this).datepicker("getDate");
                            var startDateValue = $("#" + filter.Name + "FromDate").datepicker("getDate");

                            if (value < startDateValue && startDateValue != null) {
                                setDefaultDatePicker($(this));
                            }
                        } catch (e) {
                            setDefaultDatePicker($(this));
                        }
                    }
                });
                break;

            case 5:
                $("#" + filter.Name).on('keydown', function () { $("#" + filter.Name + "-search").val(0); });
                $("#" + filter.Name).autocomplete({
                    source: function (request, response) {
                        $.getJSON(filter.AutoCompleteURL, {
                            name: request.term
                        }, function (data) {
                            if (data.length == 0) {
                                data = [];
                                data.push({
                                    value: 0,
                                    label: '',
                                    itemId: 0,
                                    itemName: '',
                                });
                            }
                            response($.map(data, function (el) {
                                return {
                                    value: el.itemName,
                                    label: el.itemName,
                                    itemId: el.itemId,
                                    itemName: el.itemName,
                                };
                            }));
                        });
                    },
                    minLength: 2,
                    select: function (event, ui) {
                        $("#" + filter.Name + "-search").val(ui.item.itemId);
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    var liElem = $('<li>').attr('itemId', item.itemId);

                    if (item.itemId > 0) {
                        $(liElem).append('<a>' + item.itemName + '</a>');
                    }
                    else {
                        $(liElem).addClass("disabled").css('text-align', 'center').append(generalResources.NoDataAvailable);
                    }

                    return $(liElem).appendTo(ul);
                };

                break;
        }
    });
}

function getData(page) {
    hideAlertBoxes();
    var filters = [];
    var textInfo = "";

    $.each(modelFilters, function (i, filter) {

        var operator = "";

        switch (filter.FilterOption.Operator) {
            case 1:
                operator = generalResources.GreaterThan;
                break;

            case 2:
                operator = generalResources.Operator;
                break;

            case 3:
                operator = generalResources.Contains;
                break;

            case 4:
            default:
                operator = generalResources.Equal;
                break;

            case 5:
                operator = generalResources.NotEqual;
                break;
        }

        var options = getFilters(filter, operator, textInfo);

        if (options[0] != null)
            filters.push(options[0]);

        if (options[1] != null && options[1] != "")
            textInfo = options[1];

    });

    var sort = $('#Sort option:selected').val();
    if (!$.isEmptyObject(sort) && sort != '') {
        textInfo += ((!textInfo || 0 === textInfo.length) ? generalResources.SortedByOnly : ' ' + generalResources.SortedBy) + ' ' + $('#Sort option:selected').text();
    }

    var data = {
        page: parseInt(page),
        filters: filters,
        sort: sort,
        indexID: $("#index-id").val()
    };

    $('.loader:first').off('ajaxSuccess');
    $('.loader:first').off('ajaxFail');

    $('.loader:first').trigger('loadAjax', [url, data]);

    $('.loader:first').on('ajaxSuccess', function (event, result) {
        dialog.hide();
        total_row = result.Total;

        var refreshPaging = false;

        if (total_row != pagingObj.number) {
            pagingObj.setNumber(total_row);
            refreshPaging = true;
        }

        prev_paging_page = parseInt(page);
        pagingObj.setPage(refreshPaging ? null : prev_paging_page);

        $('.div-list tbody').children().remove();

        if (total_row == 0) {
            var cell = "<tr><td colspan=\"" + columnCount + "\" class=\"form-group text-center div-nodata\">" + generalResources.EmptyData + "</td></tr>";
            $('.div-list tbody').html(cell);

            $("#total-description").html(new String().format(generalResources.TotalDescription, 0, 0));
        }
        else {
            $.each(result.Data, function (i, obj) {
                $('.div-list > .table-responsive > table > tbody').append(obj);
            });

            var cnt = result.Data.length < adminPageSize ? result.Data.length : adminPageSize;
            $("#total-description").html(new String().format(generalResources.TotalDescription, cnt, result.Total));

            initActiveControls();
        }

        $(':checkbox:checked').each(function (i, obj) {
            obj.checked = false;
        });

        $.each(buffer_selected_data_id, function (i, obj) {
            $('tr[itemid="' + obj + '"] input[name^="checkbox-item-"]').prop('checked', true);
        });

        if (textInfo.length > 0) {

            showMessageOnAlertBox(textInfo, $(".alert#div-alert-info"));
        }

        window.scrollTo(0, 0);

        if (typeof (initRowActions) != "undefined")
            initRowActions(result.Data);

        initSubDataCollapsed();
    });

    $('.loader:first').on('ajaxFail', function (event, data) {
        alert(data);
    });

    bindCustomPostEvent();
}

function getFilters(filter, operator, textInfo) {
    var result;
    var unison = filter.FilterOption.Unison == "1" ? generalResources.And : generalResources.Or;

    switch (filter.ControlType) {
        case 4:
            var filterFrom = $('#' + filter.Name + 'from-search').val();
            var filterTo = $('#' + filter.Name + 'to-search').val();

            if ($.trim(filterFrom) != '' || $.trim(filterTo) != '') {

                result = {
                    Type: filter.FilterOption.Type,
                    Label: filter.FilterOption.Label,
                    Operator: filter.FilterOption.Operator,
                    Unison: filter.FilterOption.Unison,
                    Value: serializationDateFormat + '::' + filterFrom + '::' + filterTo
                };
            }

            if ($.trim(filterFrom) != '') {
                textInfo += ((!textInfo || 0 === textInfo.length) ? generalResources.FilteredBy : ' ' + unison) + ' ' + filter.Text + ' ' + generalResources.From + ' ' + filterFrom;
            }

            if ($.trim(filterTo) != '') {
                textInfo += ((!textInfo || 0 === textInfo.length) ? generalResources.FilteredBy : ($.trim(filterFrom) != '' ? "" : ' ' + unison + ' ' + filter.Text) + ' ' + generalResources.To + ' ' + filterTo);
            }

            break;

        case 2:
            var date = $('#' + filter.Name + '-search').val();

            if (date != "") {
                result = {
                    Type: filter.FilterOption.Type,
                    Label: filter.FilterOption.Label,
                    Operator: filter.FilterOption.Operator,
                    Unison: filter.FilterOption.Unison,
                    Value: serializationDateFormat + "::" + date,
                };

                if ($.trim(date) != '') {
                    textInfo += ((!textInfo || 0 === textInfo.length) ? generalResources.FilteredBy : ' ' + unison) + ' ' + filter.Text + ' ' + operator + ' ' + date;
                }
            }
            break;

        case 6:
            var options = setCustomFilter(filter, operator, textInfo); //need to be defined if using custom type
            result = options[0];
            textInfo = options[1];
            break;

        default:
            var value = $('#' + filter.Name + '-search').val();
            var searchValue = value;

            if (filter.ControlType == 3) {
                if (value == "-1")
                    break;
                else {
                    searchValue = $('#' + filter.Name + ' option:selected').text();
                }
            }

            if (!$.isEmptyObject(value) && value != '') {
                result = {
                    Type: filter.FilterOption.Type,
                    Label: filter.FilterOption.Label,
                    Operator: filter.FilterOption.Operator,
                    Unison: filter.FilterOption.Unison,
                    Value: value
                };

                if ($.trim(value) != '') {
                    textInfo += ((!textInfo || 0 === textInfo.length) ? generalResources.FilteredBy : ' ' + unison) + ' ' + filter.Text + ' ' + operator + ' ' + searchValue;
                }
            }
            break;
    }

    return [result, textInfo];
}

function initActiveControls() {
    $('#button-activate').off();
    $('#button-deactivate').off();
    $('#all-checkbox-item').off();
    $(':checkbox:not(#all-checkbox-item)').off();

    $('#button-activate').click(function () {
        hideAlertBoxes();
        if ($(':checkbox:not(#all-checkbox-item):checked').length > 0) {
            changeDataStatus(true);
        }
        else {
            showMessageOnAlertBox(generalResources.NoDataSelected, $(".alert#div-alert-error"));
        }
    });
    $('#button-deactivate').click(function () {
        hideAlertBoxes();
        if ($(':checkbox:not(#all-checkbox-item):checked').length > 0) {
            changeDataStatus(false);
        }
        else {
            showMessageOnAlertBox(generalResources.NoDataSelected, $(".alert#div-alert-error"));
        }
    });

    $('#all-checkbox-item').change(function () {
        if (this.checked) {
            $(':checkbox:not(:checked)').each(function (i, obj) {
                obj.checked = true;
            });

            buffer_selected_data_id = all_data_id;
        }
        else if (!this.checked) {
            $(':checkbox:checked').each(function (i, obj) {
                obj.checked = false;
            });

            buffer_selected_data_id = [];
        }
    });

    $(':checkbox:not(#all-checkbox-item)').click(function () {
        var chkAll = $('#all-checkbox-item')[0];

        //TODO: fix this, in current build this criteria is not true
        if ($(':checkbox:checked').length == $(':checkbox:not(#all-checkbox-item)').length) {
            chkAll.checked = true;
        }
        if (!this.checked && chkAll.checked) {
            chkAll.checked = false;
        }

        var itemID = $(this).parents('tr').attr('itemid');

        if (this.checked) {
            buffer_selected_data_id.push(itemID);
        } else {
            var delIndex = buffer_selected_data_id.lastIndexOf(itemID);
            buffer_selected_data_id = buffer_selected_data_id.splice(delIndex, 1);
        }
    });
}

function changeDataStatus(activeInactive) {

    var info = '';

    var checkedIds = buffer_selected_data_id;
    /*
    var checkedIds = [];
    $(':checkbox:not(#all-checkbox-item):checked').each(function (i, obj) {
        checkedIds.push(parseInt(obj.name.replace('checkbox-item-', '')));
    });
    */

    var data = {
        checkedIds: checkedIds,
        activeInactive: activeInactive,
    };

    $('.loader:first').off('ajaxSuccess');
    $('.loader:first').off('ajaxFail');

    $('.loader:first').trigger('loadAjax', [url_change_status, data]);

    $('.loader:first').on('ajaxSuccess', function (event, result) {


        if (result.Status == "success") {
            $(':checkbox:checked').parent().parent('tr').children('.active-status').each(function () {

                $(this).children('div').remove();

                var el = $("<div></div>").addClass(activeInactive ? "active-marker" : "inactive-marker");
                el.text(activeInactive ? generalResources.Active : generalResources.Inactive);
                $(this).append(el);


                if (typeof (onChangeStatusSuccess) != "undefined") {
                    onChangeStatusSuccess(checkedIds, activeInactive);
                }

                //TODO: needs to be changed - figure out the global impl
                //info += (!info || 0 === info.length) ?
                //    $(this).parents('tr').attr('drivername') : ', ' + $(this).parents('tr').attr('drivername');
            });
            info += (info.length > 0 ? ' ' + (activeInactive ? generalResources.Activated : generalResources.Deactivated) : '');

            $(':checkbox:checked').each(function (i, obj) {
                obj.checked = false;
            });

            if (result.Message.length > 0) {
                showMessageOnAlertBox(result.Message, $(".alert#div-alert-info"));
            }
            else {
                showMessageOnAlertBox(info, $(".alert#div-alert-info"));
            }

        }
        else {

            if (result.Message.length > 0) {
                showMessageOnAlertBox(result.Message, $(".alert#div-alert-error"));
            }
        }

    });

    $('.loader:first').on('ajaxFail', function (event, data) {
        alert(data);
    });

    bindCustomPostEvent();

}


function deleteItem(itemID, itemName) {
    hideAlertBoxes();

    var deleteText = generalResources.ConfirmDelete
    deleteText = deleteText.replace("{0}", itemName);

    $('.delete-confirmation').text(deleteText);
    dialog.init({
        type: 'dialog',
        dialogSize: 'modal-md',
        headerText: generalResources.Delete + " " + itemName,
        contentId: 'modal-delete-confirmation',
        submitButtonText: generalResources.Delete,
        onSubmitButtonClicked: function () {
            callDeleteItem(itemID);
        }
    });
    dialog.show();
}

function callDeleteItem(itemID) {

    var info = '';

    var data = {
        itemID: itemID
    };

    $('.loader:first').off('ajaxSuccess');
    $('.loader:first').off('ajaxFail');
    $('.loader:first').off('ajaxComplete');

    $('.loader:first').trigger('loadAjax', [url_delete, data]);

    $('.loader:first').on('ajaxSuccess', function (event, result) {
        console.log(result.Status + "!!");
        if (result.Status == "success") {
            info += "success";
            check = true;
            $(':checkbox:checked').each(function (i, obj) {
                obj.checked = false;
            });
            resultMessage = result.Message;
            resultInfo = info;
            getData(1);
            if (resultMessage.length > 0) {
                showMessageOnAlertBox(resultMessage, $(".alert#div-alert-success"));
            }
            else {
                showMessageOnAlertBox(resultInfo, $(".alert#div-alert-success"));
            }
        }
        else {
            if (result.Message.length > 0) {
                showMessageOnAlertBox(result.Message, $(".alert#div-alert-error"));
            }
        }
    });
    $('.loader:first').on('ajaxFail', function (event, data) {
        alert(data);
    });
    bindCustomPostEvent();
}