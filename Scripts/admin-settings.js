﻿//var url = "";

$(document).on("ready", function () {
    onCancelClicked();
    onImageRemoved();
    setUploaderAttributes();
    onInsertRSSLinkClicked();
    onRemoveLinkClicked('rss-link');
    onCheckboxClicked();
});

function onCheckboxClicked() {
    $(".setting-form").find('input[type="checkbox"]').each(function () {
        var item = $(this);

        $(item).on("change", function () {
            if ($(item).prop("checked")) {
                $(item).attr("value", "true");
            }
            else {
                $(item).attr("value", "false");
            }

        });
    });
}

function onCancelClicked() {
    $(".btn-cancel").on("click", function () {
        window.location = url;
    });
}

function onFormSubmit() {
    $("form").submit(function (event) {
        $('input[mode="integer"]').each(function () {

            if ($(this).val().trim() == "") {
                $(this).val("0");
            }
        });

        return true;
    });
}

function setUploaderAttributes() {
    $(".btn-upload").on("click", function () {
        $(this).siblings(".uploader").click();
    });

    $(".uploader").each(function () {
        onFileUpload($(this));
    });
}

function onImageRemoved() {
    $(".remove-link").on("click", function () {
        var image = $(this).siblings("img");
        var folderName = "Images/";
        var imageDefaultName = "default.png";

        $(image).attr("src", rootPath + folderName + imageDefaultName + "?" + new Date().getTime());
        $(image).siblings('input[type="hidden"]').val("::delete");

        $(this).addClass("hidden");

    });
}

function onFileUpload(element) {

    var folderName = "Temp/";
    var imageValidation = $(element).parents(".input-container").find(".validation");
    var image = $(element).parents(".uploader-container").siblings("img");
    var removeLink = $(image).siblings(".remove-link");

    $(element).fileupload({
        autoUpload: true,
        url: url_uploadaction,
        formData: {
            folderName: folderName
        },
        dataType: 'json',
        add: function (e, data) {

            if (data.originalFiles[0]['size'] > (maxFileSize * 1000)) {
                $(imageValidation).removeClass("field-validation-valid");
                $(imageValidation).addClass("field-validation-error");
                $(imageValidation).html(errorResources.FileSizeTooBig);
            }
            else {

                dialog.show({ type: "loader" });

                var jqXHR = data.submit()

                    .success(function (data, textStatus, jqXHR) {

                        var inputContainer = $(e.target).parents('.btn-file').siblings('input.image-hidden-input')[0];

                        $(imageValidation).addClass("field-validation-valid");
                        $(imageValidation).removeClass("field-validation-error");
                        $(imageValidation).html("");

                        if (data.isUploaded) {
                            
                            $(image).attr("src", rootPath + "images/" + folderName + data.fileName + "?" + new Date().getTime());
                            $(inputContainer).val("/images/" + folderName + "/" + data.fileName);
                            $(removeLink).removeClass("hidden");

                            $(image).siblings('input[type="hidden"]').val("Temp/" + data.fileName);

                        }
                        else {

                        }
                        dialog.hide();

                    })
                    .error(function (data, textStatus, errorThrown) {
                        if (typeof (data) != 'undefined' || typeof (textStatus) != 'undefined' || typeof (errorThrown) != 'undefined') {
                            alert(textStatus + errorThrown + data);
                        }
                    });

                dialog.hide();
            }
        },
        fail: function (event, data) {
            if (data.files[0].error) {
                alert(data.files[0].error);
            }
        }
    });

}


function onInsertRSSLinkClicked() {
    $('#insert-rss-link-button').on('click', function () {
        var rss_link_table = $('table.rss-link-table');
        $(rss_link_table).removeClass('hide');
        var rss_link_table_body = $(rss_link_table).children('tbody');
        var new_row = '<tr index="' + rss_link_index + '" class="data-container">';
        var new_cell_link = '<td></td>';
        new_cell_link = $(new_cell_link).append('<input type="hidden" name="RSSLinks.Index" value="' + rss_link_index + '" />').append('<input name="RSSLinks[' + rss_link_index + '].Link" class="form-control no-max-width ' + class_name + '"/>');
        var new_cell_link_remove = '<td class="list-icon text-right"></td>';
        new_cell_link_remove = $(new_cell_link_remove).append('<a class="link-remove-rss-link"><span class="glyphicon glyphicon-remove" title="' + label_remove + '"></span></a>');
        new_row = $(new_row).append($(new_cell_link)).append($(new_cell_link_remove));
        $(rss_link_table_body).append($(new_row));

        $(rss_link_table).find("tr.div-nodata").remove();

        onRemoveLinkClicked('rss-link');
        rss_link_index++;
    })
}

function onRemoveLinkClicked(name) {
    $('.link-remove-' + name).off("click");
    $('.link-remove-' + name).on('click', function () {
        var elem = $(this);
        var parent = $(elem).parents("tr.data-container");
        var rule_table = $(this).closest('table');
        $(parent).remove();

        if ($(rule_table).children('tbody').find('tr').length == 0) {
            var noDataRow = $("<tr></tr>").
                addClass("div-nodata").
                append($("<td></td>").attr({
                    colspan: $(rule_table).find("thead>tr>th").length
                }).html(generalResources.EmptyData));

            $(rule_table).children('tbody').append(noDataRow);
        }
    })
}