﻿$(document).on("ready", function () {
    initRowActions();
})

function initRowActions() {
    $('.link-default').on("click", function () {

        hideAlertBoxes();

        var item = $(this);
        var row = $(item).parents("tr");
        var table = $(row).parents("table");
        var languageID = parseInt($(row).attr("itemid"));
        var defaultCol = $(row).find("td.default-col");
        var actionCol = $(row).find("td.action-col");

        var data = {
            languageID: languageID
        };

        $('.loader:first').off('ajaxSuccess');
        $('.loader:first').off('ajaxFail');

        $('.loader:first').trigger('loadAjax', [url_set_default, data]);

        $('.loader:first').on('ajaxSuccess', function (event, result) {

            if (result.Status == "success") {

                window.location = url_index;

                //showMessageOnAlertBox(result.Message, $(".alert#div-alert-success"));

                //var prevdefaultID = parseInt(result.PrevDefaultID);
                //var prevRow = $(table).find('tr[itemid="' + prevdefaultID + '"]');
                //var prevDefaultCol = $(prevRow).find("td.default-col");
                //var prevActionCol = $(prevRow).find("td.action-col");

                //$(prevDefaultCol).find("img.default").remove();
                //$(actionCol).find("a.link-default").remove();

                //var imgDefault = $("<img>").attr({
                //    "title": generalResources.Default,
                //    "alt": generalResources.Default,
                //    "src": rootPath + 'images/default-language.png?' + new Date().getTime()
                //}).addClass("img-responsive icon-uploaded default");

                //$(defaultCol).append(imgDefault);

                //var linkDefault = $("<a></a>").addClass("link-default");
                //var imgActionDefault = $("<img>").attr({
                //    "title": generalResources.SetAsDefault,
                //    "alt": generalResources.SetAsDefault,
                //    "src": rootPath + 'images/default-normal.png?' + new Date().getTime()
                //});

                //$(imgActionDefault).on("mouseover", function () {
                //    $(this).attr("src", rootPath + 'images/default-hover.png?' + new Date().getTime())
                //});

                //$(imgActionDefault).on("mouseout", function () {
                //    $(this).attr("src", rootPath + 'images/default-normal.png?' + new Date().getTime())
                //});

                //$(linkDefault).append(imgActionDefault);
                //$(prevActionCol).prepend(linkDefault);

            }
            else if (result.Status == "fail") {
                showMessageOnAlertBox(result.Message, $(".alert#div-alert-error"));
            }
        });

        $('.loader:first').on('ajaxFail', function (event, data) {
            alert(data);
        });

        bindCustomPostEvent();

    });
}

function onChangeStatusSuccess(checkedIds, activeInactive) {

    for (i = 0; i < checkedIds.length; i++) {
        var parent = $('tr[itemid="' + checkedIds[i] + '"]');
        var linkDefault = $(parent).find(".link-default");

        if (activeInactive) {
            linkDefault.removeClass("hidden");
        }
        else {
            linkDefault.addClass("hidden");
        }
    }
}