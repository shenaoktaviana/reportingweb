﻿$(document).ready(function () {
    initPublishHandler();
});

function initPublishHandler() {

    $('.button-publish').on('customClick', function () {
        
        var itemIDs = $.map($('[name^="checkbox-item-"]:checked'), function (obj, i) {

            var elemName = $(obj).attr('name');
            var elemID = elemName.substring(elemName.lastIndexOf("-") + 1);

            return parseInt(elemID);
        });

        var data = {
            itemIDs: itemIDs
        };

        if (data.itemIDs.length > 0) {
            var submitButtonText = customResources.Publish + " " + title + "?";
            $('.delete-confirmation').text(submitButtonText);

            dialog.init({
                type: 'dialog',
                dialogSize: 'modal-md',
                headerText: customResources.Publish,
                submitButtonText: customResources.Publish,
                contentId: 'modal-delete-confirmation',
                onSubmitButtonClicked: function () {

                    $('.loader:first').off('ajaxSuccess');
                    $('.loader:first').off('ajaxFail');

                    $('.loader:first').trigger('loadAjax', [url_publish, data]);

                    $('.loader:first').on('ajaxSuccess', function (event, result) {

                        $("#div-alert-success").children().not(".glyphicon").remove();
                        $("#div-alert-error").children().not(".glyphicon").remove();

                        $(".alert-info").addClass('d-none');
                        $(".alert-success").addClass('d-none');
                        $(".alert-danger").addClass('d-none');

                        if (result.status == "OK") {
                            if ($('.table.table-striped tobody tr').length == 1 && prev_paging_page > 1) {
                                getData(prev_paging_page - 1);
                            }
                            else {
                                getData(prev_paging_page);
                            }

                            $(".alert-success").removeClass('d-none');
                            $("#div-alert-success").append("<span>" + result.message + "</span>");
                            getData(1);
                        }
                        else {
                            $(".alert-danger").removeClass('d-none');
                            $("#div-alert-error").append("<span>" + result.message + "</span>");
                        }

                        $('[name^="checkbox-item-"]:checked').prop("checked", false);
                        $('[name="all-checkbox-item"]:checked').prop("checked", false);
                        buffer_selected_data_id = [];
                    });

                    $('.loader:first').on('ajaxFail', function (event, data) {
                        alert(data);
                    });

                    bindCustomPostEvent();
                }
            });

            dialog.show();

        } else {
            $(".alert-danger").removeClass('d-none');
            $("#div-alert-error").append("<span>" + customResources.NoDataSelected + "</span>");
        }
    });
}