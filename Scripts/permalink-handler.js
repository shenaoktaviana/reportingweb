﻿$(document).ready(function () {
    permalinkBlur();
});

function permalinkBlur() {

    $('#Title, #Name').focus(function () {
    }).blur(function () {
        var title = $(this).val();
        var permalink = string_to_slug(title);
        $('#Permalink').val(permalink);
        });

    $('#Permalink').focus(function () {
    }).blur(function () {
        var permalink = $(this).val();
    });
}

var string_to_slug = function (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to = "aaaaeeeeiiiioooouuuunc------";

    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace('.', '-') // replace a dot for a - 
        .replace(/[^a-z0-9 -]/g, '') // remove invalid chars 
        .replace(/\s+/g, '-') // collapse whitespace and replace by - 
        .replace(/-+/g, '-'); // collapse dashes

    return str;
};