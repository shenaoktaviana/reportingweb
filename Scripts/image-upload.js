﻿$(document).ready(function () {
    initButtonRemoveImage();
});

function initImageUploader(inputId, mobileInputId, inputUploaderId) {

    if (inputUploaderId == '' || inputUploaderId === undefined)
        inputUploaderId = '.image-uploader';
    
    $(inputUploaderId).fileupload({
        autoUpload: true,
        url: url_upload_image,
        dataType: 'json',
        add: function (e, data) {
            
            var inputContainer = $(e.target).parents('.btn-file').siblings(inputId)[0];
            var imageContainer = $(e.target).parents('.btn-file').siblings('.upload-image')[0];

            if (!inputContainer) {
                inputContainer = $(e.target).parents('.btn-file').siblings(mobileInputId)[0];
            }

            var jqXHR = data.submit()
                .done(function (data, textStatus, jqXHR) {
                    if (data.isUploaded) {
                        $(imageContainer).attr("src", "");
                        $(imageContainer).attr("src", rootPath + folderName + data.fileName + "?" + new Date().getTime());
                        $(inputContainer).val(data.fileName);
                        $('.link-remove-img').show();
                        $('.link-remove-img').removeClass('hide');
                    }
                })
                .fail(function (data, textStatus, errorThrown) {
                    if (typeof (data) != 'undefined' || typeof (textStatus) != 'undefined' || typeof (errorThrown) != 'undefined') {
                        alert(textStatus + errorThrown + data);
                    }
                });
        },
        fail: function (event, data) {
            if (data.files[0].error) {
                alert(data.files[0].error);
            }
        }
    });
    $(inputUploaderId).bind('fileuploadsubmit', function (e, data) {
        //var fileName = data.fileInput[0].name + data.files[0].name;
        var fileName = data.files[0].name;
        data.formData = {
            folderName: folderName,
            imageName: fileName
        };
    });
}

function initButtonRemoveImage() {
    $('.link-remove-img').on('click', function () {

        var $img = $(this).siblings(".image-uploaded");
        $img.attr("src", rootPath + "images/default.png" + "?" + new Date().getTime());

        $(this).siblings('input[type=hidden]').val('');

        //if ($(this).hasClass("remove-mobile")) {
        //    $(mobileInputId).val('');
        //}
        //else {
        //    $(inputId).val('');
        //}
        $(this).hide();
    });
}