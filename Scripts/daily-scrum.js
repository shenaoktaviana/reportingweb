﻿var currentID = "";

$(document).on("ready", function () {
    initDatePicker();
    onAssigneeNameAutoComplete();
    initProjectTaskDropDown();
    initDescriptionDropDown();
    initAddTaskHandler();
    initRemoveTaskHandler();
    selectAllhandler();
    initDecimalFields();
    initDetailTargetDatePicker();
});

function initDescriptionDropDown() {
    $(".descriptiondropdown").each(function (i, obj) {
        initDescriptionAutoComplete(obj);
    });
}

function initDetailTargetDatePicker() {
    $(".targetdate").each(function (i, obj) {
        initTargetDate(obj);
    });
}

function initTargetDate(element) {
    $(element).datepicker({
        maxDate: new Date(),
        changeMonth: true,
        changeYear: true,
        onClose: function (dateText, inst) {
            try {
                //formatDate($(this).val());
            } catch (e) {
                setDefaultDatePicker($(this));
            }
        }
    });
}

function initDescriptionAutoComplete(element) {

    $(element).autocomplete({
        source: function (request, response) {

            console.log("AutoComplete called");

            currentID = $(this)[0].element.attr('indexData')

            var selectedTaskID = $('[name="DailyScrumDetails[' + currentID + '].ProjectTaskID').val();

            if (selectedTaskID == "") {
                selectedTaskID = 0;
            }

            console.log(selectedTaskID);
            $.getJSON(url_description, {
                term: request.term,
                projectTaskID: selectedTaskID
            }, function (data) {

                if (data.length == 0) {
                    data = [];
                    data.push({
                        itemID: '',
                        itemName: '',
                        showedName: '',
                        projectTaskID: '',
                        projectTaskName: '',
                        projectName: ''
                    });
                }

                response($.map(data, function (el) {
                    return {
                        value: el.itemName,
                        label: el.itemName,
                        name: el.itemName,
                        showedName: el.showedName,
                        projectTaskID: el.projectTaskID,
                        projectTaskName: el.projectTaskName,
                        projectName: el.projectName,
                    };
                }));
            });
        },
        minLength: 0,
        select: function (event, ui) {
            $('[name="DailyScrumDetails[' + currentID + '].Description').val(ui.item.name);
            $('[name="DailyScrumDetails[' + currentID + '].ProjectTaskID').val(ui.item.projectTaskID);
            $('[name="DailyScrumDetails[' + currentID + '].ProjectTaskName').val(ui.item.projectTaskName);
            $('[name="DailyScrumDetails[' + currentID + '].ProjectName').empty();
            $('[name="DailyScrumDetails[' + currentID + '].ProjectName').html(ui.item.projectName);
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {

        var liElem = $('<li>')
            .attr('descriptionName', item.name);

        $(liElem).append('<a>' + item.showedName + '</a>');

        return $(liElem).appendTo(ul);
    };
}

function initDecimalFields() {
    //Decimal
    $('input[mode="decimal"]').keydown(isDecimalNumberKey);

    $('input[mode="decimal"]').focus(function (evt) {
        var numVal = $(evt.currentTarget).attr('numval');
        $(evt.currentTarget).val(parseFloat(numVal) != 0 ? numVal : '');
    });

    $('input[mode="decimal"]').blur(function (evt) {
        var numVal = $(evt.currentTarget).val();

        if (numVal == '')
            numVal = 0;

        $(evt.currentTarget).attr('numval', numVal);
        $(evt.currentTarget).val(formatNumeric(numVal));
    });
}

function initProjectTaskDropDown() {
    $(".projecttaskdropdown").each(function (i, obj) {
        onTaskNameAutoComplete(obj)
    });
}

function selectAllhandler() {
    $('[name="select_all_task"]').change(function () {
        $('input[name="select_task"]').prop('checked', $('input[name="select_all_task"]').is(':checked'));

        $(".alert").css({ "display": "none" });

        if ($('input[name="select_all_task"]').is(':checked')) {
            buffer_taskIDs = allTaskIDs;
        } else {
            buffer_taskIDs = [];
        }

        $('[name="total-selected-tasks"]').html(buffer_taskIDs.length);
    });

    $('[name="select_all_selected_task"]').change(function () {
        $('input[name="select_selected_task"]').prop('checked', $('input[name="select_all_selected_task"]').is(':checked'));
    });
}

function initRemoveTaskHandler() {

    $('[name="remove-selected-task"]').click(function () {

        $("#div-alert-task-error").children().not(".glyphicon").remove();
        $("#div-alert-task-info").children().not(".glyphicon").remove();
        $(".alert").css({ "display": "none" });

        if ($('[name="select_selected_task"]:checked').length > 0) {

            var deleteCount = $('[name="select_selected_task"]:checked').length;

            dialog.show({ type: "loader" });

            realignModifyForm();

            $('[name="select_selected_task"]:checked').each(function () {
                var tr = $(this)[0].parentNode.parentNode;

                tr.remove();
            });

            $('[name="select_all_selected_task"]').prop('checked', false);

            dialog.hide();

            if ($('[name="selected-task-data"] tbody tr').length == 0) {
                var divNoData = '<tr class="div-nodata"><td colspan="6" class="text-center">' + generalResources.EmptyData + '</td><tr>';
                $('[name="selected-task-data"].table-list > tbody').append(divNoData);
            }

            $("#div-alert-task-info").css({ "display": "" });

            $("#div-alert-task-info").append("<span>" + generalResources.SelectTaskRemoved.replace("{0}", deleteCount) + "</span>");

        } else {

            $("#div-alert-task-error").html("");
            $("#div-alert-task-error").css({ "display": "" });
            $("#div-alert-task-error").append("<span>" + generalResources.NoDataSelected + "</span>");

        }

    });
}

function initAddTaskHandler() {

    $(".btn-add-tasks").click(function () {

        var data = {
            addTaskCount: $("#AddTaskCount").val()
        };

        if (data.addTaskCount == "") {
            data.addTaskCount = 1;
        }

        $("#div-alert-task-error").children().not(".glyphicon").remove();
        $("#div-alert-task-info").children().not(".glyphicon").remove();
        $(".alert").css({ "display": "none" });

        console.log("adding new task");

        $('.loader:first').trigger('loadAjax', [url_add_new_row, data]);

        $('.loader:first').off("ajaxSuccess");
        $('.loader:first').on('ajaxSuccess', function (event, result) {

            console.log('add new row processed start');

            var refreshPaging = false;

            $(".div-nodata").remove();

            $('[name="selected-task-data"] > tbody').append(result.Data);

            console.log("inittaskdropdown");
            initProjectTaskDropDown();
            initDecimalFields();
            initDescriptionDropDown();
            initDetailTargetDatePicker();

            console.log('add new row processed end');
        });

        $('.loader:first').off("ajaxFail");
        $('.loader:first').on('ajaxFail', function (event, data) {
            alert(data);
        });

        bindCustomPostEvent();

        $("#div-alert-task-info").css({ "display": "" });
        $("#div-alert-task-info").append("<span>" + generalResources.NewRowAdded + "</span>");
    });
}

function initDatePicker() {

    $("#Date").datepicker({
        maxDate: new Date(),
        changeMonth: true,
        changeYear: true,
        onClose: function (dateText, inst) {
            try {
                //formatDate($(this).val());
            } catch (e) {
                setDefaultDatePicker($(this));
            }
        }
    });

}

function onAssigneeNameAutoComplete() {

    $("#AssigneeName").autocomplete({
        source: function (request, response) {

            $('#AssigneeID').val('');

            $.getJSON(url_users, {
                term: request.term
            }, function (data) {

                if (data.length == 0) {
                    data = [];
                    data.push({
                        itemID: '',
                        itemName: '',
                    });
                }

                response($.map(data, function (el) {
                    return {
                        value: el.itemName,
                        label: el.itemName,
                        id: el.itemID,
                        name: el.itemName,
                    };
                }));
            });
        },
        minLength: 0,
        select: function (event, ui) {
            $('#AssigneeID').val(ui.item.id);
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {

        var liElem = $('<li>')
            .attr('userID', item.id);

        if (item.id > 0) {
            $(liElem).append('<a>' + item.name + '</a>');
        }
        else {
            $(liElem).addClass("ui-state-disabled").css('text-align', 'center').append(generalResources.EmptyData);
        }

        return $(liElem).appendTo(ul);
    };
}

function onTaskNameAutoComplete(taskNameEl) {

    $(taskNameEl).autocomplete({
        source: function (request, response) {

            console.log("AutoComplete called");

            currentID = $(this)[0].element.attr('indexData')

            $('[name="DailyScrumDetails[' + currentID + '].ProjectTaskID').val('');

            $.getJSON(url_projecttasks, {
                term: request.term,
                includeTaskDesc: true
            }, function (data) {

                if (data.length == 0) {
                    data = [];
                    data.push({
                        itemID: '',
                        itemName: '',
                        projectName: ''
                    });
                }

                response($.map(data, function (el) {
                    return {
                        value: el.itemName,
                        label: el.itemName,
                        id: el.itemID,
                        name: el.itemName,
                        projectName: el.projectName
                    };
                }));
            });
        },
        minLength: 0,
        select: function (event, ui) {
            $('[name="DailyScrumDetails[' + currentID + '].ProjectTaskID').val(ui.item.id);
            $('[name="DailyScrumDetails[' + currentID + '].ProjectName').empty();
            $('[name="DailyScrumDetails[' + currentID + '].ProjectName').html(ui.item.projectName);
            console.log(ui.item.projectName);
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {

        var liElem = $('<li>')
            .attr('projectTaskID', item.id);

        if (item.id > 0) {
            $(liElem).append('<a>' + item.name + '</a>');
        }
        else {
            $(liElem).addClass("ui-state-disabled").css('text-align', 'center').append(generalResources.EmptyData);
        }

        return $(liElem).appendTo(ul);
    };
}

