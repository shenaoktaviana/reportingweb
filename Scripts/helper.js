﻿var numericList = [8, 37, 38, 39, 40, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
var negativeSignList = [109, 189, 173];
var navigationKeyList = [9, 35, 36, 37, 39];
var resizeModifyForm = null;

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    /*
    console.log("Number key: " + charCode);
    console.log("Rule #1: " + ((evt.shiftKey && $.inArray(charCode, navigationKeyList) != -1) ? "TRUE" : "FALSE"));
    console.log("Rule #2: " + ((evt.ctrlKey && $.inArray(charCode, navigationKeyList) != -1) ? "TRUE" : "FALSE"));
    console.log("Rule #3: " + ((!evt.shiftKey && !evt.ctrlKey && !evt.altKey &&
        ($.inArray(charCode, numericList.concat(navigationKeyList)) != -1)) ? "TRUE" : "FALSE"));

    console.log("Rule #3 sub criteria: " + ($.inArray(charCode, numericList.concat(navigationKeyList)) != -1 ? "TRUE" : "FALSE"));
    console.log("Rule #3 position: " + $.inArray(charCode, numericList.concat(navigationKeyList)));
    */

    return (evt.shiftKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (evt.ctrlKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (!evt.shiftKey && !evt.ctrlKey && !evt.altKey &&
        ($.inArray(charCode, numericList.concat(navigationKeyList)) != -1));
}

function isPhoneKey(evt) {
    var phoneList = numericList.concat([32, 189]);
    var charCode = (evt.which) ? evt.which : event.keyCode;

    return (evt.shiftKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (evt.ctrlKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (!evt.shiftKey && !evt.ctrlKey && !evt.altKey &&
        ($.inArray(charCode, phoneList.concat(navigationKeyList)) != -1));
}

function isDecimalNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    var isValidPeriod = ($.inArray(charCode, periodList) != -1) && $(evt.target).val().indexOf(numberFormatInfo.currencyDecimalSeparator) == -1;
    var isValidNumeric = ($.inArray(charCode, numericList.concat(navigationKeyList)) != -1);

    return (evt.shiftKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (evt.ctrlKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (!evt.shiftKey && !evt.ctrlKey && !evt.altKey && (isValidPeriod || isValidNumeric));
}

function isNegativeNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    var isValidSign = ($.inArray(charCode, negativeSignList) != -1) && $(evt.target).val().indexOf("-") == -1 && evt.target.selectionStart == 0;
    var isValidNumeric = ($.inArray(charCode, numericList.concat(navigationKeyList)) != -1);

    return (evt.shiftKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (evt.ctrlKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (!evt.shiftKey && !evt.ctrlKey && !evt.altKey && (isValidNumeric || isValidSign));
}

function isNegativeDecimalNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    var isValidPeriod = ($.inArray(charCode, periodList) != -1) && $(evt.target).val().indexOf(numberFormatInfo.currencyDecimalSeparator) == -1;
    var isValidSign = ($.inArray(charCode, negativeSignList) != -1) && $(evt.target).val().indexOf("-") == -1 && evt.target.selectionStart == 0;
    var isValidNumeric = ($.inArray(charCode, numericList.concat(navigationKeyList)) != -1);

    return (evt.shiftKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (evt.ctrlKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (!evt.shiftKey && !evt.ctrlKey && !evt.altKey && (isValidNumeric || isValidSign || isValidPeriod));
}

function isNegativeFormatNumberKeys(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    var isValidSign = ($.inArray(charCode, negativeSignList) != -1) && $(evt.target).val().indexOf("-") == -1 && evt.target.selectionStart == 0;
    var isValidPeriod = ($.inArray(charCode, periodList) != -1) && $(evt.target).val().indexOf(numberFormatInfo.currencyDecimalSeparator) == -1;
    var isValidNumeric = ($.inArray(charCode, numericList.concat(navigationKeyList)) != -1);

    return (evt.shiftKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (evt.ctrlKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (!evt.shiftKey && !evt.ctrlKey && !evt.altKey && (isValidPeriod || isValidNumeric || isValidSign));
}

function isNPWPNo(evt) {
    var npwpList = numericList.concat([109, 173, 190]);
    var charCode = (evt.which) ? evt.which : event.keyCode;

    return (evt.shiftKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (evt.ctrlKey && $.inArray(charCode, navigationKeyList) != -1) ||
        (!evt.shiftKey && !evt.ctrlKey && !evt.altKey &&
        ($.inArray(charCode, npwpList.concat(navigationKeyList)) != -1));
}

function formatMoney(val) {
    var format = {
        pos: '%s %v',
        neg: '%s (%v)',
        zero: '%s  %v'
    };
    return formatMoneyBase(val, format);
}

function formatMoneyJustify(val) {
    var format = {
        pos: '<span class="pull-left">%s</span><span class="pull-right">%v</span>',
        neg: '<span class="pull-left">%s</span><span class="pull-right">(%v)</span>',
        zero: '<span class="pull-left">%s</span><span class="pull-right">%v</span>'
    };;
    return formatMoneyBase(val, format);
}

function formatMoneyBase(val, format) {
    return accounting.formatMoney(val, numberFormatInfo.currencySymbol, numberFormatInfo.currencyDecimalDigits, numberFormatInfo.currencyGroupSeparator, numberFormatInfo.currencyDecimalSeparator, format = format);
}

function formatNumeric(nStr, numPadding) {

    nStr += '';
    x = nStr.split(numberFormatInfo.currencyDecimalSeparator);
    x1 = x[0];

    x2 = x.length > 1 ? x[1] : '';

    while (x2.length < numPadding)
        x2 += '0';

    x2 = (x2 != '' ? numberFormatInfo.currencyDecimalSeparator : '') + x2;

    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + numberFormatInfo.currencyGroupSeparator + '$2');
    }
    return x1 + x2;
}

function formatDate(date) {
    return $.datepicker.parseDate(shortDateFormat, date);
}

function formatLongDate(date) {
    //return $.datepicker.parseDate(longDateFormat, date);

    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + date.getHours() + ":00";
}

function setDefaultDatePicker(elem) {
    $(elem).val('');
    $(elem).datepicker('option', 'defaultDate', new Date());
}

// Use the browser's built-in functionality to quickly and safely escape the string
function escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
};

// UNSAFE with unsafe strings; only use on previously-escaped ones!
function unescapeHtml(escapedStr) {
    var div = document.createElement('div');
    div.innerHTML = escapedStr;
    var child = div.childNodes[0];
    return child ? child.nodeValue : '';
};

// POPUP
function toggleLoaderInPopup(isDisplay) {
    if (isDisplay) {
        $(".popup-loader").css("display", "inline");
        $("#button-popup-export").attr("disabled", "disabled");
    }
    else {
        $(".popup-loader").css("display", "none");
        $("#button-popup-export").removeAttr("disabled");
    }
}

function togglePopup(isDisplay) {
    if (isDisplay) {
        if ($(".div-overlay").length > 1) {
            $(".div-overlay").first().css("display", "block");
        }
        else {
            $(".div-overlay").css("display", "block");
        }
        $(".div-popup").css("display", "block");
        $("body").css("overflow", "hidden");
        window.scrollTo(0, 0);
    }
    else {
        $(".div-overlay").css("display", "none");
        $(".div-popup").css("display", "none");
        $("body").css("overflow", "auto");
    }
}

//function togglePopup(isDisplay, isRealign) {
//    if (isDisplay) {
//        window.scrollTo(0, 500);
//        if ($(".div-overlay").length > 1) {
//            $(".div-overlay").first().css("display", "block");
//        }
//        else {
//            $(".div-overlay").css("display", "block");
//        }
//        $(".div-popup").css("display", "block");
//        $("body").css("overflow", "hidden");
//        resizeModifyForm = setInterval(function () { realignModifyForm() }, 100);
//    }
//    else {
//        clearInterval(resizeModifyForm);
//        $(".div-overlay").css("display", "none");
//        $(".div-popup").css("display", "none");
//        $("body").css("overflow", "auto");
//    }
//}

function realignModifyForm() {
    var contentHeight = $('.div-popup').height();
    var contentWidth = $('.div-popup').width();

    var windowHeight = $(window).height();
    var windowWidth = $(window).width();

    $('.div-popup:visible').css({
        'top': $(window).scrollTop() + (windowHeight / 2),
        'margin-top': -1 * (contentHeight / 2),
        'margin-left': -1 * (contentWidth / 2),
    });

    $('.div-overlay:visible').css({
        'top': $(window).scrollTop(),
        'left': 0
    });
}

// Original code from: https://gist.github.com/remino/1563963
// Helper to process date difference
// START
function DateDiff(date1, date2) {
    this.days = null;
    this.hours = null;
    this.minutes = null;
    this.seconds = null;
    this.date1 = date1;
    this.date2 = date2;

    this.init();
}

DateDiff.prototype.init = function () {
    var data = new DateMeasure(this.date1 - this.date2);
    this.days = data.days;
    this.hours = data.hours;
    this.minutes = data.minutes;
    this.seconds = data.seconds;
};

function DateMeasure(ms) {
    var d, h, m, s;
    s = Math.floor(ms / 1000);
    m = Math.floor(s / 60);
    s = s % 60;
    h = Math.floor(m / 60);
    m = m % 60;
    d = Math.floor(h / 24);
    h = h % 24;

    this.days = d;
    this.hours = h;
    this.minutes = m;
    this.seconds = s;
};

Date.diff = function (date1, date2) {
    return new DateDiff(date1, date2);
};

Date.prototype.diff = function (date2) {
    return new DateDiff(this, date2);
};

String.prototype.capitalizeFirstLetter = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

//END
