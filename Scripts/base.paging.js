﻿
var pagingObj;
var pagingObjCollection = {};
var pagingObjPrevCollection = {};

$(document).ready(function () {
    preparePagination();
});

function initLoaderDialog() {
    dialog.init({
        type: "loader"
    });
}

function preparePagination() {
    pagingObj = $(".pagination:not([customtype])").paging(total_row, { // make 1337 elements navigatable
        format: '(qq -) nncnn (- pp)', // define how the navigation should look like and in which order onFormat() get's called
        perpage: row_per_page, // show 10 elements per page
        lapping: 0, // don't overlap pages for the moment,
        page: currentPage,
        onSelect: function (page) {

            bindCustomPostEvent();

            if (parseInt(prev_paging_page) != parseInt(page)) {

                if (dialog) {
                    initLoaderDialog();
                }

                getData(page);
            }

        },
        onFormat: function (type) {
            switch (type) {
                case 'right':
                case 'left':
                    if (this.active)
                        return '<li><a class="link-pager" name="page-' + this.value + '" href="#' + this.value + '">' + this.value + '</a></li>';
                    return "";
                case 'block':
                    if (!this.active)
                        return '<li><span class="disabled">' + this.value + '</span></li>';
                    else if (this.value != this.page)
                        return '<li><a class="" name="page-' + this.value + '" href="#' + this.value + '">' + this.value + '</a></li>';
                    return '<li class="active"><span class="current" name="page-' + this.value + '">' + this.value + '</span></li>';
                case 'next':
                    if (this.active)
                        return '<li><a class="link-pager" name="page-' + this.value + '" href="#' + this.value + '" class="next"></a></li>';
                    return '<li><span class="disabled"></span></li>';
                case 'prev':
                    if (this.active)
                        return '<li><a class="link-pager" name="page-' + this.value + '" href="#' + this.value + '" class="prev"></a></li>';
                    return '<li><span class="disabled"></span></li>';
                case 'first':
                    if (this.active)
                        return '<li><a class="link-pager" name="page-' + this.value + '" href="#' + this.value + '" class="first">First</a></li>';
                    return '<li><span class="disabled">First</span></li>';
                case 'last':
                    if (this.active)
                        return '<li><a class="link-pager" name="page-' + this.value + '" href="#' + this.value + '" class="last">Last</a></li>';
                    return '<li><span class="disabled">Last</span></li>';
                case "leap":
                    if (this.active)
                        return "<li>&nbsp;</li>";
                    return "";
                case 'fill':
                    if (this.active)
                        return "<li><span>...</span></li>";
                    return "";
            }
        }
    });
}

function prepareCustomPagination(customType, totalRow, customCurrentPage, isModal) {
    pagingObjCollection[customType] = $('.pagination[customtype="' + customType + '"]').paging(totalRow, { // make 1337 elements navigatable
        format: '(qq -) nncnn (- pp)', // define how the navigation should look like and in which order onFormat() get's called
        perpage: row_per_page, // show 10 elements per page
        lapping: 0, // don't overlap pages for the moment,
        page: customCurrentPage,
        onSelect: function (page) {

            bindCustomPostEvent();

            console.log("Custom paging");
            console.log("prev paging: " + pagingObjPrevCollection[customType]);
            console.log("page: " + page);
            console.log("type: " + customType);


            if (parseInt(pagingObjPrevCollection[customType]) != parseInt(page)) {

                if(!isModal)
                    initLoaderDialog();

                getCustomData(customType, page);
            }

        },
        onFormat: function (type) {
            switch (type) {
                case 'right':
                case 'left':
                    if (this.active)
                        return '<li><a class="link-pager loader" name="page-' + this.value + '" href="#' + this.value + '">' + this.value + '</a></li>';
                    return "";
                case 'block':
                    if (!this.active)
                        return '<li><span class="disabled">' + this.value + '</span></li>';
                    else if (this.value != this.page)
                        return '<li><a class="loader" name="page-' + this.value + '" href="#' + this.value + '">' + this.value + '</a></li>';
                    return '<li class="active"><span class="current loader" name="page-' + this.value + '">' + this.value + '</span></li>';
                case 'next':
                    if (this.active)
                        return '<li><a class="link-pager loader" name="page-' + this.value + '" href="#' + this.value + '" class="next"></a></li>';
                    return '<li><span class="disabled"></span></li>';
                case 'prev':
                    if (this.active)
                        return '<li><a class="link-pager loader" name="page-' + this.value + '" href="#' + this.value + '" class="prev"></a></li>';
                    return '<li><span class="disabled"></span></li>';
                case 'first':
                    if (this.active)
                        return '<li><a class="link-pager loader" name="page-' + this.value + '" href="#' + this.value + '" class="first">First</a></li>';
                    return '<li><span class="disabled">First</span></li>';
                case 'last':
                    if (this.active)
                        return '<li><a class="link-pager loader" name="page-' + this.value + '" href="#' + this.value + '" class="last">Last</a></li>';
                    return '<li><span class="disabled">Last</span></li>';
                case "leap":
                    if (this.active)
                        return "<li>&nbsp;</li>";
                    return "";
                case 'fill':
                    if (this.active)
                        return "<li><span>...</span></li>";
                    return "";
            }
        }
    });
}