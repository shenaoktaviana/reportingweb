﻿using DomainModelFramework;
using ReportingBusinessService;
using ReportingBusinessService.Models;
using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReportingWeb.ActionFilters
{
    public class AdminPrivilegeAttribute : AuthorizeAttribute
    {
        private string[] specialControllerName = new string[] { "Reports", "Users", "MyAccount"};

        public bool IsSuperOnly { get; set; }
        public bool SuperAccess { get; set; }
        private Module Module { get; set; }
        public string[] PrivilegeNames { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (this.PrivilegeNames == null)
            {
                this.PrivilegeNames = new string[] { };
            }

            SettingsService service = new SettingsService();

            Setting generalSettings = service.Get().Where(x => x.Type == "G").FirstOrDefault();
            GeneralSettings generalSetting = new GeneralSettings();

            if (generalSettings != null)
            {
                generalSetting = service.GetSettingObj(generalSettings) as GeneralSettings;
            }

            bool isAllowed = false;
            bool isAuthenticated = false;
            bool isUnderConstruction = generalSetting.IsUnderConstruction;

            IPrincipal user = filterContext.HttpContext.User;
            Type controller = filterContext.Controller.GetType();
            ActionDescriptor action = filterContext.ActionDescriptor;
            string controllerName = controller.Name.Replace("Controller", string.Empty);
            string actionName = action.ActionName;
            UrlHelper url = new UrlHelper(filterContext.RequestContext);

            ModuleAttribute moduleAttr = (ModuleAttribute)action.GetCustomAttributes(true)
                .Where(x => x.GetType() == typeof(ModuleAttribute)).FirstOrDefault();

            if (moduleAttr == null)
            {
                moduleAttr = (ModuleAttribute)controller.GetCustomAttributes(true)
                    .Where(x => x.GetType() == typeof(ModuleAttribute)).FirstOrDefault();
            }

            ModuleService moduleService = new ModuleService();

            if (moduleAttr != null)
                this.Module = moduleService.GetBy(x => x.Name == moduleAttr.Value).FirstOrDefault();
            else
                this.Module = moduleService.GetBy(x => x.Name == controllerName).FirstOrDefault();

            string[] validRole = new string[] { UserRole.SUPER.ToString() };

            if (!this.IsSuperOnly)
            {
                Array.Resize(ref validRole, validRole.Length + 1);
                validRole[validRole.Length - 1] = UserRole.ADMIN.ToString();
            }

            if (controllerName.ToLower() == "account" && actionName.ToLower() == "login")
                isUnderConstruction = false;

            if (user.Identity.IsAuthenticated && (((ClaimsIdentity)user.Identity).RoleClaimType) == UserRole.SUPER.ToString())
            {
                isUnderConstruction = false;
            }

            if (!isUnderConstruction)
            {
                if (user.Identity.IsAuthenticated)
                {
                    isAuthenticated = true;
                    ClaimsIdentity identity = (ClaimsIdentity)user.Identity;

                    if ((this.Module != null && validRole.Contains(identity.RoleClaimType)) ||
                        specialControllerName.Contains(controllerName))
                    {
                        if (identity.RoleClaimType == UserRole.SUPER.ToString())
                        {
                            isAllowed = true;
                        }
                        else
                        {
                            string userId = identity.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Select(x => x.Value).FirstOrDefault();

                            UserService userService = new UserService();
                            User loggedInUser = userService.Get(new object[] { long.Parse(userId) });

                            bool activeSub = true;

                            if (activeSub)
                            {
                                if (controllerName != "Users")
                                {
                                    if (controllerName == "Reports")
                                    {
                                        UserModulePrivilege userReportPrivilege = loggedInUser.UserModulePrivileges.FirstOrDefault(x =>
                                                  x.ModulePrivilege.Module.Name.Contains("Reports")
                                                  && x.ModulePrivilege.Privilege.Name == PrivilegeName.VIEW.ToString()
                                                  && x.PrivilegeState.Name == PrivilegeStateName.ALLOWED.ToString());
                                        if (userReportPrivilege != null)
                                        {
                                            isAllowed = true;
                                        }
                                    }
                                    else
                                    {
                                        UserModulePrivilege userModulePrivilege = loggedInUser.UserModulePrivileges.FirstOrDefault(x =>
                                              x.ModulePrivilege.Module.Name == this.Module.Name
                                              && x.ModulePrivilege.Privilege.Name == PrivilegeName.VIEW.ToString()
                                              && x.PrivilegeState.Name == PrivilegeStateName.ALLOWED.ToString());

                                        if (userModulePrivilege != null)
                                        {
                                            if (this.PrivilegeNames.Contains(PrivilegeName.INSERT.ToString()) || this.PrivilegeNames.Contains(PrivilegeName.EDIT.ToString()))
                                            {
                                                string idString = (filterContext.HttpContext.Request.RequestContext.RouteData.Values["id"] as string) ??
                                                    (filterContext.HttpContext.Request["id"] as string);

                                                long id = 0;

                                                Int64.TryParse(idString, out id);

                                                if (id > 0 && this.PrivilegeNames.Contains(PrivilegeName.EDIT.ToString()))
                                                {
                                                    userModulePrivilege = loggedInUser.UserModulePrivileges.FirstOrDefault(x =>
                                                        x.ModulePrivilege.Module.Name == this.Module.Name
                                                    && x.ModulePrivilege.Privilege.Name == PrivilegeName.EDIT.ToString()
                                                    && x.PrivilegeState.Name == PrivilegeStateName.ALLOWED.ToString());
                                                }
                                                else if (this.PrivilegeNames.Contains(PrivilegeName.INSERT.ToString()))
                                                {
                                                    userModulePrivilege = loggedInUser.UserModulePrivileges.FirstOrDefault(x =>
                                                        x.ModulePrivilege.Module.Name == this.Module.Name
                                                    && x.ModulePrivilege.Privilege.Name == PrivilegeName.INSERT.ToString()
                                                    && x.PrivilegeState.Name == PrivilegeStateName.ALLOWED.ToString());
                                                }
                                            }

                                            foreach (string privilegeName in this.PrivilegeNames.Except(
                                                new string[] {
                                    PrivilegeName.VIEW.ToString(),
                                    PrivilegeName.INSERT.ToString(),
                                    PrivilegeName.EDIT.ToString()
                                                }))
                                            {
                                                userModulePrivilege = loggedInUser.UserModulePrivileges.FirstOrDefault(x =>
                                                    x.ModulePrivilege.Module.Name == this.Module.Name
                                                    && x.ModulePrivilege.Privilege.Name == privilegeName
                                                    && x.PrivilegeState.Name == PrivilegeStateName.ALLOWED.ToString());
                                            }

                                            if (userModulePrivilege != null)
                                            {
                                                isAllowed = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (this.SuperAccess)
                    {
                        isAllowed = UserRole.SUPER.ToString() == identity.RoleClaimType;
                    }
                }

                if (!isAllowed)
                {
                    if (isAuthenticated)
                    {

                        byte[] plainParamBytes = Encoding.UTF8.GetBytes(HttpErrorCode.Unauthorized.ToString() + "||Access+Denied");
                        string encryptParam = Convert.ToBase64String(plainParamBytes);

                        filterContext.Result = new RedirectToRouteResult(
                                               new RouteValueDictionary
                                       {
                                       { "action", "Index" },
                                       { "controller", "Error"},
                                       {"value",encryptParam}
                                       });
                    }
                    else
                    {
                        string returnUrl = url.Action(actionName, controllerName);

                        filterContext.Result = new RedirectToRouteResult(
                                           new RouteValueDictionary
                                           {
                                       { "action", "Login" },
                                       { "controller", "Account"},
                                       {"returnUrl",returnUrl}
                                           });

                    }
                }
            }
            else
            {
                filterContext.Result = new ViewResult { ViewName = "_UnderConstructionPartial" };
            }
        }
    }
}