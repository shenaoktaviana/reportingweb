﻿using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ReportingWeb.ActionFilters
{
    public class JsonTokenAttribute : AuthorizeAttribute
    {
        private string _secretKey;

        public JsonTokenAttribute(string secret) :
            base()
        {
            this._secretKey = secret;
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            bool isValid = false;

            var headers = actionContext.Request.Headers;

            if (headers.Contains("Authorization"))
            {
                string token = headers.GetValues("Authorization").First();
                string tokenValue = "";

                if (token.StartsWith("Bearer "))
                {
                    tokenValue = token.Substring(7);
                }

                string[] tokenComp = tokenValue.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);

                if (tokenComp.Count() != 3)
                {
                    //Invalid request
                    return false;
                }
                else
                {
                    byte[] data = Convert.FromBase64String(tokenComp[0]);
                    string headerString = Encoding.UTF8.GetString(data);

                    data = Convert.FromBase64String(tokenComp[1]);
                    string payloadString = Encoding.UTF8.GetString(data);

                    data = Convert.FromBase64String(tokenComp[2]);
                    string signatureString = Encoding.UTF8.GetString(data);

                    IWTHeader header = (IWTHeader)JsonConvert.DeserializeObject(headerString, typeof(IWTHeader));

                    if (header.Algorithm != "HS256" || header.Type != "JWT")
                        return false;

                    long unixTimeNow = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

                    IWTPayload payload = (IWTPayload)JsonConvert.DeserializeObject(payloadString, typeof(IWTPayload));

                    if (payload.ExpiredTime > unixTimeNow && payload.Issuer == "iwapp" && payload.Audience == "iwapi")
                    //if (payload.Issuer == "iwapp" && payload.Audience == "iwapi")
                    {
                        byte[] bytesSecret = Encoding.UTF8.GetBytes(this._secretKey);
                        byte[] bytes = Encoding.UTF8.GetBytes(tokenComp[0] + "." + tokenComp[1]);

                        HMACSHA256 hashstring = new HMACSHA256(bytesSecret);

                        byte[] hash = hashstring.ComputeHash(bytes);
                        string hashString = string.Empty;
                        foreach (byte x in hash)
                        {
                            hashString += string.Format("{0:x2}", x);
                        }

                        isValid = hashString == signatureString;
                    }
                    else
                        return false;
                }
            }

            return isValid;
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            /*
            var msg = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Oops!!!" };
            throw new HttpResponseException(msg);

            return Content(HttpStatusCode.Unauthorized, "My error message");
            throw new HttpResponseException(HttpStatusCode.Unauthorized);
            */

            actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Unauthorized request" };
            actionContext.Response.StatusCode = HttpStatusCode.Unauthorized;
            //actionContext.Response = Content(HttpStatusCode.Unauthorized, "My error message");

            //actionContext.Response.SuppressFormsAuthenticationRedirect = true;
        }

    }
}