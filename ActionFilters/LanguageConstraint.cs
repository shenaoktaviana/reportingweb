﻿using ReportingBusinessService;
using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace ReportingWeb.ActionFilters
{
    public class LanguageConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName,
            RouteValueDictionary values, RouteDirection routeDirection)
        {
            LanguageService service = new LanguageService();

            string lang = values[parameterName].ToString();

            Language language = service
                .GetBy(x => x.Culture.ToLower() == lang.ToLower())
                .FirstOrDefault();

            return language != null;
        }
    }
}