﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingWeb.ActionFilters
{
    public class ModuleAttribute : Attribute
    {
        private string value;

        public ModuleAttribute()
        {
            this.value = "";
        }

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
    }
}