﻿using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public class LanguagesController : BaseAdminController
    {
        public LanguagesController()
            : base()
        {
            this.FriendlyIndexModuleName = "Languages";
            this.FriendlyDetailModuleName = "Language";
        }

        #region Index
        protected override string GetEntityType()
        {
            return "ReportingDomainModel.Language, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return new LanguageService().Get(filters, SortOption.Custom.ToString(), 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            this.GetRootPath();
            return new LanguageService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        [AdminPrivilege(SuperAccess = true)]
        public override ActionResult Index(long? id)
        {
            return base.Index(id);
        }

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            this.GetRootPath();

            model.Title = GeneralResources.Languages;
            model.Styles = new List<string>() { "~/Content/languages/css" };
            model.Scripts = new List<string>() { "~/bundles/languages" };
            //model.CanInsert = this.CanInsert(this.controllerName);
            //model.CanActivate = this.CanEdit(this.controllerName);
            model.CanInsert = true;
            model.CanActivate = true;
            model.Filters = new List<ListFilterItem>() {

                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Property, Label = new string[] { "Name" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "Name",
                    Text = FrameworkResources.Name,
                    ControlType = ControlType.Textbox,
                    Width = "50%"
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Property, Label = new string[] { "IsActive" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "IsActive",
                    Text = FrameworkResources.Status,
                    ControlType = ControlType.Dropdown,
                    Width = "50%",
                     Options = this.GetActiveStatus()
                }
            };

            return model;
        }

        protected override List<ListColumn> GetColumns()
        {
            return new List<ListColumn>() {
                new ListColumn()
                {
                    DisplayMode = ListColumnDisplayMode.CheckboxWithSelectAll,
                    Width = "5%"
                },
                new ListColumn()
                {
                    DisplayMode = ListColumnDisplayMode.Custom,
                    CustomPartialName = "_ActiveMarkerPartial",
                    Width = "10%"
                },
                 new ListColumn()
                {
                    DisplayMode = ListColumnDisplayMode.Custom,
                    CustomPartialName = "_LanguageDefaultPartial",
                    Width = "5%"
                },
                new ListColumn()
                {
                    DisplayMode = ListColumnDisplayMode.Custom,
                    CustomPartialName = "_LanguageIconPartial",
                    Width = "5%"
                },
                new ListColumn()
                {
                    Name = "Name",
                    DisplayMode = ListColumnDisplayMode.Text,
                    TextPropertyName = "Name",
                    HeaderText = FrameworkResources.Name,
                    Width = "55%"
                },
                new ListColumn()
                {
                    Name = "Actions",
                    DisplayMode = ListColumnDisplayMode.Custom,
                    Width = "20%",
                    CustomPartialName = "_LanguagesActionsPartial"
                }
            };
        }

        protected override SelectList GetSortList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Text = FrameworkResources.Default, Value = SortOption.Custom.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.NameAscending, Value = SortOption.NameAscending.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.NameDescending, Value = SortOption.NameDescending.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.LatestInserted, Value = SortOption.LatestInserted.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.LatestUpdated, Value = SortOption.LatestUpdated.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstInserted, Value = SortOption.FirstInserted.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstUpdated, Value = SortOption.FirstUpdated.ToString(), Selected = false });

            return new SelectList(list, "Value", "Text");
        }

        [HttpPost]
        public ActionResult ChangeStatus(List<long> checkedIds, bool activeInactive)
        {
            string message = "";
            string status = "success";
            LanguageService service = new LanguageService(this.GetLoggedInUserID());

            try
            {
                IEnumerable<Language> languages = service.GetBy(x => checkedIds.Contains(x.LanguageID));

                string names = "";
                foreach (Language item in languages)
                {
                    if (!activeInactive)
                    {
                        if (item.IsDefault)
                        {
                            message = String.Format(ErrorResources.LanguageDeactivateWarning, item.Name);

                            status = "fail";

                            break;
                        }
                    }

                    if (status == "success")
                    {
                        item.IsActive = activeInactive;
                        names += item.Name + ", ";
                    }
                }

                if (status == "success")
                {
                    if (names.Length > 2)
                    {
                        names = names.Substring(0, names.Length - 2);
                    }

                    if (activeInactive)
                    {
                        message = names + " " + FrameworkResources.Activated;
                    }
                    else
                    {
                        message = names + " " + FrameworkResources.Deactivated;
                    }

                    languages = service.UpdateLanguages(languages);
                }

            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message
                });
        }

        [HttpPost]
        public ActionResult SetAsDefault(int languageID)
        {
            string message = "";
            string status = "success";
            LanguageService service = new LanguageService(this.GetLoggedInUserID());
            int prevdefaultID = 0;
            try
            {
                Language language = service.Get(new object[] { languageID });
                Language prevDefaultLanguage = service.GetBy(x => x.IsDefault).FirstOrDefault();

                if (!language.IsActive)
                {
                    status = "fail";
                    message = String.Format(ErrorResources.LanguageDefaultWarning, language.Name);
                }

                if (status == "success")
                {
                    message = String.Format(GeneralResources.LanguageSetAsDefault, language.Name);

                    prevdefaultID = prevDefaultLanguage.LanguageID;

                    language.IsDefault = true;
                    prevDefaultLanguage.IsDefault = false;

                    IEnumerable<Language> languages = new List<Language>{
                        language,
                        prevDefaultLanguage
                    };

                    languages = service.UpdateLanguages(languages);

                    Dictionary<string, string> messages = new Dictionary<string, string>();
                    messages.Add("Success", message);
                    TempData["Message"] = messages;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message,
                    PrevDefaultID = prevdefaultID
                });
        }


        [HttpPost]
        public ActionResult Delete(long itemID)
        {
            string message = "";
            string status = "success";
            LanguageService service = new LanguageService();

            try
            {
                Language language = service.GetBy(x => x.LanguageID == itemID).FirstOrDefault();

                string languageName = language.Name;

                message = ErrorResources.LanguageUsedInDetail;
                status = "fail";
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            bool deleteSuccess = (status == "success");

            return Json(
                new
                {
                    Status = status,
                    Message = message,
                    DisableHide = deleteSuccess
                });
        }

        #endregion

        #region Insert/Edit

        //[AdminPrivilege(PrivilegeNames = new string[] { "INSERT", "EDIT" })]
        [AdminPrivilege(SuperAccess = true)]
        public ActionResult Detail(long? id)
        {
            return View(this.GenerateDetailModel(id));
        }

        //[AdminPrivilege]
        [AdminPrivilege(SuperAccess = true)]
        public ActionResult View(long id)
        {
            ViewBag.CanEdit = false;
            return View(this.GenerateDetailModel(id));
        }

        private Language GenerateDetailModel(long? id)
        {
            Language language = new Language();

            LanguageService languageService = new LanguageService();

            string fullCulture = string.Empty;

            if (id.HasValue && id.Value > 0)
            {
                language = languageService.Get(new object[] { id.Value });
                fullCulture = (language.Icon == null ? language.Culture : language.Culture + "-" + language.Icon.ToUpper());
            }
            else
            {
                CultureInfo cultureInfo = this.GetAvailableCultures(id).FirstOrDefault();

                if (cultureInfo != null)
                {
                    fullCulture = this.GetAvailableCultures(id).FirstOrDefault().TextInfo.CultureName;
                }

                language.Culture = fullCulture.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                language.IsActive = true;
                language.IsDefault = false;
            }

            language.Icon = this.GetFlagIcon(fullCulture);

            this.GetCultures(fullCulture, id);

            this.InitDefaultFunctions(id);

            return language;
        }

        private string GetFlagIcon(string culture)
        {
            if (!string.IsNullOrEmpty(culture))
            {
                var regionInfos = CultureInfo
            .GetCultures(CultureTypes.AllCultures)
            .Where(ci => ci.ThreeLetterISOLanguageName != "ivl")
            .Where(ci => !ci.IsNeutralCulture)
            .Select(ci => new RegionInfo(ci.LCID))
            .Distinct()
            .ToList();
                RegionInfo r = regionInfos.Find(
                        region => region.Name.ToLower().Equals(culture.Split(new char[] { '-' }).LastOrDefault().ToLower()));
                return (r == null ? "" : r.Name.ToLower());
            }

            return "";
        }

        private IEnumerable<CultureInfo> GetAvailableCultures(long? id)
        {
            LanguageService languageService = new LanguageService();

            IEnumerable<Language> languages = languageService.Get();

            if (id.HasValue && id.Value > 0)
            {
                languages = languages.Where(x => x.LanguageID != id.Value);
            }

            IEnumerable<CultureInfo> cultureInfos = CultureInfo.GetCultures(CultureTypes.NeutralCultures)
               .OrderBy(x => x.EnglishName).Where(x => !languages.Any(y => (y.Culture + "-" + y.Icon).ToLower() == x.TextInfo.CultureName.ToLower()));

            return cultureInfos;
        }

        private void GetCultures(string culture, long? id)
        {
            List<SelectListItem> cultures = new List<SelectListItem>();

            if (!string.IsNullOrEmpty(culture))
            {
                IEnumerable<CultureInfo> cultureInfos = this.GetAvailableCultures(id);

                foreach (CultureInfo cultureInfo in cultureInfos)
                {
                    try
                    {
                        cultures.Add(new SelectListItem()
                        {
                            Selected = cultureInfo.TextInfo.CultureName.ToLower() == culture.ToLower(),
                            Text = cultureInfo.EnglishName,
                            Value = cultureInfo.TextInfo.CultureName.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(),
                        });
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }

            ViewBag.LanguageCultureList = cultures;
        }

        [HttpPost]
        public ActionResult Detail(Language model)
        {
            Dictionary<string, string> message = new Dictionary<string, string>();

            try
            {
                this.InitDefaultFunctions(model.LanguageID);

                if (this.IsValid(model))
                {
                    LanguageService languageService = new LanguageService(this.GetLoggedInUserID());

                    if (model.LanguageID == 0)
                    {
                        languageService.InsertLanguage(model);
                        message.Add("Success", model.Name + " " + FrameworkResources.Inserted);
                    }
                    else
                    {
                        languageService.UpdateLanguage(model);
                        message.Add("Success", model.Name + " " + FrameworkResources.Updated);
                    }

                    TempData["Message"] = message;

                    return RedirectToAction("Index");
                }

                ViewBag.ErrorText = FrameworkResources.UnableSave;
            }
            catch (Exception e)
            {
                ViewBag.ErrorText = e.Message;
            }

            string culture = model.Culture + "-" + model.Icon.ToUpper();
            this.GetCultures(culture, model.LanguageID);

            return View(model);
        }

        private bool IsValid(Language model)
        {
            bool isValid = true;

            LanguageService languageService = new LanguageService();

            Language existsLanguage = null;

            if (model.LanguageID == 0)
            {
                existsLanguage = languageService.GetBy(x => x.Name.ToLower() == model.Name.ToLower()).FirstOrDefault();
            }
            else
            {
                existsLanguage = languageService.GetBy(x => x.Name.ToLower() == model.Name.ToLower() && x.LanguageID != model.LanguageID).FirstOrDefault();
            }

            if (existsLanguage != null)
            {
                isValid = false;
                this.ModelState.AddModelError("Name", String.Format(FrameworkResources.ExistsFormat, FrameworkResources.Name));
            }

            if (!this.ModelState.IsValid)
            {
                isValid = false;
            }

            return isValid;
        }
        #endregion


    }
}