﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public class WeeklyReportsController : BaseReportController
    {
        public WeeklyReportsController()
            : base()
        {
            this.controllerName = "WeeklyReports";
            this.FriendlyReportModuleName = GeneralResources.WeeklyReports;
        }

        private UserService _serviceprop { get; set; }

        #region GeneralMethods

        private UserService GetModuleService()
        {
            if (this._serviceprop == null)
                this._serviceprop = new UserService(this.GetLoggedInUserID());

            return this._serviceprop;
        }

        #endregion

        protected override string GetEntityType()
        {
            return "ReportingDomainModel.User, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return this.GetModuleService().Get(filters, "Latest", 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            return this.GetModuleService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            this.GetRootPath();

            model.Title = this.FriendlyReportModuleName;
            //model.Styles = new List<string>() { "~/Content/weeklyreports/css" };
            model.Scripts = new List<string>() { "~/bundles/weeklyreports" };
            model.CustomFilterButtonsPartialName = "_ReportFilterButtonsPartial";

            model.Filters = new List<ListFilterItem>() {
                 new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "Date" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "Date",
                    Text = GeneralResources.Date,
                    ControlType = ControlType.DateRange,
                    Width = "100%",
                    Alignment = Alignment.Center
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "User" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "User",
                    Text = GeneralResources.User,
                    ControlType = ControlType.Custom,
                    CustomControlPartial="_UserMultiselectFilterPartial",
                    Width = "100%"
                }
            };

            return model;
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPreviewData(DateTime? startDate, DateTime? endDate, string selectedUsers, bool isPrint, string type)
        {
            FilterReportModel filterReportModel = this.GenerateBaseFilterReportModel(startDate, endDate, selectedUsers, null, isPrint, type);
            filterReportModel.EndDate = filterReportModel.StartDate.Value.AddDays(4);

            return this.BaseGetPreviewData(filterReportModel);
        }

        protected override List<HeaderReportModel> GetReportData(FilterReportModel filterReportModel)
        {
            List<HeaderReportModel> headerReportModelList = new List<HeaderReportModel>();

            UserService userService = new UserService();
            IEnumerable<User> users = userService.GetBy(x => x.IsActive && x.IsMember);

            if (filterReportModel.FilteredUserIds.Count() > 0)
                users = users.Where(x => filterReportModel.FilteredUserIds.Contains(x.UserID));

            ProjectTaskMemberDetailService projectTaskMemberDetailService = new ProjectTaskMemberDetailService();

            IEnumerable<ProjectTaskMemberDetail> projectTaskMemberDetailsInPeriod =
                projectTaskMemberDetailService.GetAllTaskMemberDetailsByStatusInPeriod(ProjectTaskMemberStatus.APPROVED, filterReportModel.StartDate.Value, filterReportModel.EndDate.Value);

            IEnumerable<ProjectTaskMemberDetail> previousProjectTaskMemberDetailsInPeriod =
                projectTaskMemberDetailService.GetAllTaskMemberDetailsByStatus(ProjectTaskMemberStatus.APPROVED).Where(x => x.ProjectTaskMember.Date < filterReportModel.StartDate.Value);

            if (projectTaskMemberDetailsInPeriod.Count() > 0)
            {
                foreach (User user in users.OrderBy(x => x.FirstName))
                {
                    IEnumerable<ProjectTaskMemberDetail> projectTaskMemberDetailsOfUsers = projectTaskMemberDetailsInPeriod.Where(x => x.ProjectTaskMember.AssigneeID == user.UserID);
                    IEnumerable<ProjectTaskMemberDetail> previousTaskMemberDetailsOfUsers = previousProjectTaskMemberDetailsInPeriod.Where(x => x.ProjectTaskMember.AssigneeID == user.UserID);

                    #region GenerateHeaderReportAndColumnHeader

                    HeaderReportModel headerReportModel = this.CreateHeaderReportModelAndSetBaseValue(filterReportModel.Type, filterReportModel.StartDate, filterReportModel.EndDate);
                    headerReportModel.Title = this.FriendlyReportModuleName;
                    headerReportModelList.Add(headerReportModel);
                    headerReportModel.RootFilterName = "- " + user.FullName.ToUpper() + " -";
                    List<float> columnWidthsList = new List<float>();

                    //GENERATE COLUMN HEADER
                    headerReportModel.HeaderColumns = new List<Tuple<string, ReportRowAlign>>();
                    //headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Project, ReportRowAlign.Left));
                    //columnWidthsList.Add(13);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.ProjectTask, ReportRowAlign.Left));
                    columnWidthsList.Add(25);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Description, ReportRowAlign.Left));
                    columnWidthsList.Add(30);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Status, ReportRowAlign.Center));
                    columnWidthsList.Add(11);

                    List<DateTime> showedDates = new List<DateTime>();

                    for (DateTime date = filterReportModel.StartDate.Value.Date; date <= filterReportModel.EndDate.Value.Date; date.AddDays(1))
                    {
                        headerReportModel.HeaderColumns.Add(Tuple.Create(date.ToString("ddd"), ReportRowAlign.Center));
                        showedDates.Add(date);

                        columnWidthsList.Add(8);
                        date = date.AddDays(1);
                    }

                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.SubtotalOfThisPeriod, ReportRowAlign.Center));
                    columnWidthsList.Add(11);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.SubtotalOfPrevPeriod, ReportRowAlign.Center));
                    columnWidthsList.Add(11);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.GrandTotalDurHourOfTask, ReportRowAlign.Center));
                    columnWidthsList.Add(11);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.EstHour, ReportRowAlign.Center));
                    columnWidthsList.Add(9);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.OvertimeHour, ReportRowAlign.Center));
                    columnWidthsList.Add(9);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.TargetDate, ReportRowAlign.Center));
                    columnWidthsList.Add(12);

                    headerReportModel.ContentColumnWidths = columnWidthsList.ToArray();

                    #endregion

                    List<Tuple<DateTime, decimal>> tupleDateDurList = new List<Tuple<DateTime, decimal>>();

                    if (projectTaskMemberDetailsOfUsers.Count() == 0)
                    {
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, FrameworkResources.NoDataAvailable, headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.GRAY));
                    }
                    else
                    {
                        IEnumerable<IEnumerable<ProjectTaskMemberDetail>> groupedDetailsByProjectTaskID = projectTaskMemberDetailsOfUsers.OrderBy(x => x.ProjectTask.Project.Name).ThenBy(
                            x => x.ProjectTask.ParentTaskID).GroupBy(x => x.ProjectTaskID);

                        string projectName = string.Empty;

                        foreach (IEnumerable<ProjectTaskMemberDetail> groupedDetailByProjectTaskID in groupedDetailsByProjectTaskID.OrderBy(x => x.FirstOrDefault().ProjectTask.ProjectID))
                        {
                            string currentProjectName = groupedDetailByProjectTaskID.FirstOrDefault().ProjectTask.Project.Name.ToUpper();

                            if (projectName != currentProjectName)
                            {
                                projectName = currentProjectName;
                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingName, currentProjectName, headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.BOLD));
                            }

                            foreach (IEnumerable<ProjectTaskMemberDetail> groupedDetailByDescription in groupedDetailByProjectTaskID.GroupBy(x => x.Description))
                            {
                                ProjectTaskMemberDetail detail = groupedDetailByDescription.OrderByDescending(x => x.ProjectTaskMember.Date).FirstOrDefault();

                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, (detail.ProjectTask.IsRoutine ? "(Routine) " : string.Empty) + detail.ProjectTask.Name));
                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, detail.DescriptionOrNoDesc, 0, ReportRowAlign.Left, detail.Description == null ? ReportFontStyle.ITALIC : ReportFontStyle.NORMAL));
                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, detail.StatusTask.Name.ToUpper(), 0, ReportRowAlign.Center, this.GetReportFontStyleForStatus(detail.StatusTask.Name)));

                                decimal totalDurHourThisPeriod = 0;

                                foreach (DateTime showedDate in showedDates)
                                {
                                    ProjectTaskMemberDetail detailOnDate = groupedDetailByDescription.Where(x => x.ProjectTaskMember.Date == showedDate).FirstOrDefault();

                                    decimal duration = detailOnDate != null ? detailOnDate.Duration : 0;
                                    totalDurHourThisPeriod += duration;

                                    Tuple<DateTime, decimal> tupleOfDate = tupleDateDurList.Where(x => x.Item1 == showedDate).FirstOrDefault();

                                    if (tupleOfDate == null)
                                        tupleDateDurList.Add(new Tuple<DateTime, decimal>(showedDate, duration));
                                    else
                                    {
                                        tupleDateDurList.Add(new Tuple<DateTime, decimal>(showedDate, duration + tupleOfDate.Item2));
                                        tupleDateDurList.Remove(tupleOfDate);
                                    }

                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(duration, false), 0, ReportRowAlign.Center));
                                }

                                decimal prevDur = 0;

                                if (string.IsNullOrEmpty(detail.Description))
                                    prevDur = previousTaskMemberDetailsOfUsers.Where(o => o.ProjectTaskID == detail.ProjectTaskID).Sum(o => o.Duration);
                                else
                                    prevDur = previousTaskMemberDetailsOfUsers.Where(o => o.Description == detail.Description && o.ProjectTaskID == detail.ProjectTaskID).Sum(o => o.Duration);

                                decimal totalDur = prevDur + totalDurHourThisPeriod;
                                decimal overHour = 0;
                                string estHourDesc = "-";
                                bool overTargetDate = showedDates.OrderByDescending(x => x.Date).FirstOrDefault() >= detail.ProjectTask.TargetDate;

                                if (!detail.ProjectTask.IsRoutine && detail.ProjectTask.EstHour.HasValue)
                                    overHour = totalDur - detail.ProjectTask.EstHour.Value;

                                if (overHour < 0)
                                    overHour = 0;

                                if (detail.ProjectTask.EstHour != null)
                                    estHourDesc = this.ConvertHourToString(detail.ProjectTask.EstHour) + (detail.ProjectTask.IsRoutine ? " /day" : string.Empty);

                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, totalDurHourThisPeriod.ToString(), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, prevDur.ToString(), 0, ReportRowAlign.Center));
                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, totalDur.ToString(), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, estHourDesc, 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(overHour, false), 0, ReportRowAlign.Center, overHour > 0 ? ReportFontStyle.REDBOLD : ReportFontStyle.BOLD));
                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertDateToSettingShortFormat(detail.ProjectTask.TargetDate, false), 0, ReportRowAlign.Center, overTargetDate ? ReportFontStyle.REDBOLD : ReportFontStyle.BOLD));
                            }
                        }
                    }

                    if (tupleDateDurList.Count() > 0)
                    {
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingFooter, GeneralResources.TotalWorkHourPerDay.ToUpper(), 3, ReportRowAlign.Right, ReportFontStyle.BOLD));

                        foreach (DateTime showedDate in showedDates)
                        {
                            Tuple<DateTime, decimal> tupleOfDate = tupleDateDurList.Where(x => x.Item1 == showedDate).FirstOrDefault();

                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingFooter, this.ConvertHourToString(tupleOfDate.Item2, true), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                        }

                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingFooter, "", 7, ReportRowAlign.Left, ReportFontStyle.BOLD));
                    }

                    DayOffDataService dayOffDataService = new DayOffDataService();
                    IEnumerable<DayOffData> dayOffDatas = dayOffDataService.GetBy(x => (x.UserID == null || x.UserID == user.UserID) && x.Status == "CONFIRMED");

                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingFooter, "NOTES", 3, ReportRowAlign.Right, ReportFontStyle.BOLD));

                    foreach (DateTime showedDate in showedDates)
                    {
                        DayOffData dayOffData = dayOffDatas.Where(x => x.Date == showedDate).OrderBy(x => x.UserID).FirstOrDefault();
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingFooter, dayOffData != null ? dayOffData.Type : string.Empty, 0, ReportRowAlign.Center, this.GetReportFontStyleForDayOff(dayOffData)));
                    }

                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingFooter, string.Empty, 6));
                }
            }

            return headerReportModelList;
        }
    }
}