﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    [Module(Value = "DailyScrums")]
    public class AdminDailyScrumsController : BaseAdminController
    {
        public static int indexDataNew = 1000;

        private DailyScrumService _serviceprop { get; set; }

        public AdminDailyScrumsController()
            : base()
        {
            this.controllerName = "AdminDailyScrums";
            this.FriendlyIndexModuleName = GeneralResources.DailyScrums;
            this.FriendlyDetailModuleName = GeneralResources.DailyScrum;
        }

        protected override string GetEntityType()
        {
            return "ReportingDomainModel.DailyScrum, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return this.GetModuleService().Get(filters, SortOption.LatestUpdated.ToString(), 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            return this.GetModuleService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        protected override List<ListColumn> GetColumns()
        {
            List<ListColumn> columns = new List<ListColumn>();

            columns.Add(new ListColumn()
            {
                DisplayMode = ListColumnDisplayMode.CheckboxWithSelectAll,
                Width = "1%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Date",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "DateWithDay",
                HeaderText = GeneralResources.Date,
                Width = "12%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Name",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "User.FullName",
                HeaderText = GeneralResources.Name,
                Width = "13%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Actions",
                DisplayMode = ListColumnDisplayMode.Custom,
                Width = "10%",
                CustomPartialName = "_DailyScrumActionPartial"
            });

            return columns;
        }

        protected override SelectList GetSortList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem() { Text = GeneralResources.LatestUpdated, Value = SortOption.LatestUpdated.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.LatestInserted, Value = SortOption.LatestInserted.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstUpdated, Value = SortOption.FirstUpdated.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstInserted, Value = SortOption.FirstInserted.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.DateDescending, Value = SortOption.Latest.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.DateAscending, Value = SortOption.Oldest.ToString(), Selected = false });

            return new SelectList(list, "Value", "Text");
        }

        #region GeneralMethods

        private DailyScrumService GetModuleService()
        {
            if (this._serviceprop == null)
                this._serviceprop = new DailyScrumService(this.GetLoggedInUserID());

            return this._serviceprop;
        }

        private string GetDisplayedData(DailyScrum model)
        {
            return this.FriendlyDetailModuleName;
        }

        private long GetModelID(DailyScrum model)
        {
            if (model != null)
                return model.DailyScrumID;

            return 0;
        }

        #endregion

        #region Initialize

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            model.Filters = new List<ListFilterItem>() {
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Property, Label = new string[] { "Date" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "Date",
                    Text = GeneralResources.Date,
                    ControlType = ControlType.DatePicker,
                    Width = "50%",
                    Alignment = Alignment.Right
                }
            };

            return model;
        }

        #endregion

        #region Index

        [AdminPrivilege]
        public override ActionResult Index(long? id)
        {
            return base.Index(id);
        }

        [HttpPost]
        public ActionResult Delete(long itemID)
        {
            string message = "";
            string status = "success";

            try
            {
                DailyScrum model = this.GetModuleService().GetBy(x => x.DailyScrumID == itemID).FirstOrDefault();

                this.GetModuleService().DeleteDailyScrum(model);
                message += this.GetDisplayedData(model) + " " + FrameworkResources.Deleted;
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message,
                    DisableHide = status == "success"
                });
        }

        #endregion

        #region Detail

        [AdminPrivilege(PrivilegeNames = new string[] { "INSERT", "EDIT" })]
        public ActionResult Detail(long? id)
        {
            DailyScrum model = this.SetUpModel(id);

            return View(model);
        }

        [AdminPrivilege]
        public ActionResult View(long? id)
        {
            ViewBag.CanEdit = false;
            DailyScrum model = this.SetUpModel(id);
            return View("Detail", model);
        }

        [HttpPost]
        public ActionResult Detail(DailyScrum model)
        {
            long modelID = this.GetModelID(model);

            ViewBag.Breadcrumbs = this.GetDetailBreadcrumbItems(modelID > 0 ? DetailMode.Edit : DetailMode.Insert);
            ViewBag.Title = (modelID > 0 ? FrameworkResources.Edit : FrameworkResources.Insert) + " " + GeneralResources.DailyScrum;

            Dictionary<string, string> message = new Dictionary<string, string>();

            try
            {
                if (this.IsValid(model))
                {
                    if (modelID == 0)
                    {
                        this.GetModuleService().InsertDailyScrum(model);
                        message.Add("Success", this.GetDisplayedData(model) + " " + FrameworkResources.Inserted);
                    }
                    else
                    {
                        this.GetModuleService().UpdateDailyScrum(model);
                        message.Add("Success", this.GetDisplayedData(model) + " " + FrameworkResources.Updated);
                    }

                    TempData["Message"] = message;
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorDetail = CommonUtilities.GetExceptionMessage(e);
            }

            this.InitDefaultFunctions(model.DailyScrumID);

            return View(model);
        }

        private DailyScrum SetUpModel(long? id)
        {
            DailyScrum model = null;

            if (id.HasValue && id.Value > 0)
            {
                model = this.GetModuleService().GetBy(x => x.DailyScrumID == id.Value).FirstOrDefault();
                model.AssigneeName = model.User.FullName;

                foreach (DailyScrumDetail detail in model.DailyScrumDetails)
                {
                    detail.ProjectTaskName = detail.ProjectTask.Name;
                    detail.ProjectName = detail.ProjectTask.Project.Name;
                }
            }
            else
            {
                model = new DailyScrum();
                model.Date = DateTime.Now;
            }

            this.InitDefaultFunctions(id);

            return model;
        }

        protected override void InitDefaultFunctions(long? id)
        {
            base.InitDefaultFunctions(id);

            ViewBag.Priorities = this.GetPriorities();
            ViewBag.DevSettings = this.DeveloperSettings;
        }

        private bool IsValid(DailyScrum model)
        {
            bool isValid = true;

            if (!this.ModelState.IsValid)
                isValid = false;

            if (model.AssigneeID <= 0)
            {
                isValid = false;
                this.ModelState.AddModelError("AssigneeName", string.Format(ErrorResources.FieldRequired, GeneralResources.Name));
            }

            if (model.DailyScrumDetails.Count() == 0)
            {
                isValid = false;
                ViewBag.ErrorTaskDetail = string.Format(ErrorResources.PleaseAddTasks);
            }
            else
            {
                foreach (DailyScrumDetail detail in model.DailyScrumDetails)
                {
                    if (detail.ProjectTaskID <= 0)
                    {
                        isValid = false;
                        ViewBag.ErrorTaskDetail = string.Format(ErrorResources.MustBeSelected, GeneralResources.ProjectTask);
                    }
                    else if (model.DailyScrumDetails.Count(x => x.ProjectTaskID == detail.ProjectTaskID && (!string.IsNullOrEmpty(x.Description) ? x.Description.Trim().ToLower() : null) == (!string.IsNullOrEmpty(detail.Description) ? detail.Description.Trim().ToLower() : null)) > 1)
                    {
                        isValid = false;
                        ViewBag.ErrorTaskDetail = string.Format(ErrorResources.DuplicateTask);
                    }
                }
            }

            if (isValid)
            {
                long modelID = this.GetModelID(model);

                DailyScrum existingData = this.GetModuleService().GetBy(x => x.DailyScrumID != modelID
                    && x.AssigneeID == model.AssigneeID && x.Date == model.Date).FirstOrDefault();

                if (existingData != null)
                {
                    isValid = false;
                    ViewBag.ErrorDetail = string.Format(ErrorResources.AlreadyExist, this.GetDisplayedData(model));
                }

            }

            return isValid;
        }

        #endregion

        [HttpPost]
        public ActionResult AddNewTaskRow(int addTaskCount)
        {
            List<object> result = new List<object>();

            List<string> priorities = this.GetPriorities();
            ViewBag.Priorities = priorities;

            ViewDataDictionary viewData = new ViewDataDictionary();
            viewData["CanEdit"] = this.CanEdit(this.controllerName);
            viewData["Priorities"] = priorities;
            viewData["DevSettings"] = this.DeveloperSettings;

            for (int i = 0; i < addTaskCount; i++)
            {
                indexDataNew = indexDataNew + 1;

                viewData["IndexData"] = indexDataNew;

                DailyScrumDetail newDetail = new DailyScrumDetail();
                newDetail.TargetDate = DateTime.Now;

                result.Add(Utilities.RenderPartialViewToString(this, "_DailyScrumDetailListPartial", newDetail, viewData));
            }

            return Json(new
            {
                Data = result
            });
        }
    }
}