﻿using ReportingBusinessService;
using ReportingBusinessService.Models;
using ReportingDomainModel;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web.Hosting;
using System.Web.Http;
using System.Xml.Serialization;

namespace ReportingWeb.Controllers
{
    public abstract class BaseAPIController : ApiController
    {
        public const string TOKEN_SECRET_KEY = "Reporting";

        protected GeneralSettings GeneralSettings { get; set; }
        protected DeveloperSettings DeveloperSettings { get; set; }
        protected Info Info { get; set; }

        //Simplyfy this since validity has been checked on other function???
        protected long GetTokenUserID()
        {
            long userID = 0;

            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("Authorization"))
            {
                string token = headers.GetValues("Authorization").First();
                string tokenValue = "";

                if (token.StartsWith("Bearer "))
                {
                    tokenValue = token.Substring(7);
                }

                string[] tokenComp = tokenValue.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);

                if (tokenComp.Count() != 3)
                {
                    //Invalid request
                }
                else
                {
                    byte[] data = Convert.FromBase64String(tokenComp[1]);
                    string payloadString = Encoding.UTF8.GetString(data);

                    long unixTimeNow = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

                    IWTPayload payload = (IWTPayload)JsonConvert.DeserializeObject(payloadString, typeof(IWTPayload));

                    if (payload.ExpiredTime > unixTimeNow)
                        userID = payload.UserID;
                }
            }

            return userID;
        }

        protected void PopulateSettings(long userID)
        {
            SettingsService service = new SettingsService(userID);

            //TODO:: get root user id
            List<Setting> settings = service.GetRelatedSettings(userID);

            Setting generalSetting = settings.Where(x => x.Type == "G").FirstOrDefault();

            if (generalSetting != null)
            {
                this.GeneralSettings = service.GetSettingObj(generalSetting) as GeneralSettings;
            }

            Setting developerSetting = settings.Where(x => x.Type == "D").FirstOrDefault();
            if (developerSetting != null)
            {
                this.DeveloperSettings = service.GetSettingObj(developerSetting) as DeveloperSettings;
            }

            Setting info = settings.Where(x => x.Type == "I").FirstOrDefault();
            if (info != null)
            {
                this.Info = service.GetSettingObj(info) as Info;
            }
        }

        protected HttpResponseMessage CreateResponse(object content)
        {
            return this.CreateResponse(content, "application/json");
        }

        protected HttpResponseMessage CreateResponse(object content, string contentType)
        {
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, content, new MediaTypeHeaderValue(contentType));
        }

        public object GetObjectFromXml(string filePath, Type type)
        {
            StreamReader reader = null;
            MemoryStream stream = null;
            object values = null;

            try
            {
                reader = new StreamReader(filePath);
                stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);

                writer.Write(reader.ReadToEnd());
                writer.Flush();
                stream.Position = 0;

                XmlSerializer serializer = new XmlSerializer(type);

                values = serializer.Deserialize(new StreamReader(stream));
            }
            catch (Exception excp)
            {
                throw excp;
            }
            finally
            {
                if (stream != null)
                    stream.Close();

                if (reader != null)
                    reader.Close();
            }

            return values;
        }

        protected string GetRootPath()
        {
            System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
            string url = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, Request.GetRequestContext().VirtualPathRoot);

            return url.EndsWith("/") ? url : url + "/";
        }
    }
}
