﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    [Module(Value = "ProjectTaskMembers")]
    public class AdminProjectTaskMembersController : BaseAdminController
    {
        public static int indexDataNew = 1000;

        private ProjectTaskMemberService _serviceprop { get; set; }

        public AdminProjectTaskMembersController()
            : base()
        {
            this.controllerName = "AdminProjectTaskMembers";
            this.FriendlyIndexModuleName = GeneralResources.ProjectTaskMembers;
            this.FriendlyDetailModuleName = GeneralResources.ProjectTaskMember;
        }

        protected override string GetEntityType()
        {
            return "ReportingDomainModel.ProjectTaskMember, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return this.GetModuleService().Get(filters, SortOption.LatestUpdated.ToString(), 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            return this.GetModuleService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        protected override List<ListColumn> GetColumns()
        {
            List<ListColumn> columns = new List<ListColumn>();

            columns.Add(new ListColumn()
            {
                DisplayMode = ListColumnDisplayMode.CheckboxWithSelectAll,
                Width = "1%"
            });

            columns.Add(new ListColumn()
            {
                DisplayMode = ListColumnDisplayMode.Custom,
                CustomPartialName = "_ProjectTaskMemberStatusMarkerPartial",
                Width = "8%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Date",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "DateWithDay",
                HeaderText = GeneralResources.Date,
                Width = "12%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Name",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "User.FullName",
                HeaderText = GeneralResources.Name,
                Width = "13%"
            });

            columns.Add(new ListColumn()
            {
                Name = "ReviewerName",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "ReviewerName",
                HeaderText = GeneralResources.ReviewedBy,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Actions",
                DisplayMode = ListColumnDisplayMode.Custom,
                Width = "10%",
                CustomPartialName = "_ProjectTaskMemberActionPartial"
            });

            return columns;
        }

        protected override SelectList GetSortList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem() { Text = GeneralResources.LatestUpdated, Value = SortOption.LatestUpdated.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.LatestInserted, Value = SortOption.LatestInserted.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstUpdated, Value = SortOption.FirstUpdated.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstInserted, Value = SortOption.FirstInserted.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.DateDescending, Value = SortOption.Latest.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.DateAscending, Value = SortOption.Oldest.ToString(), Selected = false });

            return new SelectList(list, "Value", "Text");
        }

        public override void AddDefaultFilter(List<FilterOption> filters)
        {
            base.AddDefaultFilter(filters);

            if (loggedInUser.Role != UserRole.SUPER.ToString())
                filters.Add(new FilterOption() { Type = FilterType.Custom, Label = new string[] { "SelfAndChildAssigneeID" }, Operator = FilterOperator.Equal, Value = loggedInUser.UserID, Unison = FilterUnison.And });
        }

        #region GeneralMethods

        private ProjectTaskMemberService GetModuleService()
        {
            if (this._serviceprop == null)
                this._serviceprop = new ProjectTaskMemberService(this.GetLoggedInUserID());

            return this._serviceprop;
        }

        private string GetDisplayedData(ProjectTaskMember model)
        {
            return this.FriendlyDetailModuleName;
        }

        private long GetModelID(ProjectTaskMember model)
        {
            if (model != null)
                return model.ProjectTaskMemberID;

            return 0;
        }

        #endregion

        #region Initialize

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            model.Filters = new List<ListFilterItem>() {
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Property, Label = new string[] { "Date" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "Date",
                    Text = GeneralResources.Date,
                    ControlType = ControlType.DatePicker,
                    Width = "50%",
                    Alignment = Alignment.Right
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "ProjectTaskName" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "ProjectTaskName",
                    Text = GeneralResources.ProjectTask,
                    ControlType = ControlType.Textbox,
                    Width = "50%",
                    Alignment = Alignment.Right
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "Description" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "Description",
                    Text = GeneralResources.Description,
                    ControlType = ControlType.Textbox,
                    Width = "50%",
                    Alignment = Alignment.Right
                }
            };

            return model;
        }

        #endregion

        #region Index

        [AdminPrivilege]
        public override ActionResult Index(long? id)
        {
            return base.Index(id);
        }

        [HttpPost]
        public ActionResult Delete(long itemID)
        {
            string message = "";
            string status = "success";

            try
            {
                ProjectTaskMember model = this.GetModuleService().GetBy(x => x.ProjectTaskMemberID == itemID).FirstOrDefault();

                this.GetModuleService().DeleteProjectTaskMember(model);
                message += this.GetDisplayedData(model) + " " + FrameworkResources.Deleted;
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message,
                    DisableHide = status == "success"
                });
        }

        #endregion

        #region Detail

        [AdminPrivilege(PrivilegeNames = new string[] { "INSERT", "EDIT" })]
        public ActionResult Detail(long? id)
        {
            ProjectTaskMember model = this.SetUpModel(id);

            return View(model);
        }

        [AdminPrivilege]
        public ActionResult View(long? id)
        {
            ViewBag.CanEdit = false;
            ProjectTaskMember model = this.SetUpModel(id);
            return View("Detail", model);
        }

        [HttpPost]
        public ActionResult Detail(ProjectTaskMember model)
        {
            long modelID = this.GetModelID(model);

            ViewBag.Breadcrumbs = this.GetDetailBreadcrumbItems(modelID > 0 ? DetailMode.Edit : DetailMode.Insert);
            ViewBag.Title = (modelID > 0 ? FrameworkResources.Edit : FrameworkResources.Insert) + " " + GeneralResources.ProjectTaskMember;

            Dictionary<string, string> message = new Dictionary<string, string>();

            try
            {
                if (this.IsValid(model))
                {
                    model.Status = ProjectTaskMemberStatus.WAITINGFORREVIEW.ToString();
                    model.ReviewedBy = null;

                    if (modelID == 0)
                    {
                        this.GetModuleService().InsertProjectTaskMember(model);
                        message.Add("Success", this.GetDisplayedData(model) + " " + FrameworkResources.Inserted);
                    }
                    else
                    {
                        this.GetModuleService().UpdateProjectTaskMember(model);
                        message.Add("Success", this.GetDisplayedData(model) + " " + FrameworkResources.Updated);
                    }

                    TempData["Message"] = message;
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorDetail = CommonUtilities.GetExceptionMessage(e);
            }

            this.InitDefaultFunctions(model.ProjectTaskMemberID);

            List<long> projectTaskIDs = model.ProjectTaskMemberDetails.Select(x => x.ProjectTaskID).ToList();

            ProjectTaskService service = new ProjectTaskService();
            List<ProjectTask> projectTasks = service.GetBy(x => projectTaskIDs.Contains(x.ProjectTaskID)).ToList();

            foreach (ProjectTaskMemberDetail detail in model.ProjectTaskMemberDetails)
            {
                detail.ProjectTask = projectTasks.Where(x => x.ProjectTaskID == detail.ProjectTaskID).FirstOrDefault();

                if (detail.ProjectTask != null)
                    detail.ProjectTaskName = detail.ProjectTask.Name;
            }

            return View(model);
        }

        private ProjectTaskMember SetUpModel(long? id)
        {
            ProjectTaskMember model = null;

            if (id.HasValue && id.Value > 0)
            {
                model = this.GetModuleService().GetBy(x => x.ProjectTaskMemberID == id.Value).FirstOrDefault();
                model.AssigneeName = model.User.FullName;

                foreach (ProjectTaskMemberDetail detail in model.ProjectTaskMemberDetails)
                {
                    detail.ProjectTaskName = detail.ProjectTask.Name;
                    detail.ProjectName = detail.ProjectTask.Project.Name;
                }
            }
            else
            {
                model = new ProjectTaskMember();
                model.Date = DateTime.Now;
                model.Status = ProjectTaskMemberStatus.WAITINGFORREVIEW.ToString();
            }

            this.InitDefaultFunctions(id);

            return model;
        }

        protected override void InitDefaultFunctions(long? id)
        {
            base.InitDefaultFunctions(id);

            IEnumerable<StatusTask> statusTasks = this.GetActiveStatusTask();
            ViewBag.StatusTasks = statusTasks;
        }

        private bool IsValid(ProjectTaskMember model)
        {
            bool isValid = true;

            if (!this.ModelState.IsValid)
                isValid = false;

            if (model.AssigneeID <= 0)
            {
                isValid = false;
                this.ModelState.AddModelError("AssigneeName", string.Format(ErrorResources.FieldRequired, GeneralResources.Name));
            }

            if (model.ProjectTaskMemberDetails.Count() == 0)
            {
                isValid = false;
                ViewBag.ErrorTaskDetail = string.Format(ErrorResources.PleaseAddTasks);
            }
            else
            {
                foreach (ProjectTaskMemberDetail detail in model.ProjectTaskMemberDetails)
                {
                    if (detail.ProjectTaskID <= 0)
                    {
                        isValid = false;
                        ViewBag.ErrorTaskDetail = string.Format(ErrorResources.MustBeSelected, GeneralResources.ProjectTask);
                    }
                    else if (model.ProjectTaskMemberDetails.Count(x => x.ProjectTaskID == detail.ProjectTaskID && (!string.IsNullOrEmpty(x.Description) ? x.Description.Trim().ToLower() : null) == (!string.IsNullOrEmpty(detail.Description) ? detail.Description.Trim().ToLower() : null)) > 1)
                    {
                        isValid = false;
                        ViewBag.ErrorTaskDetail = string.Format(ErrorResources.DuplicateTask);
                    }
                }
            }

            if (isValid)
            {
                long modelID = this.GetModelID(model);

                ProjectTaskMember existingData = this.GetModuleService().GetBy(x => x.ProjectTaskMemberID != modelID
                    && x.AssigneeID == model.AssigneeID && x.Date == model.Date).FirstOrDefault();

                if (existingData != null)
                {
                    isValid = false;
                    ViewBag.ErrorDetail = string.Format(ErrorResources.AlreadyExist, this.GetDisplayedData(model));
                }

            }

            return isValid;
        }

        #endregion

        [HttpPost]
        public ActionResult AddNewTaskRow(int addTaskCount)
        {
            List<object> result = new List<object>();

            IEnumerable<StatusTask> statusTasks = this.GetActiveStatusTask();
            ViewBag.StatusTasks = statusTasks;

            ViewDataDictionary viewData = new ViewDataDictionary();
            viewData["CanEdit"] = this.CanEdit(this.controllerName);
            viewData["StatusTasks"] = statusTasks;

            for (int i = 0; i < addTaskCount; i++)
            {
                indexDataNew = indexDataNew + 1;

                viewData["IndexData"] = indexDataNew;

                ProjectTaskMemberDetail newDetail = new ProjectTaskMemberDetail();
                newDetail.StatusTaskID = statusTasks.FirstOrDefault().StatusTaskID;

                result.Add(Utilities.RenderPartialViewToString(this, "_ProjectTaskMemberDetailListPartial", newDetail, viewData));
            }

            return Json(new
            {
                Data = result
            });
        }

        [HttpPost]
        public ContentResult ApproveReject(long itemId, string statusTask, string reason)
        {
            ProjectTaskMemberService service = new ProjectTaskMemberService(this.GetLoggedInUserID());
            ProjectTaskMember projectTaskMember = service.Get(new object[] { itemId });

            string status = "OK";
            string message = "";

            if (statusTask == ProjectTaskMemberStatus.REJECTED.ToString() && string.IsNullOrEmpty(reason.Trim()))
            {
                message = String.Format(ErrorResources.FieldRequired, GeneralResources.Reason);
                status = "FAIL";
            }
            else
            {

                if (projectTaskMember != null)
                {
                    projectTaskMember.Status = statusTask;
                    projectTaskMember.StatusReason = reason;

                    if (statusTask == ProjectTaskMemberStatus.APPROVED.ToString())
                        projectTaskMember.ReviewedBy = this.GetLoggedInUserID();
                    else
                        projectTaskMember.ReviewedBy = null;

                    try
                    {
                        service.UpdateProjectTaskMember(projectTaskMember);
                        service.Save();

                        message = this.FriendlyDetailModuleName + " " + (statusTask == ProjectTaskMemberStatus.APPROVED.ToString() ? GeneralResources.Approved : GeneralResources.Rejected);
                    }
                    catch (Exception excp)
                    {
                        message = "error: " + CommonUtilities.GetExceptionMessage(excp);
                        status = "FAIL";
                    }
                }
                else
                {
                    message = "Selected data not found";
                    status = "FAIL";
                }
            }

            var results = new
            {
                status = status,
                message = message,
            };

            JsonSerializerSettings settings = new JsonSerializerSettings();
            String jsonResult = JsonConvert.SerializeObject(results, settings);

            return this.Content(jsonResult, "application/json");
        }

        [HttpPost]
        public ActionResult CopyFromDailyScrum(DateTime date, long assigneeID)
        {
            string status = "OK";

            List<object> result = new List<object>();

            IEnumerable<StatusTask> statusTasks = this.GetActiveStatusTask();
            ViewBag.StatusTasks = statusTasks;

            DailyScrumService dailyScrumService = new DailyScrumService();
            DailyScrum dailyScrum = dailyScrumService.GetBy(x => x.AssigneeID == assigneeID && x.Date == date).FirstOrDefault();

            if (dailyScrum != null)
            {
                foreach (DailyScrumDetail detail in dailyScrum.DailyScrumDetails)
                {
                    indexDataNew = indexDataNew + 1;

                    ViewDataDictionary viewData = new ViewDataDictionary();
                    viewData["CanEdit"] = this.CanEdit(this.controllerName);
                    viewData["StatusTasks"] = statusTasks;
                    viewData["IndexData"] = indexDataNew;

                    ProjectTaskMemberDetail newDetail = new ProjectTaskMemberDetail();
                    newDetail.StatusTaskID = statusTasks.FirstOrDefault().StatusTaskID;

                    newDetail.ProjectTaskID = detail.ProjectTaskID;
                    newDetail.ProjectTaskName = detail.ProjectTask.Name;
                    newDetail.ProjectName = detail.ProjectTask.Project.Name;
                    newDetail.Description = detail.Description;

                    result.Add(Utilities.RenderPartialViewToString(this, "_ProjectTaskMemberDetailListPartial", newDetail, viewData));
                }
            }
            else
            {
                status = "NOTFOUND";
            }

            return Json(new
            {
                Status = status,
                Data = result
            });
        }

    }
}