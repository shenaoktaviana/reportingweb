﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ReportingWeb.Models;
using ReportingDomainModel;
using ReportingBusinessService;
using ReportingResources;
using System.Collections.Generic;
using System.Net.Mail;
using System.IO;
using DomainModelFramework;
using ReportingBusinessService.Models;
using System.Net.Mime;

namespace ReportingWeb.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #region Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            LoginViewModel model = new LoginViewModel();
            model.RememberMe = true;

            ViewBag.ReturnUrl = returnUrl;
            ViewBag.ActivePage = "Login";

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            SettingsService settingService = new SettingsService();

            Setting generalSettings = settingService.Get().Where(x => x.Type == "G").FirstOrDefault();
            GeneralSettings generalSetting = new GeneralSettings();

            if (generalSettings != null)
            {
                generalSetting = settingService.GetSettingObj(generalSettings) as GeneralSettings;
            }

            bool valid = true;

            if (string.IsNullOrEmpty(model.Email) && string.IsNullOrEmpty(model.Password))
            {
                valid = false;
                ViewBag.ErrorText = string.Format(FrameworkResources.FieldRequired, "Email and password");
                return View(model);
            }
            else if (string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Password))
            {
                valid = false;
                ViewBag.ErrorText = string.Format(FrameworkResources.FieldRequired, "Email");
                return View(model);
            }
            else if (!string.IsNullOrEmpty(model.Email) && string.IsNullOrEmpty(model.Password))
            {
                valid = false;
                ViewBag.ErrorText = string.Format(FrameworkResources.FieldRequired, "Password");
                return View(model);
            }

            if (!ValidateEmail(model.Email))
            {
                valid = false;
                ViewBag.ErrorText = string.Format(FrameworkResources.FieldInvalid, "Email");
                return View(model);
            }

            if (!ModelState.IsValid)
            {
                valid = false;
            }

            if (valid)
            {
                UserService userService = new UserService();
                User loggedInUser = await Task.Run(() =>
                {
                    return userService.ValidateUserPassword(model.Email, model.Password);
                });

                if (loggedInUser != null && loggedInUser.LoginType == "EMAIL")
                {
                    if (!generalSetting.IsUnderConstruction || loggedInUser.Role == UserRole.SUPER.ToString())
                    {
                        if (IsEmailVerified(loggedInUser, userService))
                        {
                            if (IsEmailActive(loggedInUser))
                            {
                                this.GenerateLoginCookie(new ApplicationUser(loggedInUser), model.RememberMe);

                                string url = returnUrl;
                                string defaultURL = Url.Content("~");

                                if (string.IsNullOrEmpty(url) || url.ToLower() == defaultURL.ToLower())
                                {
                                    url = this.GetDefaultReturnPageByUser(loggedInUser);
                                }

                                return Redirect(url);
                            }
                            else
                            {
                                ViewBag.ErrorText = ErrorResources.EmailNotActive;
                                return View(model);
                            }
                        }
                        else
                        {
                            ViewBag.ErrorText = ErrorResources.EmailNotVerified;
                            return View(model);
                        }
                    }
                    else
                    {
                        ViewBag.ErrorText = ErrorResources.SiteUnderMaintenance;
                        return View(model);
                    }
                }
            }
            ViewBag.ErrorText = ErrorResources.LoginFailed;
            return View(model);
        }

        private bool IsEmailVerified(User user, UserService service)
        {
            return user.IsVerified;
        }

        private bool IsEmailActive(User user)
        {
            return user.IsActive;
        }

        public bool ValidateEmail(string email)
        {
            try
            {
                MailAddress mailAddress = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private void GenerateLoginCookie(ApplicationUser user, bool isPersistent)
        {
            string fullName = user.FullName;

            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, fullName));
            if (string.IsNullOrEmpty(user.Email))
            {
                claims.Add(new Claim(ClaimTypes.Email, user.UserName));
            }
            else
            {
                claims.Add(new Claim(ClaimTypes.Email, user.Email));
            }
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id));

            var id = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie, fullName, user.Role);

            this.AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, id);
        }

        #endregion

        #region LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Login");
        }
        #endregion

        #region Forgot Password

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            SettingsService service = new SettingsService();
            Setting generalSettings = service.Get().Where(x => x.Type == "G").FirstOrDefault();
            GeneralSettings generalSetting = new GeneralSettings();

            if (generalSettings != null)
            {
                generalSetting = service.GetSettingObj(generalSettings) as GeneralSettings;
            }

            ForgotPasswordViewModel model = new ForgotPasswordViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ApplicationUser user = await UserManager.FindByEmailAsync(model.Email);
                    if (user == null)
                    {
                        // Don't reveal that the user does not exist or is not confirmed
                        return RedirectToAction("ForgotPasswordConfirmation", "Account");
                    }
                    else
                    {
                        string code = await UserManager.GenerateUserTokenAsync("ResetPassword", user.Id);

                        code = HttpUtility.UrlEncode(code);

                        string callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code, email = user.Email }, protocol: Request.Url.Scheme);

                        string path = this.Info.Theme.ToLower() + "_user_reset_password.html";
                        path = Server.MapPath("~/TemplateFiles/" + path);

                        StreamReader reader = new StreamReader(path);
                        string template = reader.ReadToEnd();

                        template = template.Replace("[FullName]", user.FullName);
                        template = template.Replace("[ResetLink]", callbackUrl);
                        template = template.Replace("[Email]", user.Email);

                        List<string> emailList = new List<string>();
                        emailList.Add(model.Email);

                        string subject = this.Info.Name + " - Reset Password";
                        string baseRootPath = this.GetRootPath();
                        Mailer mailer = new Mailer(this.DeveloperSettings.SMTPServerType, this.DeveloperSettings, this.GetLoggedInUserID());
                        mailer.SendEmailAsync(this.GeneralSettings.SenderEmail, emailList, subject, template, true, baseRootPath, this.Info, this.DeveloperSettings);

                        return RedirectToAction("ForgotPasswordConfirmation", "Account");
                    }
                }
                catch (Exception e)
                {
                    ViewBag.ErrorText = e.Message;
                }

            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            this.GetRootPath();
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string userId, string code, string email)
        {
            ResetPasswordViewModel model = new ResetPasswordViewModel();

            UserService userService = new UserService();

            long userID = 0;

            Int64.TryParse(userId, out userID);

            User user = userService.Get(new object[] { userID });

            if (user != null && user.Email == email)
            {
                model.Email = email;
            }

            model.Code = code;

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            bool isValid = true;
            if (!ModelState.IsValid)
            {
                isValid = false;
            }

            if (isValid)
            {
                ApplicationUser user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist
                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
                else
                {
                    string code = HttpUtility.UrlDecode(model.Code);

                    bool valid = await UserManager.VerifyUserTokenAsync(user.Id, "ResetPassword", code);

                    if (valid)
                    {
                        UserService service = new UserService(long.Parse(user.Id));

                        User dbUser = service.GetBy(x => x.Email == user.Email).FirstOrDefault();

                        service.UpdateUser(dbUser, true, false, model.Password);

                        return RedirectToAction("ResetPasswordConfirmation", "Account");
                    }
                }
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        #endregion

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register(string email)
        {
            SettingsService service = new SettingsService();
            Setting generalSettings = service.Get().Where(x => x.Type == "G").FirstOrDefault();
            GeneralSettings generalSetting = new GeneralSettings();

            if (generalSettings != null)
            {
                generalSetting = service.GetSettingObj(generalSettings) as GeneralSettings;
            }

            RegisterViewModel registerViewModel = new RegisterViewModel();
            registerViewModel.Email = email;
            return View(registerViewModel);
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.AgreeTC != true)
                {
                    ViewBag.AgreeTC = string.Format(ErrorResources.MustAgree, GeneralResources.TermsAndConditions);
                    return View(model);
                }

                UserService userService = new UserService();
                User oldUser = userService.GetBy(u => u.Email == model.Email).FirstOrDefault();
                if (oldUser != null)
                {
                    ViewBag.Error = string.Format(ErrorResources.Exists, FrameworkResources.Email);
                    return View(model);
                }

                User user = new User();
                user.Email = model.Email;
                user.Password = model.Password;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.Phone = model.Phone;
                user.Role = UserRole.ADMIN.ToString();
                user.IsActive = false;
                user.IsVerified = false;
                user.LoginType = "EMAIL";

                userService.InsertUser(user);

                var appuser = new ApplicationUser(user); //{ UserName = model.Email, Email = model.Email };
                //var result = await UserManager.CreateAsync(appuser, model.Password);
                //if (result.Succeeded)
                //{
                //    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                //    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                //    // Send an email with this link
                string code = await UserManager.GenerateEmailConfirmationTokenAsync(appuser.Id);
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = appuser.Id, code = code }, protocol: Request.Url.Scheme);
                //    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                string path = this.Info.Theme.ToLower() + "_user_confirm_email.html";
                path = Server.MapPath("~/TemplateFiles/" + path);

                StreamReader reader = new StreamReader(path);
                string template = reader.ReadToEnd();
                template = template.Replace("[FullName]", user.FirstName);
                template = template.Replace("[ConfirmLink]", callbackUrl);

                List<string> emailList = new List<string>();
                emailList.Add(model.Email);

                string subject = this.Info.Name + " - Verify Email Address";
                string baseRootPath = this.GetRootPath();
                Mailer mailer = new Mailer(this.DeveloperSettings.SMTPServerType, this.DeveloperSettings, this.GetLoggedInUserID());
                mailer.SendEmailAsync(this.GeneralSettings.SenderEmail, emailList, subject, template, true, baseRootPath, this.Info, this.DeveloperSettings);

                return RedirectToAction("RegisterSuccess");
                //}
                //AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/RegisterComplete
        [AllowAnonymous]
        public ActionResult RegisterSuccess()
        {
            return View();
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                UserService userService = new UserService();
                long userid = long.Parse(userId);
                User user = userService.GetBy(u => u.UserID == userid).FirstOrDefault();

                if (!user.IsVerified)
                {
                    user.IsActive = true;
                    user.IsVerified = true;

                    userService.UpdateUser(user, false);
                }

                var callbackUrl = Url.Action("Login");

                string path = this.Info.Theme.ToLower() + "_user_confirm_email.html";
                path = Server.MapPath("~/TemplateFiles/" + path);

                StreamReader reader = new StreamReader(path);
                string template = reader.ReadToEnd();
                template = template.Replace("[FullName]", user.FullName);
                template = template.Replace("[LoginLink]", callbackUrl);

                List<string> emailList = new List<string>();
                emailList.Add(user.Email);

                string subject = this.Info.Name + " - Congratulations";
                string baseRootPath = this.GetRootPath();
                Mailer mailer = new Mailer(this.DeveloperSettings.SMTPServerType, this.DeveloperSettings, this.GetLoggedInUserID());
                mailer.SendEmailAsync(this.GeneralSettings.SenderEmail, emailList, subject, template, true, baseRootPath, this.Info, this.DeveloperSettings);

            }
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction(this.GetDefaultReturnPage());
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}