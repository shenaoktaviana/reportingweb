﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    [Module(Value = "Users")]
    public class AdminUsersController : BaseAdminController
    {
        private UserService _serviceprop { get; set; }

        public AdminUsersController()
            : base()
        {
            this.controllerName = "AdminUsers";
            this.FriendlyIndexModuleName = GeneralResources.Users;
            this.FriendlyDetailModuleName = GeneralResources.User;
        }

        protected override string GetEntityType()
        {
            return "ReportingDomainModel.User, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return this.GetModuleService().Get(filters, "Latest", 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            return this.GetModuleService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        protected override List<ListColumn> GetColumns()
        {
            List<ListColumn> columns = new List<ListColumn>();

            columns.Add(new ListColumn()
            {
                DisplayMode = ListColumnDisplayMode.CheckboxWithSelectAll,
                Width = "1%"
            });

            columns.Add(new ListColumn()
            {
                DisplayMode = ListColumnDisplayMode.Custom,
                CustomPartialName = "_ActiveMarkerPartial",
                Width = "5%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Email",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "Email",
                HeaderText = GeneralResources.Email,
                Width = "5%"
            });

            columns.Add(new ListColumn()
            {
                Name = "FirstName",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "FirstName",
                HeaderText = GeneralResources.FirstName,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "LastName",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "LastName",
                HeaderText = GeneralResources.LastName,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Role",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "RoleNames",
                HeaderText = GeneralResources.Role,
                Width = "15%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Supervisor",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "SupervisorNames",
                HeaderText = GeneralResources.Supervisor,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "DateOfBirth",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "DateOfBirth",
                HeaderText = GeneralResources.Birthday,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Actions",
                DisplayMode = ListColumnDisplayMode.Custom,
                Width = "8%",
                CustomPartialName = "_UserActionPartial"
            });

            return columns;
        }

        protected override SelectList GetSortList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem() { Text = GeneralResources.LatestUpdated, Value = SortOption.LatestUpdated.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.LatestInserted, Value = SortOption.LatestInserted.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstUpdated, Value = SortOption.FirstUpdated.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstInserted, Value = SortOption.FirstInserted.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.NameAscending, Value = SortOption.NameAscending.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.NameDescending, Value = SortOption.NameDescending.ToString(), Selected = false });

            return new SelectList(list, "Value", "Text");
        }

        #region GeneralMethods

        private UserService GetModuleService()
        {
            if (this._serviceprop == null)
                this._serviceprop = new UserService(this.GetLoggedInUserID());

            return this._serviceprop;
        }

        private string GetDisplayedData(User model)
        {
            return model.FullName;
        }

        private long GetModelID(User model)
        {
            if (model != null)
                return model.UserID;
            
            return 0;
        }

        #endregion

        #region Initialize

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            model.Filters = new List<ListFilterItem>() {
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Property, Label = new string[] { "Code" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "Code",
                    Text = GeneralResources.Code,
                    ControlType = ControlType.Textbox,
                    Width = "100%",
                    Alignment = Alignment.Right
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Property, Label = new string[] { "FullName" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "FullName",
                    Text = GeneralResources.Name,
                    ControlType = ControlType.Textbox,
                    Width = "100%",
                    Alignment = Alignment.Right
                }
            };

            return model;
        }

        #endregion

        #region Index

        [AdminPrivilege]
        public override ActionResult Index(long? id)
        {
            return base.Index(id);
        }

        [HttpPost]
        public ActionResult Delete(long itemID)
        {
            string message = "";
            string status = "success";

            try
            {
                User model = this.GetModuleService().GetBy(x => x.UserID == itemID).FirstOrDefault();

                if (model.ProjectTaskMembers.Count() > 0)
                {
                    message = string.Format(ErrorResources.CannotDeleteAlreadyUsed, this.GetDisplayedData(model));
                    status = "fail";
                }
                else
                {
                    this.GetModuleService().DeleteUser(model);
                    message += this.GetDisplayedData(model) + " " + FrameworkResources.Deleted;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message,
                    DisableHide = status == "success"
                });
        }

        #endregion

        #region Detail

        [AdminPrivilege(PrivilegeNames = new string[] { "INSERT", "EDIT" })]
        public ActionResult Detail(long? id)
        {
            User model = this.SetUpModel(id);
            return View(model);
        }

        [AdminPrivilege]
        public ActionResult View(long? id)
        {
            ViewBag.CanEdit = false;

            User model = this.SetUpModel(id);
            return View("Detail", model);
        }

        [HttpPost]
        public ActionResult Detail(User model)
        {
            ViewBag.Breadcrumbs = this.GetDetailBreadcrumbItems(this.GetModelID(model) > 0 ? DetailMode.Edit : DetailMode.Insert);
            ViewBag.Title = (this.GetModelID(model) == 0 ? FrameworkResources.Edit : FrameworkResources.Insert) + " " + this.FriendlyDetailModuleName;

            Dictionary<string, string> message = new Dictionary<string, string>();

            try
            {
                if (this.IsValid(model))
                {
                    if (model.UserID == 0)
                    {
                        this.GetModuleService().InsertUser(model);
                        message.Add("Success", this.GetDisplayedData(model) + " " + FrameworkResources.Inserted);
                    }
                    else
                    {
                        this.GetModuleService().UpdateUser(model, false);
                        message.Add("Success", this.GetDisplayedData(model) + " " + FrameworkResources.Updated);
                    }

                    TempData["Message"] = message;
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorDetail = CommonUtilities.GetExceptionMessage(e);
            }

            this.InitDefaultFunctions(this.GetModelID(model));

            return View(model);
        }

        private User SetUpModel(long? id)
        {
            User model = null;

            if (id.HasValue && id.Value > 0)
                model = this.GetModuleService().GetBy(x => x.UserID == id.Value).FirstOrDefault();
            else
                model = new User();

            this.InitDefaultFunctions(id);
            return model;
        }

        private bool IsValid(User model)
        {
            bool isValid = true;

            if (!this.ModelState.IsValid)
                isValid = false;

            if (isValid)
            {
                User existingData = this.GetModuleService().GetBy(x => x.UserID != this.GetModelID(model) &&
                    x.Email.ToLower() == model.Email.ToLower()).FirstOrDefault();

                if (existingData != null)
                {
                    isValid = false;
                    ViewBag.ErrorDetail = string.Format(ErrorResources.AlreadyExist, model.Email);
                }
                else
                {
                    existingData = this.GetModuleService().GetBy(x => x.UserID != this.GetModelID(model) &&
                        x.FullName.ToLower() == model.FullName.ToLower()).FirstOrDefault();

                    if (existingData != null)
                    {
                        isValid = false;
                        ViewBag.ErrorDetail = string.Format(ErrorResources.AlreadyExist, model.FullName);
                    }
                }
            }

            return isValid;
        }

        #endregion

        #region Action

        [HttpPost]
        public ActionResult ChangeStatus(List<long> checkedIds, bool activeInactive)
        {
            string message = "";
            string status = "success";

            try
            {
                IEnumerable<User> items = this.GetModuleService().GetBy(x => checkedIds.Contains(x.UserID));

                string itemNames = "";

                foreach (User item in items)
                {
                    item.IsActive = activeInactive;
                    itemNames += item.FullName + ", ";
                }

                if (itemNames.Length > 2)
                    itemNames = itemNames.Substring(0, itemNames.Length - 2);

                items = this.GetModuleService().UpdateUsers(items);

                if (activeInactive)
                {
                    message = itemNames + " " + FrameworkResources.Activated;
                }
                else
                {
                    message = itemNames + " " + FrameworkResources.Deactivated;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message
                });
        }

        #endregion
    }
}