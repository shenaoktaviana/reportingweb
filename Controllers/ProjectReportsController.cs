﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public class ProjectReportsController : BaseReportController
    {
        public ProjectReportsController()
            : base()
        {
            this.controllerName = "ProjectReports";
            this.FriendlyReportModuleName = GeneralResources.ProjectReports;
        }

        private ProjectService _serviceprop { get; set; }

        #region GeneralMethods

        private ProjectService GetModuleService()
        {
            if (this._serviceprop == null)
                this._serviceprop = new ProjectService(this.GetLoggedInUserID());

            return this._serviceprop;
        }

        #endregion

        protected override string GetEntityType()
        {
            return "ReportingDomainModel.Project, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return this.GetModuleService().Get(filters, "Latest", 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            return this.GetModuleService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            this.GetRootPath();

            model.Title = this.FriendlyReportModuleName;
            //model.Styles = new List<string>() { "~/Content/weeklyreports/css" };
            model.Scripts = new List<string>() { "~/bundles/projectreports" };
            model.CustomFilterButtonsPartialName = "_ReportFilterButtonsPartial";

            model.Filters = new List<ListFilterItem>() {
                 new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "Date" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "Date",
                    Text = GeneralResources.Date,
                    ControlType = ControlType.DateRange,
                    Width = "100%",
                    Alignment = Alignment.Center
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "Project" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "Project",
                    Text = GeneralResources.Project,
                    ControlType = ControlType.Custom,
                    CustomControlPartial="_ProjectMultiselectFilterPartial",
                    Width = "100%"
                }
            };

            return model;
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPreviewData(DateTime? startDate, DateTime? endDate, bool isPrint, string selectedProjects, string type)
        {
            FilterReportModel filterReportModel = this.GenerateBaseFilterReportModel(startDate, endDate, null, selectedProjects, isPrint, type);

            return this.BaseGetPreviewData(filterReportModel, false);
        }

        protected override List<HeaderReportModel> GetReportData(FilterReportModel filterReportModel)
        {
            List<HeaderReportModel> headerReportModelList = new List<HeaderReportModel>();

            ProjectService projectService = new ProjectService();
            IEnumerable<Project> projects = projectService.GetBy(x => filterReportModel.FilteredProjectIds.Count() > 0 ? filterReportModel.FilteredProjectIds.Contains(x.ProjectID) : true).OrderBy(x => x.Name);

            foreach (Project project in projects.Where(x => x.IsActive))
            {
                IEnumerable<IEnumerable<ProjectTask>> childTasksGroupByParent = project.ProjectTasks.Where(x => x.IsActive && x.ChildProjectTasks.Count() == 0).GroupBy(
                    x => x.ReportParentTaskName).OrderBy(x => x.FirstOrDefault().ReportParentTaskName);

                if (childTasksGroupByParent.Count() > 0)
                {
                    #region GenerateHeaderReportAndColumnHeader

                    HeaderReportModel headerReportModel = this.CreateHeaderReportModelAndSetBaseValue(filterReportModel.Type, filterReportModel.StartDate, filterReportModel.EndDate);
                    headerReportModel.Title = this.FriendlyReportModuleName;
                    headerReportModelList.Add(headerReportModel);
                    headerReportModel.RootFilterName = "- " + project.Name.ToUpper() + " -";

                    //GENERATE COLUMN HEADER
                    List<float> columnWidthsList = new List<float>();
                    headerReportModel.HeaderColumns = new List<Tuple<string, ReportRowAlign>>();
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.ProjectTask.ToUpper(), ReportRowAlign.Left));
                    columnWidthsList.Add(35);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Status.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(7);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.SubtotalOfThisPeriod.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.SubtotalOfPrevPeriod.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.GrandTotalDurHourOfTask.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.EstHour.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.OvertimeHour.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.TargetDate.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Assignee.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(12);
                    headerReportModel.ContentColumnWidths = columnWidthsList.ToArray();

                    #endregion

                    foreach (IEnumerable<ProjectTask> childTaskInCurrentParent in childTasksGroupByParent)
                    {
                        //if (childTaskInCurrentParent.Count() == 0)
                        //{
                        //    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, "     " + FrameworkResources.NoDataAvailable, headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.BOLD));
                        //}

                        bool isParentRendered = false;

                        foreach (ProjectTask childTask in childTaskInCurrentParent)
                        {
                            decimal subtotalThisPeriodGroup = childTask.GetSubtotalOfPeriod(filterReportModel.StartDate, filterReportModel.EndDate);
                            decimal subtotalPrevPeriodGroup = childTask.GetSubtotalOfPreviousPeriod(filterReportModel.StartDate);
                            decimal grandTotalGroup = subtotalThisPeriodGroup + subtotalPrevPeriodGroup;

                            if (subtotalThisPeriodGroup > 0)
                            {
                                if (!isParentRendered)
                                {
                                    ProjectTask defaultChildTask = childTaskInCurrentParent.FirstOrDefault();
                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.ParentGroupingName, defaultChildTask.ReportParentTaskName, headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.BOLD));

                                    isParentRendered = true;
                                }

                                headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, "     " + childTask.NameWithIsRoutine, 9, ReportRowAlign.Left, ReportFontStyle.BOLD));
                                //headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, this.ConvertHourToString(subtotalThisPeriodGroup, false), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                //headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, this.ConvertHourToString(subtotalPrevPeriodGroup, false), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                //headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, this.ConvertHourToString(grandTotalGroup, false), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                //headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, string.Empty, 4, ReportRowAlign.Left, ReportFontStyle.BOLD));

                                IEnumerable<IEnumerable<ProjectTaskMemberDetail>> taskMemberDetailGroupedByDesc = childTask.ProjectTaskMemberDetails.OrderBy(x => x.Description).GroupBy(x => x.Description);

                                if (taskMemberDetailGroupedByDesc.Count() == 0)
                                {
                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, "         " + FrameworkResources.NoDataAvailable, headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.ITALIC));
                                }
                                else
                                {
                                    decimal sumSubtotalPrevPeriod = 0;

                                    foreach (IEnumerable<ProjectTaskMemberDetail> taskMemberDetailsPerDesc in taskMemberDetailGroupedByDesc)
                                    {
                                        ProjectTaskMemberDetail defaultTaskMemberDetail = taskMemberDetailsPerDesc.FirstOrDefault();

                                        string status = childTask.GetLatestStatusInPeriodByParentTaskID(filterReportModel.StartDate, filterReportModel.EndDate, false, defaultTaskMemberDetail.Description);
                                        decimal subtotalThisPeriod = childTask.GetSubtotalOfPeriod(filterReportModel.StartDate, filterReportModel.EndDate, false, defaultTaskMemberDetail.Description);
                                        decimal subtotalPrevPeriod = childTask.GetSubtotalOfPreviousPeriod(filterReportModel.StartDate, false, defaultTaskMemberDetail.Description);
                                        decimal grandTotal = subtotalThisPeriod + subtotalPrevPeriod;
                                        decimal overtimeHour = 0;

                                        if (!childTask.IsRoutine)
                                            overtimeHour = childTask.EstHour.HasValue ? childTask.EstHour.Value - grandTotal : 0;

                                        if (subtotalThisPeriod > 0)
                                        {
                                            string assigneeNames = childTask.GetAssigneeNamesInPeriod(filterReportModel.StartDate, filterReportModel.EndDate, false, defaultTaskMemberDetail.Description);

                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, "       - " + defaultTaskMemberDetail.DescriptionOrNoDesc, 0, ReportRowAlign.Left, defaultTaskMemberDetail.Description != null ? ReportFontStyle.NORMAL : ReportFontStyle.ITALIC));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, status, 0, ReportRowAlign.Center, GetReportFontStyleForStatus(status, false)));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(subtotalThisPeriod, false), 0, ReportRowAlign.Center));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(subtotalPrevPeriod, false), 0, ReportRowAlign.Center));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(grandTotal, false), 0, ReportRowAlign.Center));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(childTask.EstHour, false) + (childTask.IsRoutine ? "/day" : string.Empty), 0, ReportRowAlign.Center));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(overtimeHour, false), 0, ReportRowAlign.Center, GetReportFontStyleOfMinusValue(overtimeHour)));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertDateToSettingShortFormat(childTask.TargetDate, false), 0, ReportRowAlign.Center));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, assigneeNames));

                                            sumSubtotalPrevPeriod += subtotalPrevPeriod;
                                        }
                                    }

                                    decimal diffHour = subtotalPrevPeriodGroup - sumSubtotalPrevPeriod;

                                    if (diffHour > 0)
                                    {
                                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, "       - " + "Other tasks in previous duration", 3, ReportRowAlign.Left, ReportFontStyle.ITALIC));
                                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(diffHour, false), 0, ReportRowAlign.Center));
                                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(diffHour, false), 0, ReportRowAlign.Center));
                                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, string.Empty, 5));
                                    }

                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, "     " + "Total " + childTask.NameWithIsRoutine, 2, ReportRowAlign.Left, ReportFontStyle.BOLD));
                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, this.ConvertHourToString(subtotalThisPeriodGroup, false), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, this.ConvertHourToString(subtotalPrevPeriodGroup, false), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, this.ConvertHourToString(grandTotalGroup, false), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, string.Empty, 4, ReportRowAlign.Left, ReportFontStyle.BOLD));
                                }
                            }
                        }
                    }
                }
            }

            return headerReportModelList;
        }
    }
}