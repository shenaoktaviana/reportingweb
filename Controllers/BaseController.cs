﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingBusinessService.Models;
using ReportingDomainModel;
using ReportingResources;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml.Serialization;

namespace ReportingWeb.Controllers
{
    public abstract class BaseController : Controller
    {
        protected static string LANG = "id-ID";
        protected GeneralSettings GeneralSettings { get; set; }
        protected DeveloperSettings DeveloperSettings { get; set; }
        protected Info Info { get; set; }
        protected Navigation FooterNavItems { get; set; }

        protected User loggedInUser;

        public BaseController()
        {
            string footerFilePath = HostingEnvironment.MapPath("~/App_Data/Navigation/footer.xml");
            this.FooterNavItems = CommonUtilities.GetObjectFromXml(footerFilePath, typeof(Navigation)) as Navigation;
            ViewBag.FooterNavItems = this.FooterNavItems;

            Language language = new LanguageService().GetBy(o => o.IsDefault).FirstOrDefault();
            if (language != null)
            {
                ViewBag.DefaultLanguage = language.Culture;
            }

            //Note: Must move settings initialization to Init not Constructor
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            UserService userService = new UserService();
            this.loggedInUser = userService.Get(new object[] { this.GetLoggedInUserID() });

            ViewBag.IsLoginSuper = this.IsLoginSuper();
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            try
            {
                long userID = this.GetLoggedInUserID();
                User user = this.GetLoggedInUser();

                SettingsService service = new SettingsService(userID);
                List<Setting> settings = service.GetRelatedSettings(userID);

                Setting generalSetting = settings.Where(x => x.Type == "G").FirstOrDefault();

                if (generalSetting != null)
                {
                    this.GeneralSettings = service.GetSettingObj(generalSetting) as GeneralSettings;
                    ViewBag.GeneralSettings = this.GeneralSettings;
                }

                Setting developerSetting = settings.Where(x => x.Type == "D").FirstOrDefault();
                if (developerSetting != null)
                {
                    this.DeveloperSettings = service.GetSettingObj(developerSetting) as DeveloperSettings;
                    ViewBag.DeveloperSettings = this.DeveloperSettings;
                }

                Setting info = settings.Where(x => x.Type == "I").FirstOrDefault();
                if (info != null)
                {
                    this.Info = service.GetSettingObj(info) as Info;
                    ViewBag.Info = this.Info;
                }

                Language language = new LanguageService().GetBy(x => x.IsDefault).FirstOrDefault();

                LANG = (string)RouteData.Values["lang"] ?? language.Culture;

                CultureInfo culture = new CultureInfo(LANG);
                culture.DateTimeFormat = new CultureInfo("en-US").DateTimeFormat;

                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;
            }
            catch (Exception)
            {
                throw new NotSupportedException(String.Format(ErrorResources.LanguageNotSupported, LANG));
            }


            ViewBag.CurrentLanguage = LANG;
            ViewBag.ActiveLanguages = new LanguageService().GetBy(x => x.IsActive).OrderByDescending(x => x.IsDefault).ThenBy(x => x.Name);

            HttpRequestBase request = requestContext.HttpContext.Request;
            UrlHelper url = new UrlHelper(requestContext);
            ViewBag.RootBasePath = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, url.Content("~"));
            ViewBag.DefaultPath = this.GetDefaultReturnPage();
        }

        public virtual long GetLoggedInUserID()
        {
            if (this.User != null && this.User.Identity.IsAuthenticated)
            {
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;

                string userId = identity.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier)
                    .Select(x => x.Value).FirstOrDefault();

                long numUserId = 0;

                long.TryParse(userId, out numUserId);

                return numUserId;
            }

            return 0;
        }

        public virtual User GetLoggedInUser()
        {
            UserService userService = new UserService();
            long userID = this.GetLoggedInUserID();

            return userService.Get(new object[] { userID });
        }

        public object GetObjectFromXml(string filePath, Type type)
        {
            StreamReader reader = null;
            MemoryStream stream = null;
            object values = null;

            try
            {
                reader = new StreamReader(filePath);
                stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);

                writer.Write(reader.ReadToEnd());
                writer.Flush();
                stream.Position = 0;

                XmlSerializer serializer = new XmlSerializer(type);

                values = serializer.Deserialize(new StreamReader(stream));
            }
            catch (Exception excp)
            {
                throw excp;
            }
            finally
            {
                if (stream != null)
                    stream.Close();

                if (reader != null)
                    reader.Close();
            }

            return values;
        }

        protected void InsertSearchLabel(List<SelectListItem> list, string labelName, string defaultValue)
        {
            list.Insert(0, new SelectListItem()
            {
                Text = String.Format(FrameworkResources.SelectOption, labelName),
                Value = defaultValue
            });
        }

        protected bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        protected bool ResizeImage(string newFileName, string sourceFile, int width, int height)
        {
            try
            {
                FileInfo imageInfo = new FileInfo(sourceFile);
                using (Image img = Image.FromFile(sourceFile))
                {
                    if (height == 0)
                        height = width * img.Height / img.Width;

                    if (width == 0)
                        width = height * img.Width / img.Height;

                    Image image = new Bitmap(width, height);

                    Graphics g = Graphics.FromImage(image);
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    Rectangle rect = new Rectangle(0, 0, width, height);
                    g.DrawImage(img, rect);

                    image.Save(newFileName, img.RawFormat);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected string ReEncodeImage(Image oldImage, string savePath, string fileName, string extension)
        {
            if (this.CreateFolderIfNeeded(savePath))
            {
                try
                {
                    string[] fileNames = fileName
                        .Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries)
                        .ToArray();

                    ImageFormat currentImgFormat = ImageFormat.Jpeg;
                    switch (extension)
                    {
                        case ".jpg":
                        case ".jpeg":
                            currentImgFormat = ImageFormat.Jpeg;
                            break;
                        case ".png":
                            currentImgFormat = ImageFormat.Png;
                            break;
                    }

                    string newFileName = string.Join(".", fileNames.Take(fileNames.Length - 1).ToArray());
                    string tempGuid = Guid.NewGuid().ToString("N");
                    newFileName = string.Format("{0}_{1}{2}", newFileName, tempGuid, extension);

                    using (var tempImage = new Bitmap(oldImage))
                    {
                        //System.IO.File.WriteAllText(Path.Combine(savePath, newFileName), "empty");

                        tempImage.Save(Path.Combine(savePath, newFileName), currentImgFormat);
                    }

                    return newFileName;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return "";
        }

        [HttpPost]
        public ActionResult UploadImages(IEnumerable<HttpPostedFileBase> files, string imageType, string path, string imageName, string controllerName, string actionName)
        {
            bool isValid = false;

            string message = FrameworkResources.UploadFailed;

            Dictionary<string, string> messages = new Dictionary<string, string>();

            try
            {
                if (files.Count() > 0)
                {
                    IEnumerable<HttpPostedFileBase> nonPdfFiles = files.Where(x => !x.ContentType.Equals("application/pdf"));

                    if (nonPdfFiles.Count() > 0)
                    {
                        string type = string.Empty;

                        foreach (HttpPostedFileBase file in nonPdfFiles)
                        {
                            type += file.ContentType + "; ";
                        }

                        type = type.Substring(0, type.Length - 2);

                        message = string.Format(FrameworkResources.OnlyImageSupportedFormat, type);
                    }
                    else
                    {
                        int count = 1;
                        string fileNames = string.Empty;

                        foreach (HttpPostedFileBase file in files)
                        {
                            path = Server.MapPath("~/Images/" + path);

                            if (string.IsNullOrEmpty(imageName))
                            {
                                imageName = Guid.NewGuid().ToString();
                            }

                            if (files.Count() > 1)
                            {
                                imageName += count;
                            }

                            imageName += Path.GetExtension(file.FileName);

                            CommonUtilities.CreateFolderIfNeeded(path);

                            file.SaveAs(Path.Combine(path, imageName));

                            fileNames += imageName + ", ";
                            count++;
                        }

                        fileNames = fileNames.Substring(0, fileNames.Length - 2);

                        isValid = true;
                        message = string.Format(GeneralResources.FileUploadedFormat, fileNames);
                    }
                }

            }
            catch (Exception ex)
            {
                isValid = false;
                message = FrameworkResources.UploadFailed + ":" + ex.Message;
            }

            if (isValid)
            {
                messages.Add("Success", message);
            }
            else
            {
                messages.Add("Error", message);
            }

            TempData["Message"] = messages;

            return RedirectToAction(actionName, controllerName);
        }

        protected string GetRootPath()
        {
            string rootPath = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            ViewBag.Rootpath = rootPath;

            return rootPath;
        }

        protected string GetDefaultReturnPage()
        {
            string result = "/Login";
            long userID = this.GetLoggedInUserID();
            if (userID != 0)
            {
                UserService userService = new UserService();

                User loggedInUser = userService.Get(new object[] { userID });

                result = this.GetDefaultReturnPageByUser(loggedInUser);
            }
            return result;
        }

        protected string GetDefaultReturnPageByUser(User loggedInUser)
        {
            string result = Url.Action("Index", "AdminSettings");
            string moduleName = "";

            if (loggedInUser != null)
            {
                if (loggedInUser.Role == UserRole.SUPER.ToString())
                {
                    moduleName = "AdminSettings";
                }
                else
                {
                    UserModulePrivilege allowedModule = loggedInUser.UserModulePrivileges.FirstOrDefault(x => x.IsAllowed);

                    if (allowedModule != null)
                    {
                        moduleName = allowedModule.ModulePrivilege.Module.Name;
                    }
                    result = Url.Action("Index", moduleName);
                }

                ViewBag.DefaultMenu = moduleName;
            }

            return result;
        }

        protected List<string> GetGenderOptions()
        {
            List<string> genderList = new List<string>();

            genderList.Add("Male");
            genderList.Add("Female");

            ViewBag.GenderList = genderList;

            return genderList;
        }

        protected void GenerateAlertViewbag()
        {
            Dictionary<string, string> message = TempData["Message"] as Dictionary<string, string>;

            if (message != null)
            {
                ViewBag.InfoText = message.ContainsKey("Info") ? message["Info"] : string.Empty;
                ViewBag.SuccessText = message.ContainsKey("Success") ? message["Success"] : string.Empty;
                ViewBag.ErrorText = message.ContainsKey("Error") ? message["Error"] : string.Empty;
                ViewBag.InfoDetail = message.ContainsKey("InfoDetail") ? message["InfoDetail"] : string.Empty;
                ViewBag.SuccessDetail = message.ContainsKey("SuccessDetail") ? message["SuccessDetail"] : string.Empty;
                ViewBag.ErrorDetail = message.ContainsKey("ErrorDetail") ? message["ErrorDetail"] : string.Empty;
            }
        }

        protected bool IsLoginSuper()
        {
            return this.loggedInUser != null ?
                this.loggedInUser.Role == UserRole.SUPER.ToString() : false;
        }

        protected virtual void CreateLog(LogPriority logPriority, string title, string value, string entityType = null, long? entityID = null)
        {
            LogService logService = new LogService(this.GetLoggedInUserID(), this.DeveloperSettings, logPriority);
            logService.InsertLog(logPriority, title, value, entityType, entityID);
        }

        #region GetData

        protected IEnumerable<StatusTask> GetActiveStatusTask()
        {
            StatusTaskService statusTaskService = new StatusTaskService();
            IEnumerable<StatusTask> statusTasks = statusTaskService.GetAllActiveStatus().OrderBy(x => x.Priority);
            return statusTasks;
        }

        protected List<string> GetPriorities()
        {
            List<string> priorities = new List<string>();

            priorities.Add("MEDIUM");
            priorities.Add("HIGH");
            priorities.Add("LOW");

            return priorities;
        }

        protected IEnumerable<JobRole> GetActiveJobRoles()
        {
            JobRoleService jobRoleService = new JobRoleService();
            IEnumerable<JobRole> jobRoles = jobRoleService.GetAllActiveJobRoles();
            return jobRoles;
        }

        [HttpGet]
        public ContentResult GetActiveMembers(string term, string filterType = "ALL")
        {
            List<FilterOption> filters = new List<FilterOption>();

            FilterOption filterValue = new FilterOption()
            {
                Label = new string[] { "FullName" },
                Operator = FilterOperator.Like,
                Type = FilterType.Custom,
                Unison = FilterUnison.And,
                Value = term
            };
            filters.Add(filterValue);

            FilterOption filterIsActive = new FilterOption()
            {
                Label = new string[] { "IsActive" },
                Operator = FilterOperator.Equal,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = true
            };
            filters.Add(filterIsActive);

            FilterOption filterIsMember = new FilterOption()
            {
                Label = new string[] { "IsMember" },
                Operator = FilterOperator.Equal,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = true
            };
            filters.Add(filterIsMember);

            UserService service = new UserService();
            IEnumerable<User> items = service.Get(filters).OrderBy(x => x.FirstName).Take(this.DeveloperSettings.AutoCompletePageSize);

            var results = items.Select(x => new
            {
                itemID = x.UserID,
                itemName = x.FullName,
            }).ToArray();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            String jsonResult = JsonConvert.SerializeObject(results, settings);

            return this.Content(jsonResult, "application/json");
        }

        [HttpGet]
        public ContentResult GetActiveProjectTasks(string term, bool includeTaskDesc)
        {
            //Generate filter
            List<FilterOption> filters = new List<FilterOption>();

            if (includeTaskDesc)
            {
                FilterOption filterDesc = new FilterOption()
                {
                    Label = new string[] { "TaskNameAndDescription" },
                    Operator = FilterOperator.Like,
                    Type = FilterType.Custom,
                    Unison = FilterUnison.And,
                    Value = term
                };
                filters.Add(filterDesc);
            }
            else
            {
                FilterOption filterValue = new FilterOption()
                {
                    Label = new string[] { "Name" },
                    Operator = FilterOperator.Like,
                    Type = FilterType.Property,
                    Unison = FilterUnison.And,
                    Value = term
                };
                filters.Add(filterValue);

            }

            FilterOption filterIsActive = new FilterOption()
            {
                Label = new string[] { "IsActive" },
                Operator = FilterOperator.Equal,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = true
            };
            filters.Add(filterIsActive);

            FilterOption filterNoChild = new FilterOption()
            {
                Label = new string[] { "NoChilds" },
                Operator = FilterOperator.Equal,
                Type = FilterType.Custom,
                Unison = FilterUnison.And,
                Value = true
            };
            filters.Add(filterNoChild);

            ProjectTaskService service = new ProjectTaskService();
            IEnumerable<ProjectTask> items = service.Get(filters).OrderBy(x => x.Name).Take(this.DeveloperSettings.AutoCompletePageSize);

            var results = items.Select(x => new
            {
                itemID = x.ProjectTaskID,
                itemName = x.Name,
                projectName = x.Project.Name
            }).ToArray();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            String jsonResult = JsonConvert.SerializeObject(results, settings);

            return this.Content(jsonResult, "application/json");
        }

        [HttpGet]
        public ContentResult GetExistingDescriptions(string term, long projectTaskID)
        {
            //Generate filter
            List<FilterOption> filters = new List<FilterOption>();

            FilterOption filterValue = new FilterOption()
            {
                Label = new string[] { "DescriptionContains" },
                Operator = FilterOperator.Like,
                Type = FilterType.Custom,
                Unison = FilterUnison.And,
                Value = term
            };
            filters.Add(filterValue);

            if (projectTaskID > 0)
            {
                FilterOption filterTaskID = new FilterOption()
                {
                    Label = new string[] { "ProjectTaskID" },
                    Operator = FilterOperator.Equal,
                    Type = FilterType.Property,
                    Unison = FilterUnison.And,
                    Value = projectTaskID
                };
                filters.Add(filterTaskID);
            }

            ProjectTaskMemberDetailService service = new ProjectTaskMemberDetailService();
            IEnumerable<ProjectTaskMemberDetail> items = service.Get(filters).OrderBy(x => x.Description).Take(this.DeveloperSettings.AutoCompletePageSize).ToList();

            var results = items.Select(x => new
            {
                showedName = "(" + x.ProjectTask.Name + ") " + x.Description,
                itemName = x.Description,
                projectTaskID = x.ProjectTask.ProjectTaskID,
                projectTaskName = x.ProjectTask.Name,
                projectName = x.ProjectTask.Project.Name
            }).Distinct().ToArray();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            String jsonResult = JsonConvert.SerializeObject(results, settings);

            return this.Content(jsonResult, "application/json");
        }

        #endregion
    }
}