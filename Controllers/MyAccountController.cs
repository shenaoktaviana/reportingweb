﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    [Authorize]
    public class MyAccountController : BaseController
    {
        private ApplicationUserManager _userManager;

        public MyAccountController()
        {

        }

        public MyAccountController(ApplicationUserManager userManager)
        {
            this._userManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            this.Init();
            UserService userService = new UserService();
            UserModel model = new UserModel();
            User user = userService.Get(new object[] { this.GetLoggedInUserID() });

            model.User = user;

            return View(model);
        }

        private void Init()
        {
            this.GetGenderOptions();
            this.GetBreadcrumbItems();
            this.GenerateAlertViewbag();
        }

        protected IEnumerable<BreadcrumbItem> GetBreadcrumbItems()
        {
            string title = GeneralResources.MyAccount;

            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = title, Link = "" });

            ViewBag.Breadcrumbs = items;
            ViewBag.Title = title;

            return items;
        }

        private bool IsValid(UserModel model)
        {
            bool isValid = true;

            if (!this.ModelState.IsValid)
            {
                isValid = false;
            }

            return isValid;
        }

        [HttpPost]
        public ActionResult Index(UserModel model)
        {
            Dictionary<string, string> message = new Dictionary<string, string>();

            try
            {
                this.Init();

                UserService userService = new UserService(this.GetLoggedInUserID());

                if (this.IsValid(model))
                {
                    userService.UpdateUser(model.User, false);
                    message.Add("Success", GeneralResources.Account + " " + FrameworkResources.Updated);

                    TempData["Message"] = message;

                    return RedirectToAction("Index");
                }

                ViewBag.ErrorText = FrameworkResources.UnableSave;
            }
            catch (Exception e)
            {
                ViewBag.ErrorText = e.Message;
            }

            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> ChangePassword(string oldPassword, string newPassword, string confirmNewPassword)
        {
            string status = "success";
            string message = "";

            UserService userService = new UserService(this.GetLoggedInUserID());

            User user = userService.Get(new object[] { this.GetLoggedInUserID() });

            try
            {
                if (userService.ValidatePassword(user.Email, oldPassword) != null)
                {

                    IdentityResult result = await this.UserManager.PasswordValidator.ValidateAsync(newPassword);

                    if (!result.Succeeded)
                    {
                        status = "fail";
                        message = result.Errors.ElementAt(0);
                    }
                    else
                    {
                        message = FrameworkResources.Password + " " + FrameworkResources.Updated;

                        userService.UpdateUser(user, true, false, newPassword);

                        this.SendPasswordChangeNotification(user, newPassword);
                    }
                }
                else
                {
                    status = "fail";
                    message = String.Format(FrameworkResources.FieldInvalid, GeneralResources.OldPassword);
                }

            }
            catch (Exception e)
            {
                status = "fail";
                message = CommonUtilities.GetExceptionMessage(e);
            }

            return Json(new
            {
                Status = status,
                Message = message
            });
        }

        private void SendPasswordChangeNotification(User user, string password)
        {
            if (this.DeveloperSettings.EnableNotifications || user.Role == UserRole.ADMIN.ToString() || user.Role == UserRole.SUPER.ToString())
            {
                string title = this.Info.Name + " - " + GeneralResources.ChangePassword;

                string path = this.Info.Theme.ToLower() + "_user_change_password_success.html";
                path = Server.MapPath("~/TemplateFiles/" + path);

                StreamReader reader = new StreamReader(path);
                string template = reader.ReadToEnd();

                template = template.Replace("[FullName]", user.FullName);
                template = template.Replace("[Email]", user.Email);

                List<string> emailList = new List<string>();
                emailList.Add(user.Email);

                new BusinessServiceFramework.BaseMailer(this.DeveloperSettings.SMTPServerType).SendEmailAsync(this.GeneralSettings.SenderEmail, emailList, title, template, true, this.GetRootPath(), this.Info, this.DeveloperSettings);
            }
        }
    }
}