﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public class MemberReportsController : BaseReportController
    {
        public MemberReportsController()
            : base()
        {
            this.controllerName = "MemberReports";
            this.FriendlyReportModuleName = GeneralResources.MemberReports;
        }

        private UserService _serviceprop { get; set; }

        #region GeneralMethods

        private UserService GetModuleService()
        {
            if (this._serviceprop == null)
                this._serviceprop = new UserService(this.GetLoggedInUserID());

            return this._serviceprop;
        }

        #endregion

        protected override string GetEntityType()
        {
            return "ReportingDomainModel.User, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return this.GetModuleService().Get(filters, "Latest", 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            return this.GetModuleService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            this.GetRootPath();

            model.Title = this.FriendlyReportModuleName;
            //model.Styles = new List<string>() { "~/Content/Memberreports/css" };
            model.Scripts = new List<string>() { "~/bundles/Memberreports" };
            model.CustomFilterButtonsPartialName = "_ReportFilterButtonsPartial";

            model.Filters = new List<ListFilterItem>() {
                 new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "Date" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "Date",
                    Text = GeneralResources.Date,
                    ControlType = ControlType.DateRange,
                    Width = "100%",
                    Alignment = Alignment.Center
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "User" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "User",
                    Text = GeneralResources.User,
                    ControlType = ControlType.Custom,
                    CustomControlPartial="_UserMultiselectFilterPartial",
                    Width = "100%"
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "Project" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "Project",
                    Text = GeneralResources.Project,
                    ControlType = ControlType.Custom,
                    CustomControlPartial="_ProjectMultiselectFilterPartial",
                    Width = "100%"
                }
            };

            return model;
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPreviewData(DateTime? startDate, DateTime? endDate, string selectedUsers, string selectedProjects, bool isPrint, string type)
        {
            FilterReportModel filterReportModel = this.GenerateBaseFilterReportModel(startDate, endDate, selectedUsers, selectedProjects, isPrint, type);

            return this.BaseGetPreviewData(filterReportModel, false);
        }

        protected override List<HeaderReportModel> GetReportData(FilterReportModel filterReportModel)
        {
            List<HeaderReportModel> headerReportModelList = new List<HeaderReportModel>();

            UserService userService = new UserService();
            IEnumerable<User> users = userService.GetBy(x => x.IsActive && (filterReportModel.FilteredUserIds.Count() > 0 ? filterReportModel.FilteredUserIds.Contains(x.UserID) : true)).OrderBy(x => x.FirstName);

            ProjectService projectService = new ProjectService();
            IEnumerable<Project> projects = projectService.GetBy(x => filterReportModel.FilteredProjectIds.Count() > 0 ? filterReportModel.FilteredProjectIds.Contains(x.ProjectID) : true).OrderBy(x => x.Name);

            foreach (User user in users)
            {
                ProjectTaskMemberDetailService projectTaskMemberDetailService = new ProjectTaskMemberDetailService();
                IEnumerable<ProjectTaskMemberDetail> taskDetails = projectTaskMemberDetailService.GetBy(x => x.ProjectTaskMember.AssigneeID == user.UserID);

                if (filterReportModel.StartDate != null)
                    taskDetails = taskDetails.Where(x => x.ProjectTaskMember.Date >= filterReportModel.StartDate);

                if (filterReportModel.EndDate != null)
                    taskDetails = taskDetails.Where(x => x.ProjectTaskMember.Date <= filterReportModel.EndDate);

                List<long> existingProjectOfTaskIDs = taskDetails.GroupBy(x => x.ProjectTask.ProjectID).Select(x => x.FirstOrDefault().ProjectTask.ProjectID).ToList();

                if (existingProjectOfTaskIDs.Count() > 0)
                {
                    #region GenerateHeaderReportAndColumnHeader

                    HeaderReportModel headerReportModel = this.CreateHeaderReportModelAndSetBaseValue(filterReportModel.Type, filterReportModel.StartDate, filterReportModel.EndDate);
                    headerReportModel.Title = this.FriendlyReportModuleName;
                    headerReportModelList.Add(headerReportModel);
                    headerReportModel.RootFilterName = "- " + user.FullName.ToUpper() + " -";

                    //GENERATE COLUMN HEADER
                    List<float> columnWidthsList = new List<float>();
                    headerReportModel.HeaderColumns = new List<Tuple<string, ReportRowAlign>>();
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.ProjectTask.ToUpper(), ReportRowAlign.Left));
                    columnWidthsList.Add(35);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Status.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(7);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.SubtotalOfThisPeriod.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    //headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.SubtotalOfPrevPeriod.ToUpper(), ReportRowAlign.Center));
                    //columnWidthsList.Add(6);
                    //headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.GrandTotalDurHourOfTask.ToUpper(), ReportRowAlign.Center));
                    //columnWidthsList.Add(6);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.EstHour.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.OvertimeHour.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.TargetDate.ToUpper(), ReportRowAlign.Center));
                    columnWidthsList.Add(6);
                    headerReportModel.ContentColumnWidths = columnWidthsList.ToArray();

                    #endregion

                    foreach (Project project in projects.Where(x => existingProjectOfTaskIDs.Contains(x.ProjectID)))
                    {
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.ParentGroupingName, project.Name.ToUpper(), headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.BOLD));

                        IEnumerable<ProjectTaskMemberDetail> taskDetailsInProject = taskDetails.Where(x => x.ProjectTask.ProjectID == project.ProjectID);

                        if (taskDetailsInProject.Count() == 0)
                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, FrameworkResources.NoDataAvailable, headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.ITALIC));
                        else
                        {
                            IEnumerable<IEnumerable<ProjectTaskMemberDetail>> taskDetailsInProjectGroupByParent = taskDetailsInProject.GroupBy(x => x.ProjectTask.ParentTaskID);

                            foreach (IEnumerable<ProjectTaskMemberDetail> taskDetailsInProjectPerParent in taskDetailsInProjectGroupByParent)
                            {
                                ProjectTaskMemberDetail defaultTaskDetailPerParent = taskDetailsInProjectPerParent.FirstOrDefault();
                                ProjectTask relatedTaskOfTaskDetailPerParent = defaultTaskDetailPerParent.ProjectTask;

                                //if (relatedTaskOfTaskDetailPerParent.ParentProjectTask != null)
                                //    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingName, "    " + relatedTaskOfTaskDetailPerParent.ReportParentTaskName, headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.BOLD));
                                //else
                                //    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingName, string.Empty, headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.BOLD));

                                IEnumerable<IEnumerable<ProjectTaskMemberDetail>> taskDetailsInProjectGroupByProjectTask = taskDetailsInProjectPerParent.GroupBy(x => x.ProjectTaskID);

                                foreach (IEnumerable<ProjectTaskMemberDetail> taskMemberDetailsPerProjectTask in taskDetailsInProjectGroupByProjectTask.OrderBy(x => x.FirstOrDefault().ProjectTask.NameWithIsRoutine))
                                {
                                    ProjectTaskMemberDetail defaultProjectTaskDetail = taskMemberDetailsPerProjectTask.FirstOrDefault();
                                    ProjectTask projectTask = defaultProjectTaskDetail.ProjectTask;

                                    string projectTaskName = projectTask.NameWithIsRoutine;
                                    string projectTaskNameWithParent = relatedTaskOfTaskDetailPerParent.ParentProjectTask != null ? relatedTaskOfTaskDetailPerParent.ParentProjectTask.Name + " - " + projectTaskName : projectTaskName;

                                    decimal subtotalThisPeriodGroup = projectTask.GetSubtotalOfPeriod(filterReportModel.StartDate, filterReportModel.EndDate, true, null, user.UserID);
                                    decimal subtotalPrevPeriodGroup = projectTask.GetSubtotalOfPreviousPeriod(filterReportModel.StartDate, true, null, user.UserID);
                                    decimal grandTotalGroup = subtotalThisPeriodGroup + subtotalPrevPeriodGroup;

                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, "     " + projectTaskNameWithParent, 2, ReportRowAlign.Left, ReportFontStyle.NORMAL));
                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, this.ConvertHourToString(subtotalThisPeriodGroup, false), 0, ReportRowAlign.Center, ReportFontStyle.NORMAL));
                                    //headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, this.ConvertHourToString(subtotalPrevPeriodGroup, false), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                    //headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, this.ConvertHourToString(grandTotalGroup, false), 0, ReportRowAlign.Center, ReportFontStyle.BOLD));
                                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.SubgroupName, string.Empty, 3, ReportRowAlign.Left, ReportFontStyle.BOLD));

                                    IEnumerable<IEnumerable<ProjectTaskMemberDetail>> taskDetailsInProjectGroupByDesc = taskMemberDetailsPerProjectTask.GroupBy(x => x.Description);

                                    foreach (IEnumerable<ProjectTaskMemberDetail> taskMemberDetailsPerDesc in taskDetailsInProjectGroupByDesc.OrderBy(x => x.FirstOrDefault().Description))
                                    {
                                        ProjectTaskMemberDetail defaultTaskMemberDetail = taskMemberDetailsPerDesc.FirstOrDefault();
                                        ProjectTask childTask = defaultTaskMemberDetail.ProjectTask;

                                        string status = childTask.GetLatestStatusInPeriodByParentTaskID(filterReportModel.StartDate, filterReportModel.EndDate, false, defaultTaskMemberDetail.Description, user.UserID);
                                        decimal subtotalThisPeriod = childTask.GetSubtotalOfPeriod(filterReportModel.StartDate, filterReportModel.EndDate, false, defaultTaskMemberDetail.Description, user.UserID);
                                        decimal subtotalPrevPeriod = childTask.GetSubtotalOfPreviousPeriod(filterReportModel.StartDate, false, defaultTaskMemberDetail.Description, user.UserID);
                                        decimal grandTotal = subtotalThisPeriod + subtotalPrevPeriod;
                                        decimal overtimeHour = 0;

                                        if (!childTask.IsRoutine)
                                            overtimeHour = childTask.EstHour.HasValue ? childTask.EstHour.Value - grandTotal : 0;

                                        if (subtotalThisPeriod > 0)
                                        {
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, "             - " + defaultTaskMemberDetail.DescriptionOrNoDesc, 0, ReportRowAlign.Left, defaultTaskMemberDetail.Description != null ? ReportFontStyle.NORMAL : ReportFontStyle.ITALIC));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, status, 0, ReportRowAlign.Center, GetReportFontStyleForStatus(status, false)));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(subtotalThisPeriod, false), 0, ReportRowAlign.Center));
                                            //headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(subtotalPrevPeriod, false), 0, ReportRowAlign.Center));
                                            //headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(grandTotal, false), 0, ReportRowAlign.Center));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(childTask.EstHour, false) + (childTask.IsRoutine ? "/day" : string.Empty), 0, ReportRowAlign.Center));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(overtimeHour, false), 0, ReportRowAlign.Center, GetReportFontStyleOfMinusValue(overtimeHour)));
                                            headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertDateToSettingShortFormat(childTask.TargetDate, false), 0, ReportRowAlign.Center));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return headerReportModelList;
        }
    }
}