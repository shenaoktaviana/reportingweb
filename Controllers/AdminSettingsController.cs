﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingBusinessService.Models;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.BaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Xml.Serialization;
using ReportingDomainModel;

namespace ReportingWeb.Controllers
{
    [Module(Value = "Settings")]
    public class AdminSettingsController : BaseAdminController
    {
        #region Settings Main Page
        public override ActionResult Index(long? id)
        {
            UserService userService = new UserService();
            User loggedInUser = userService.Get(new object[] { this.GetLoggedInUserID() });

            this.GenerateAlertViewbag();

            List<NavigationSettings> items = new List<NavigationSettings>();

            if (loggedInUser.Role == UserRole.SUPER.ToString())
            {
                items.Add(new NavigationSettings()
                {
                    Value = GeneralResources.DeveloperSettings,
                    Description = GeneralResources.DeveloperSettingsDesc,
                    Link = Url.Action("DeveloperSettings", "AdminSettings")
                });
            }

            if (loggedInUser.Role == UserRole.SUPER.ToString() || loggedInUser.Role == UserRole.ADMIN.ToString())
            {
                items.Add(new NavigationSettings()
                {
                    Value = GeneralResources.GeneralSettings,
                    Description = GeneralResources.GeneralSettingsDesc,
                    Link = Url.Action("GeneralSettings", "AdminSettings")
                });
            }
            
            if (loggedInUser.Role == UserRole.SUPER.ToString() || loggedInUser.Role == UserRole.ADMIN.ToString())
            {
                items.Add(new NavigationSettings()
                {
                    Value = GeneralResources.Info,
                    Description = GeneralResources.InfoDesc,
                    Link = Url.Action("Info", "AdminSettings")
                });
            }

            ViewBag.Breadcrumbs = this.GetSettingsBreadcrumbs();
            ViewBag.Title = GeneralResources.Settings;
            return View(items);
        }

        #endregion

        #region General Settings
        [AdminPrivilege]
        [ActionName("GeneralSettings")]
        public ActionResult GeneralSettingsPage()
        {
            this.GenerateAlertViewbag();

            SettingModel model = this.InitGeneralSettings();

            return View(model);
        }

        protected SettingModel InitGeneralSettings()
        {
            SettingsService service = new SettingsService(this.GetLoggedInUserID());
            SettingModel model = new SettingModel();

            model.EntityType = typeof(GeneralSettings);
            model.NonRequiredProperties = new List<string>() { "IsUnderConstruction" };
            model.Data = service.GetGeneralSetting();
            model.HiddenProperties.AddRange(this.SetIgnoredProperties(model));
            model.CurrentTab = "GeneralSettings";
            model.Title = GeneralResources.GeneralSettings;

            ViewBag.Title = GeneralResources.GeneralSettings;
            ViewBag.Breadcrumbs = this.GetGeneralSettingsBreadcrumbs();
            return model;
        }

        [HttpPost]
        [ActionName("GeneralSettings")]
        public ActionResult GeneralSettingsPage(GeneralSettings generalSettings)
        {
            SettingModel model = this.InitGeneralSettings();
            model.Data = generalSettings;

            if (generalSettings.MaxFileSize == 0)
            {
                this.ModelState.AddModelError("MaxFileSize", string.Format(FrameworkResources.ZeroRequired, GeneralResources.MaxFileSize));
            }

            return this.SaveData(model);
        }

        #endregion

        #region Developer Settings
        [AdminPrivilege(IsSuperOnly = true)]
        [ActionName("DeveloperSettings")]
        public ActionResult DeveloperSettingsPage()
        {
            this.GenerateAlertViewbag();
            ViewBag.Title = GeneralResources.DeveloperSettings;
            SettingModel model = this.InitDeveloperSettings();

            return View(model);
        }

        protected SettingModel InitDeveloperSettings()
        {
            SettingModel model = new SettingModel();
            SettingsService service = new SettingsService(this.GetLoggedInUserID());

            model.EntityType = typeof(DeveloperSettings);
            model.CurrentTab = "DeveloperSettings";
            model.Title = GeneralResources.DeveloperSettings;
            model.NonRequiredProperties = new List<string>() { "BitlyAccessToken" };
            model.Data = service.GetDeveloperSetting();
            model.HiddenProperties.AddRange(this.SetIgnoredProperties(model));

            #region Dropdown properties
            List<SelectListItem> smtpList = new List<SelectListItem>();

            foreach (SMTPServerType smtpServerType in Enum.GetValues(typeof(SMTPServerType)))
            {
                smtpList.Add(
                    new SelectListItem()
                    {
                        Text = smtpServerType.ToString(),
                        Value = smtpServerType.ToString()
                    });
            }

            model.DropdownProperties.Add("SMTPServerType", smtpList);
            #endregion

            ViewBag.Breadcrumbs = this.GetDeveloperSettingsBreadcrumbs();
            return model;
        }

        [HttpPost]
        [ActionName("DeveloperSettings")]
        public ActionResult DeveloperSettingsPage(DeveloperSettings developerSettings)
        {
            SettingModel model = this.InitDeveloperSettings();
            model.Data = developerSettings;

            return this.SaveData(model);
        }

        #endregion

        #region Info Settings
        [AdminPrivilege]
        [ActionName("Info")]
        public ActionResult InfoPage()
        {
            this.GenerateAlertViewbag();

            SettingModel model = this.InitInfo();
            return View(model);
        }

        protected SettingModel InitInfo()
        {
            SettingsService service = new SettingsService(this.GetLoggedInUserID());
            SettingModel model = new SettingModel();

            model.EntityType = typeof(Info);
            model.CurrentTab = "Info";
            model.Title = GeneralResources.Info;
            model.NonRequiredProperties = new List<string>() { "MainPhone", "Fax", "Address", "WhatsApp", "Facebook", "Twitter", "Instagram" };
            model.ImageProperties = new List<string>() { "Logo" };
            model.Data = service.GetInfo();
            model.HiddenProperties.AddRange(this.SetIgnoredProperties(model));
            model.HiddenProperties.AddRange(new List<string>() { "GooglePlus", "HPNo" });
            model.TextAreaProperties = new List<string>() { "Address", "MetaDescription" };

            #region Dropdown properties
            List<SelectListItem> currencyListItem = new List<SelectListItem>();
            List<SelectListItem> themeListItem = new List<SelectListItem>();
            List<string> themeList = new BaseInfo().GetThemeOptions();

            foreach (Currency currency in Enum.GetValues(typeof(Currency)))
            {
                currencyListItem.Add(
                    new SelectListItem()
                    {
                        Text = currency.ToString(),
                        Value = currency.ToString()
                    });
            }

            model.DropdownProperties.Add("Currency", currencyListItem);

            foreach (string theme in themeList)
            {
                themeListItem.Add(
                    new SelectListItem()
                    {
                        Text = theme,
                        Value = theme
                    });
            }

            model.DropdownProperties.Add("Theme", themeListItem);
            #endregion

            ViewBag.Title = GeneralResources.Info;
            ViewBag.Breadcrumbs = this.GetInfoBreadcrumbs();
            return model;
        }

        [HttpPost]
        [ActionName("Info")]
        public ActionResult InfoPage(Info info)
        {
            SettingModel model = this.InitInfo();
            model.Data = info;

            return this.SaveData(model);
        }
        #endregion

        protected IEnumerable<BreadcrumbItem> GetSettingsBreadcrumbs()
        {
            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = GeneralResources.Settings, Link = "" });

            return items;
        }

        protected IEnumerable<BreadcrumbItem> GetGeneralSettingsBreadcrumbs()
        {
            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = GeneralResources.Settings, Link = Url.Action("Index") });
            items.Add(new BreadcrumbItem() { Text = GeneralResources.GeneralSettings, Link = "" });

            return items;
        }

        protected IEnumerable<BreadcrumbItem> GetDeveloperSettingsBreadcrumbs()
        {
            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = GeneralResources.Settings, Link = Url.Action("Index") });
            items.Add(new BreadcrumbItem() { Text = GeneralResources.DeveloperSettings, Link = "" });

            return items;
        }

        protected IEnumerable<BreadcrumbItem> GetRewardSettingsBreadcrumbs()
        {
            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = GeneralResources.Settings, Link = Url.Action("Index") });

            return items;
        }

        protected IEnumerable<BreadcrumbItem> GetInfoBreadcrumbs()
        {
            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = GeneralResources.Settings, Link = Url.Action("Index") });
            items.Add(new BreadcrumbItem() { Text = GeneralResources.Info, Link = "" });

            return items;
        }

        protected List<string> SetIgnoredProperties(SettingModel model)
        {
            Type type = model.EntityType;

            List<string> hiddenProperties = new List<string>();

            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo propertyInfo in properties)
            {
                bool isIgnored = propertyInfo.GetCustomAttributes().Any(x => x is XmlIgnoreAttribute);

                if (isIgnored)
                {
                    hiddenProperties.Add(propertyInfo.Name);
                }
            }
            return hiddenProperties;
        }

        protected virtual bool IsValid(SettingModel model)
        {
            bool isValid = true;

            Type type = model.EntityType;

            PropertyInfo[] properties = type.GetProperties();

            List<string> customExceptions = this.GetCustomValidException(model);

            foreach (PropertyInfo propertyInfo in properties)
            {
                string name = propertyInfo.Name;
                object obj = propertyInfo.GetValue(model.Data, null);

                if (!model.NonRequiredProperties.Contains(name) && !model.HiddenProperties.Contains(name) && !customExceptions.Contains(name))
                {
                    if (obj == null)
                    {
                        isValid = false;

                        string resourceName = GeneralResources.ResourceManager.GetString(name);

                        if (string.IsNullOrEmpty(resourceName))
                            resourceName = FrameworkResources.ResourceManager.GetString(name);

                        this.ModelState.AddModelError(name, string.Format(FrameworkResources.FieldRequired, resourceName));
                    }
                }
            }

            if (!this.ModelState.IsValid)
            {
                List<string> androidErrors = ModelState.Where(x => x.Value.Errors.Count > 0 && x.Key.EndsWith("AndroidVersion") ||
                    x.Key.EndsWith("AndroidNamespace")).Select(x => x.Key).ToList();

                List<string> errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => x.Key).ToList();

                if (androidErrors.Count == errors.Count)
                    isValid = true;
                else
                    isValid = false;
            }

            return isValid;
        }

        private List<string> GetCustomValidException(SettingModel model)
        {
            Type type = model.EntityType;
            PropertyInfo[] properties = type.GetProperties();
            List<string> customExceptions = new List<string>();

            return customExceptions;
        }

        private void ProcessLogo(SettingModel model, Info info)
        {
            if (info.Logo != "logo.png")
            {
                CommonUtilities.MoveFiles(Server.MapPath("~/images/Temp/"), Server.MapPath("~/images/"), info.Logo, "logo.png");
                info.Logo = "logo.png";
            }
        }

        protected virtual ActionResult SaveData(SettingModel model)
        {
            bool isValid = this.IsValid(model);
            Dictionary<string, string> message = new Dictionary<string, string>();
            string controllerName = this.RouteData.Values["controller"].ToString();

            try
            {
                if (isValid)
                {
                    Info info = this.Info;

                    if (model.EntityType.Name == "Info")
                    {
                        info = (Info)model.Data;
                        this.ProcessLogo(model, info);
                    }

                    SettingsService service = new SettingsService(this.GetLoggedInUserID());

                    if (model.Data.GetType() == typeof(GeneralSettings))
                        service.SmartInsert((GeneralSettings)model.Data);
                    else if (model.Data.GetType() == typeof(DeveloperSettings))
                        service.SmartInsert((DeveloperSettings)model.Data);
                    else if (model.Data.GetType() == typeof(Info))
                        service.SmartInsert((Info)model.Data);

                    message.Add("Success", GeneralResources.ResourceManager.GetString(model.EntityType.Name) + " " + FrameworkResources.Updated);
                    TempData["Message"] = message;

                    return RedirectToAction("Index", controllerName);
                }
                else
                {
                    ViewBag.ErrorText = FrameworkResources.UnableSave;
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorText = e.Message;
            }

            return View(model);
        }
    }
}