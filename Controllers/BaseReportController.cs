﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingBusinessService.Models;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public class BaseReportController : BaseAdminController
    {
        protected string FriendlyReportModuleName { get; set; }

        [AllowAnonymous]
        [HttpGet]
        public ContentResult GetReportMembers(string value, string selectedData)
        {
            UserService service = new UserService();
            List<FilterOption> filters = new List<FilterOption>();

            String jsonResult = null;

            try
            {
                if (!string.IsNullOrEmpty(selectedData))
                {
                    string[] selectedItemArray = selectedData.Split(",".ToCharArray());

                    foreach (string selectedItem in selectedItemArray)
                    {
                        long itemID = 0;

                        Int64.TryParse(selectedItem, out itemID);

                        FilterOption filterItemID = new FilterOption()
                        {

                            Label = new string[] { "UserID" },
                            Operator = FilterOperator.NotEqual,
                            Type = FilterType.Property,
                            Unison = FilterUnison.And,
                            Value = itemID
                        };

                        filters.Add(filterItemID);
                    }
                }

                FilterOption filterValue = new FilterOption()
                {

                    Label = new string[] { "FullName" },
                    Operator = FilterOperator.Like,
                    Type = FilterType.Custom,
                    Unison = FilterUnison.And,
                    Value = value
                };

                filters.Add(filterValue);

                FilterOption filterIsActive = new FilterOption()
                {

                    Label = new string[] { "IsActive" },
                    Operator = FilterOperator.Equal,
                    Type = FilterType.Property,
                    Unison = FilterUnison.And,
                    Value = true
                };

                filters.Add(filterIsActive);

                FilterOption filterIsMember = new FilterOption()
                {

                    Label = new string[] { "IsMember" },
                    Operator = FilterOperator.Equal,
                    Type = FilterType.Property,
                    Unison = FilterUnison.And,
                    Value = true
                };

                filters.Add(filterIsMember);

                IEnumerable<User> data = service.Get(filters);

                if (data.Count() > 0)
                {
                    data = data.Take(this.DeveloperSettings.AutoCompletePageSize);
                }

                var results = data.Select(x => new
                {
                    UserID = x.UserID,
                    Name = x.FullName,
                }).ToArray();

                JsonSerializerSettings settings = new JsonSerializerSettings();
                jsonResult = JsonConvert.SerializeObject(results, settings);
            }
            catch (Exception e)
            {
                throw e;
            }

            return this.Content(jsonResult, "application/json");
        }

        [AllowAnonymous]
        [HttpGet]
        public ContentResult GetReportProjects(string value, string selectedData)
        {
            ProjectService service = new ProjectService();
            List<FilterOption> filters = new List<FilterOption>();

            String jsonResult = null;

            try
            {
                if (!string.IsNullOrEmpty(selectedData))
                {
                    string[] selectedItemArray = selectedData.Split(",".ToCharArray());

                    foreach (string selectedItem in selectedItemArray)
                    {
                        long itemID = 0;

                        Int64.TryParse(selectedItem, out itemID);

                        FilterOption filterItemID = new FilterOption()
                        {

                            Label = new string[] { "ProjectID" },
                            Operator = FilterOperator.NotEqual,
                            Type = FilterType.Property,
                            Unison = FilterUnison.And,
                            Value = itemID
                        };

                        filters.Add(filterItemID);
                    }
                }

                FilterOption filterValue = new FilterOption()
                {

                    Label = new string[] { "Name" },
                    Operator = FilterOperator.Like,
                    Type = FilterType.Property,
                    Unison = FilterUnison.And,
                    Value = value
                };

                filters.Add(filterValue);

                FilterOption filterIsActive = new FilterOption()
                {

                    Label = new string[] { "IsActive" },
                    Operator = FilterOperator.Equal,
                    Type = FilterType.Property,
                    Unison = FilterUnison.And,
                    Value = true
                };

                filters.Add(filterIsActive);

                IEnumerable<Project> data = service.Get(filters);

                if (data.Count() > 0)
                {
                    data = data.Take(this.DeveloperSettings.AutoCompletePageSize);
                }

                var results = data.Select(x => new
                {
                    ProjectID = x.ProjectID,
                    Name = x.Name,
                }).ToArray();

                JsonSerializerSettings settings = new JsonSerializerSettings();
                jsonResult = JsonConvert.SerializeObject(results, settings);
            }
            catch (Exception e)
            {
                throw e;
            }

            return this.Content(jsonResult, "application/json");
        }

        [AllowAnonymous]
        [HttpGet]
        public ContentResult GetJobRoles(string value, string selectedData)
        {
            JobRoleService service = new JobRoleService();
            List<FilterOption> filters = new List<FilterOption>();

            String jsonResult = null;

            try
            {
                if (!string.IsNullOrEmpty(selectedData))
                {
                    string[] selectedItemArray = selectedData.Split(",".ToCharArray());

                    foreach (string selectedItem in selectedItemArray)
                    {
                        long itemID = 0;

                        Int64.TryParse(selectedItem, out itemID);

                        FilterOption filterItemID = new FilterOption()
                        {

                            Label = new string[] { "JobRoleID" },
                            Operator = FilterOperator.NotEqual,
                            Type = FilterType.Property,
                            Unison = FilterUnison.And,
                            Value = itemID
                        };

                        filters.Add(filterItemID);
                    }
                }

                FilterOption filterValue = new FilterOption()
                {

                    Label = new string[] { "Name" },
                    Operator = FilterOperator.Like,
                    Type = FilterType.Property,
                    Unison = FilterUnison.And,
                    Value = value
                };

                filters.Add(filterValue);

                FilterOption filterIsActive = new FilterOption()
                {

                    Label = new string[] { "Outlet.IsActive" },
                    Operator = FilterOperator.Equal,
                    Type = FilterType.Property,
                    Unison = FilterUnison.And,
                    Value = true
                };

                filters.Add(filterIsActive);

                IEnumerable<JobRole> data = service.Get(filters);

                if (data.Count() > 0)
                {
                    data = data.Take(this.DeveloperSettings.AutoCompletePageSize);
                }

                var results = data.Select(x => new
                {
                    ItemID = x.JobRoleID,
                    Name = x.Name,
                }).ToArray();

                JsonSerializerSettings settings = new JsonSerializerSettings();
                jsonResult = JsonConvert.SerializeObject(results, settings);
            }
            catch (Exception e)
            {
                throw e;
            }

            return this.Content(jsonResult, "application/json");
        }

        protected override IEnumerable<BreadcrumbItem> GetBreadcrumbItems()
        {
            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = GeneralResources.Reports, Link = "Reports" });
            items.Add(new BreadcrumbItem() { Text = this.FriendlyReportModuleName, Link = "" });

            ViewBag.Breadcrumbs = items;

            return items;
        }

        protected override List<ListColumn> GetColumns()
        {
            return new List<ListColumn>();
        }

        protected FilterReportModel GenerateBaseFilterReportModel(DateTime? startDate, DateTime? endDate, string selectedUsers, string selectedProjects, bool isPrint, string type)
        {
            FilterReportModel filterReportModel = new FilterReportModel();
            filterReportModel.IsPrint = isPrint;
            filterReportModel.Type = type;
            filterReportModel.StartDate = startDate;
            filterReportModel.EndDate = endDate;

            if (selectedUsers != null)
            {
                List<long> userIDs = this.ConvertIDsFromJsonArray(selectedUsers);
                UserService userService = new UserService();
                IEnumerable<User> filteredUsers = userService.GetBy(x => x.IsActive && (userIDs.Count() > 0 ? userIDs.Contains(x.UserID) : true));
                filterReportModel.FilteredUsers = filteredUsers.Distinct();
                filterReportModel.FilteredUserIds = filterReportModel.FilteredUsers.Select(x => x.UserID);
            }

            if (selectedProjects != null)
            {
                List<long> projectIDs = this.ConvertIDsFromJsonArray(selectedProjects);
                ProjectService projectService = new ProjectService();
                IEnumerable<Project> filteredProjects = projectService.GetBy(x => x.IsActive && (projectIDs.Count() > 0 ? projectIDs.Contains(x.ProjectID) : true));
                filterReportModel.FilteredProjects = filteredProjects.Distinct();
                filterReportModel.FilteredProjectIds = filterReportModel.FilteredProjects.Select(x => x.ProjectID);
            }

            return filterReportModel;
        }

        protected JsonResult BaseGetPreviewData(FilterReportModel filterReportModel, bool dateRequired = true)
        {
            if (filterReportModel != null && (dateRequired ? filterReportModel.StartDate != null && filterReportModel.EndDate != null : true))
            {
                string message = string.Empty;

                if (filterReportModel.IsPrint)
                    message = GeneralResources.ReportPrinted;
                else
                    message = GeneralResources.ReportDisplayed;

                string currentLanguage = ViewBag.CurrentLanguage;
                string pdfPath = string.Empty;
                string status = "success";

                List<object> result = new List<object>();

                try
                {
                    List<HeaderReportModel> headerReportModelList = this.GetReportData(filterReportModel);

                    if (headerReportModelList != null && headerReportModelList.Count() > 0)
                    {
                        pdfPath = ReportingCommonUtilities.GetExportPDF(headerReportModelList);
                        pdfPath = this.GetRootPath() + "/Documents/" + pdfPath;
                    }
                    else
                    {
                        status = "nodata";
                        message = GeneralResources.EmptyData;
                    }
                }
                catch (Exception e)
                {
                    status = "fail";
                    message = CommonUtilities.GetExceptionMessage(e);
                }

                return Json(new
                {
                    Status = status,
                    Message = message,
                    Data = pdfPath
                });
            }
            else
            {
                return Json(new
                {
                    Status = "fail",
                    Message = String.Format(FrameworkResources.FieldRequired, GeneralResources.Date)
                });
            }
        }

        protected virtual List<HeaderReportModel> GetReportData(FilterReportModel filterReportModel)
        {
            //implementoncontroller
            return null;
        }

        protected List<long> ConvertIDsFromJsonArray(string data)
        {
            List<long> results = new List<long>();
            if (!string.IsNullOrEmpty(data))
            {
                string[] selectedData = data.Split(",".ToCharArray());

                foreach (string item in selectedData)
                {
                    long id = 0;

                    Int64.TryParse(item, out id);

                    if (id > 0)
                    {
                        results.Add(id);
                    }
                }
            }

            return results;
        }

        protected string ConvertHourToString(decimal? hour, bool allowEmpty = true)
        {
            if (hour == null || hour == 0)
                return allowEmpty ? string.Empty : "-";

            return hour.ToString();
        }

        protected string ConvertHourToStringWithMandays(decimal? hour, bool allowEmpty = true)
        {
            string hourText = this.ConvertHourToString(hour, allowEmpty);

            if (hourText != "-" && hourText != string.Empty)
                return hourText + " H / " + (hour.Value / 8).ToString("N2") + " D";
            else
                return hourText;
        }

        protected string ConvertDateToSettingShortFormat(DateTime? date, bool allowEmpty = true)
        {
            if (date == null)
                return allowEmpty ? string.Empty : "-";

            return date.Value.ToString(this.DeveloperSettings.ShortDateFormat);
        }

        protected string ConvertDateToSettingLongFormat(DateTime? date, bool allowEmpty = true)
        {
            if (date == null)
                return allowEmpty ? string.Empty : "-";

            return date.Value.ToString(this.DeveloperSettings.LongDateFormat);
        }

        protected ReportFontStyle GetReportFontStyleForStatus(string status, bool allowBold = true)
        {
            if (status != "DONE" && status != "CLOSED")
                return allowBold ? ReportFontStyle.REDBOLD : ReportFontStyle.RED;

            return allowBold ? ReportFontStyle.BOLD : ReportFontStyle.NORMAL;
        }

        protected ReportFontStyle GetReportFontStyleForDayOff(DayOffData dayOff)
        {
            if (dayOff == null)
                return ReportFontStyle.BOLD;

            return ReportFontStyle.REDBOLD;
        }

        protected ReportFontStyle GetReportFontStyleOfMinusValue(decimal value)
        {
            if (value < 0)
                return ReportFontStyle.REDBOLD;

            return ReportFontStyle.BOLD;
        }

        protected DetailReportModel GenerateDetailReportModel(ReportRowType reportRowType, string description, int colspan = 0,
            ReportRowAlign horizontalAlign = ReportRowAlign.Left, ReportFontStyle fontStyle = ReportFontStyle.NORMAL, string tag = null)
        {
            DetailReportModel detailReportModel = new DetailReportModel();
            detailReportModel.ReportRowType = reportRowType;
            detailReportModel.RowHorizontalAlign = horizontalAlign;
            detailReportModel.Tag = tag;
            detailReportModel.FontStyle = fontStyle;

            if (colspan > 0)
                detailReportModel.Colspan = colspan;

            detailReportModel.Description = description;

            return detailReportModel;
        }

        protected HeaderReportModel CreateHeaderReportModelAndSetBaseValue(string type, DateTime? startDate, DateTime? endDate, bool isLandscape = true)
        {
            HeaderReportModel headerReportModel = new HeaderReportModel();
            headerReportModel.Type = type;
            headerReportModel.DetailRows = new List<DetailReportModel>();
            headerReportModel.StartDate = startDate;
            headerReportModel.EndDate = endDate;
            //headerReportModel.CompanyName = this.GeneralSettings.CompanyName;
            //headerReportModel.CompanyLogo = this.GeneralSettings.CompanyLogo;
            headerReportModel.CurrentLoggedInName = this.GetLoggedInUser() != null ? this.GetLoggedInUser().FullName : string.Empty;
            headerReportModel.FooterDateFormat = "dd/MM/yyyy HH:MM";
            headerReportModel.IsLandscape = isLandscape;
            //headerReportModel.CompanyLogoHeight = this.GeneralSettings.ReportCompanyLogoHeight > 0 ? this.GeneralSettings.ReportCompanyLogoHeight : 50;
            //headerReportModel.CompanyLogoWidth = this.GeneralSettings.ReportCompanyLogoWidth > 0 ? this.GeneralSettings.ReportCompanyLogoWidth : 80;

            return headerReportModel;
        }

        protected string GetLatestStatusOfTask(long projectTaskID)
        {
            string status = GeneralResources.NotStarted;

            ProjectTaskMemberDetailService projectTaskMemberDetailService = new ProjectTaskMemberDetailService();
            ProjectTaskMemberDetail latestReportMemberOfTask = projectTaskMemberDetailService.GetBy(x => x.ProjectTaskID == projectTaskID).OrderByDescending(x => x.CreatedDate).FirstOrDefault();

            if (latestReportMemberOfTask != null)
                status = latestReportMemberOfTask.StatusTask.Name;

            return status;
        }
    }
}