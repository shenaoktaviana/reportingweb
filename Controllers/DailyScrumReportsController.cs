﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public class DailyScrumReportsController : BaseReportController
    {
        public DailyScrumReportsController()
            : base()
        {
            this.controllerName = "DailyScrumReports";
            this.FriendlyReportModuleName = GeneralResources.DailyScrumReports;
        }

        private DailyScrumService _serviceprop { get; set; }

        #region GeneralMethods

        private DailyScrumService GetModuleService()
        {
            if (this._serviceprop == null)
                this._serviceprop = new DailyScrumService(this.GetLoggedInUserID());

            return this._serviceprop;
        }

        #endregion

        protected override string GetEntityType()
        {
            return "ReportingDomainModel.DailyScrum, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return this.GetModuleService().Get(filters, "Latest", 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            return this.GetModuleService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            this.GetRootPath();

            model.Title = this.FriendlyReportModuleName;
            //model.Styles = new List<string>() { "~/Content/DailyScrumReports/css" };
            model.Scripts = new List<string>() { "~/bundles/DailyScrumReports" };
            model.CustomFilterButtonsPartialName = "_ReportFilterButtonsPartial";

            model.Filters = new List<ListFilterItem>() {
                 new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "Date" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "Date",
                    Text = GeneralResources.Date,
                    ControlType = ControlType.DateRange,
                    Width = "100%",
                    Alignment = Alignment.Center
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "User" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "User",
                    Text = GeneralResources.User,
                    ControlType = ControlType.Custom,
                    CustomControlPartial="_UserMultiselectFilterPartial",
                    Width = "100%"
                }
            };

            return model;
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPreviewData(DateTime? startDate, DateTime? endDate, string selectedUsers, bool isPrint, string type)
        {
            FilterReportModel filterReportModel = this.GenerateBaseFilterReportModel(startDate, endDate, selectedUsers, null, isPrint, type);

            return this.BaseGetPreviewData(filterReportModel, false);
        }

        protected override List<HeaderReportModel> GetReportData(FilterReportModel filterReportModel)
        {
            List<HeaderReportModel> headerReportModelList = new List<HeaderReportModel>();

            DailyScrumService dailyScrumService = new DailyScrumService();

            IEnumerable<IEnumerable<DailyScrum>> dailyScrumsByDate = dailyScrumService.GetBy(
                x => (filterReportModel.StartDate.HasValue ? x.Date >= filterReportModel.StartDate : true) &&
                    (filterReportModel.EndDate.HasValue ? x.Date <= filterReportModel.EndDate : true)).OrderBy(x => x.Date).GroupBy(x => x.Date);

            foreach (IEnumerable<DailyScrum> dailyScrums in dailyScrumsByDate)
            {
                DailyScrum defaultScrum = dailyScrums.OrderBy(x => x.User.FirstName).FirstOrDefault();

                #region GenerateHeaderReportAndColumnHeader

                HeaderReportModel headerReportModel = this.CreateHeaderReportModelAndSetBaseValue(filterReportModel.Type, defaultScrum.Date, null, false);
                headerReportModel.Title = this.FriendlyReportModuleName.ToUpper();
                headerReportModelList.Add(headerReportModel);
                List<float> columnWidthsList = new List<float>();

                //GENERATE COLUMN HEADER
                headerReportModel.HeaderColumns = new List<Tuple<string, ReportRowAlign>>();
                headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.No, ReportRowAlign.Left));
                columnWidthsList.Add(3);
                headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Project, ReportRowAlign.Left));
                columnWidthsList.Add(8);
                headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Description, ReportRowAlign.Left));
                columnWidthsList.Add(20);
                headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.EstHour, ReportRowAlign.Center));
                columnWidthsList.Add(5);
                headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Priority, ReportRowAlign.Center));
                columnWidthsList.Add(7);
                headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.TargetDate, ReportRowAlign.Center));
                columnWidthsList.Add(7);
                headerReportModel.HeaderColumns.Add(Tuple.Create(GeneralResources.Notes, ReportRowAlign.Center));
                columnWidthsList.Add(20);

                headerReportModel.ContentColumnWidths = columnWidthsList.ToArray();

                #endregion

                foreach (DailyScrum scrum in dailyScrums.OrderBy(x => x.User.FirstName))
                {
                    headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.GroupingName, scrum.User.FullName.ToUpper(), headerReportModel.HeaderColumns.Count(), ReportRowAlign.Left, ReportFontStyle.BOLD));

                    int index = 1;
                    foreach (DailyScrumDetail detail in scrum.DailyScrumDetails)
                    {
                        bool overTargetDate = detail.TargetDate < scrum.Date;

                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, index.ToString(), 0, ReportRowAlign.Center));
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, detail.ProjectTask.Project.Name));
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, detail.DescriptionOrProjectTaskName));
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, this.ConvertHourToString(detail.EstHour), 0, ReportRowAlign.Center));
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, detail.Priority, 0, ReportRowAlign.Center, detail.Priority.ToUpper() == "HIGH" ? ReportFontStyle.REDBOLD : ReportFontStyle.NORMAL));
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, (detail.TargetDate == scrum.Date ? "Today" : this.ConvertDateToSettingShortFormat(detail.TargetDate).ToUpper()), 0, ReportRowAlign.Center, overTargetDate ? ReportFontStyle.REDBOLD : ReportFontStyle.BOLD));
                        headerReportModel.DetailRows.Add(this.GenerateDetailReportModel(ReportRowType.Content, detail.Notes, 0, ReportRowAlign.Left, ReportFontStyle.ITALIC));

                        index++;
                    }
                }
            }

            return headerReportModelList;
        }
    }
}