﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingBusinessService.Models;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public abstract class BaseAdminController : BaseController
    {
        protected Navigation MainNavItems { get; set; }

        protected string FriendlyIndexModuleName { get; set; }
        protected string FriendlyDetailModuleName { get; set; }

        private string[] ModuleWithActiveStatus = new string[] { "AdminProjects", "AdminProjectTasks", "AdminUsers", "AdminJobRoles" };

        public BaseAdminController()
        {
            string mainFilePath = HostingEnvironment.MapPath("~/App_Data/Navigation/AdminMainNavigation.xml");
            this.MainNavItems = CommonUtilities.GetObjectFromXml(mainFilePath, typeof(Navigation)) as Navigation;
            ViewBag.MainNavItems = this.MainNavItems;
        }

        protected string controllerName;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            ViewBag.AdminMenuItems = this.GetAdminMenuItems();
        }

        private Dictionary<string, string> GetAdminMenuItems()
        {
            GeneralSettings generalSettings = this.GeneralSettings;
            Dictionary<string, string> items = new Dictionary<string, string>();

            UserService userService = new UserService();
            User loginUser = userService.Get(new object[] { this.GetLoggedInUserID() });

            if (loginUser != null)
            {
                List<string> modules = new List<string>(){
                    "Settings", "Projects", "ProjectTasks", "ProjectTaskMembers", "DailyScrums", "Users", "Reports"
                };

                string pageName = string.Empty;

                foreach (string module in modules)
                {
                    bool isAllowed = this.IsModuleAllowed(loginUser, module);

                    switch (module)
                    {
                        case "Projects":
                        case "ProjectTasks":
                        case "JobRoles":
                        case "Users":
                        case "Settings":
                        case "ProjectTaskMembers":
                        case "DailyScrums":
                            pageName = "Admin" + module;
                            break;
                        default:
                            pageName = module;
                            isAllowed = this.IsLoginSuper() ? true :
                                    this.IsModuleAllowed(loginUser, module);
                            break;
                    }

                    if (isAllowed)
                        items.Add(pageName, module);
                }
            }
            return items;
        }

        public bool IsModuleAllowed(User loginUser, string module)
        {
            if (loginUser.Role == UserRole.SUPER.ToString())
            {
                return true;
            }
            else
            {
                if (module != "Import" && module != "Reports")
                {
                    if (module == "Reports")
                    {
                        UserModulePrivilege userModulePrivilege = loginUser.UserModulePrivileges.FirstOrDefault(x =>
                            x.ModulePrivilege.Module.Name.Contains(module)
                            && x.IsAllowed
                            && x.ModulePrivilege.Privilege.Name == PrivilegeName.VIEW.ToString());

                        if (userModulePrivilege != null)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        UserModulePrivilege userModulePrivilege = loginUser.UserModulePrivileges.FirstOrDefault(x =>
                        x.ModulePrivilege.Module.Name == module
                        && x.IsAllowed
                        && x.ModulePrivilege.Privilege.Name == PrivilegeName.VIEW.ToString());

                        if (userModulePrivilege != null)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        [AdminPrivilege]
        public virtual ActionResult Index(long? id)
        {
            this.controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();

            ListModel model = this.Init();
            if (model != null)
            {
                this.GenerateAlertViewbag();

                List<FilterOption> filters = new List<FilterOption>();
                this.AddDefaultFilter(filters);

                model.Data = this.GetModelData(id, filters);

                ViewBag.IndexID = id;
                ViewBag.KeyPropertyInfo = (Type.GetType(this.GetEntityType()) as Type).GetProperties().Where(x => x.IsDefined(typeof(KeyAttribute), true)).FirstOrDefault();

                ViewBag.Title = model.Title;

                return View(model);
            }
            else
                return View();
        }

        public virtual void AddDefaultFilter(List<FilterOption> filters)
        {
            User loggedInUser = this.GetLoggedInUser();

            if (filters == null)
                filters = new List<FilterOption>();

            //if (loggedInUser.Role != UserRole.SUPER.ToString())
            //{
            //    filters.Add(new FilterOption() { Type = FilterType.Property, Label = new string[] { "AssigneeID" }, Operator = FilterOperator.Equal, Value = loggedInUser.UserID, Unison = FilterUnison.And });
            //}
        }

        [HttpPost]
        public virtual JsonResult GetData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            List<object> result = new List<object>();

            if (filters == null)
                filters = new List<FilterOption>();

            this.AddDefaultFilter(filters);
            DataModel data = this.GetModelPagedData(page, filters, sort, indexID);

            this.controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            UserService userService = new UserService();
            this.loggedInUser = userService.Get(new object[] { this.GetLoggedInUserID() });

            ViewBag.Breadcrumbs = this.GetBreadcrumbItems();
            ViewBag.Columns = this.GetColumns();
            ViewBag.EntityType = this.GetEntityType();
            ViewBag.KeyPropertyInfo = (Type.GetType(this.GetEntityType()) as Type).GetProperties().Where(x => x.IsDefined(typeof(KeyAttribute), true)).FirstOrDefault();
            ViewBag.CanInsert = this.CanInsert(this.controllerName);
            ViewBag.CanEdit = this.CanEdit(this.controllerName);
            ViewBag.CanExport = this.CanExport(this.controllerName);
            ViewBag.CanDelete = this.CanDelete(this.controllerName);
            ViewBag.CanView = this.CanView(this.controllerName);

            foreach (var item in data.Collection)
            {
                result.Add(Utilities.RenderPartialViewToString(this, "_ListItemPartial", item));
            }

            return Json(new
            {
                Data = result,
                Total = data.Total,
            });
        }

        protected virtual string GetEntityType()
        {
            return string.Empty;
        }

        protected virtual DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return null;
        }

        protected virtual DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            return null;
        }

        protected virtual void InitDefaultFunctions(long? id)
        {
            this.controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();

            bool canEdit = this.CanEdit(this.controllerName);
            bool canDelete = this.CanDelete(this.controllerName);
            ViewBag.CanView = this.CanView(this.controllerName);

            ViewBag.FriendlyDetailModuleName = this.FriendlyDetailModuleName;

            this.GenerateAlertViewbag();

            DetailMode mode = new DetailMode();

            if (ViewBag.CanEdit == null)
            {
                ViewBag.CanEdit = canEdit;
            }

            if (id.HasValue && id.Value > 0)
            {
                if (ViewBag.CanEdit)
                {
                    mode = DetailMode.Edit;
                }
                else
                {
                    mode = DetailMode.View;
                }
            }
            else
            {
                mode = DetailMode.Insert;

                ViewBag.CanEdit = this.CanInsert(this.controllerName);
            }

            this.GetDetailBreadcrumbItems(mode);
        }

        protected virtual ListModel Init()
        {
            ListModel model = new ListModel();

            UserService userService = new UserService();
            this.loggedInUser = userService.Get(new object[] { this.GetLoggedInUserID() });

            model.Columns = ViewBag.Columns = this.GetColumns();
            model.SortOptions = this.GetSortList();

            ViewBag.EntityType = this.GetEntityType();
            ViewBag.Breadcrumbs = this.GetBreadcrumbItems();

            model.CanInsert = this.CanInsert(this.controllerName);
            model.CanExport = this.CanExport(this.controllerName);
            model.CanDelete = this.CanDelete(this.controllerName);
            model.Title = this.FriendlyIndexModuleName;

            bool canEdit = this.CanEdit(this.controllerName);

            if (this.ModuleWithActiveStatus.Count(x => x == this.controllerName) > 0)
                model.CanActivate = canEdit;
            else
                model.CanActivate = false;

            ViewBag.CanEdit = canEdit;
            ViewBag.CanExport = model.CanExport;
            ViewBag.CanInsert = model.CanInsert;
            ViewBag.CanDelete = model.CanDelete;
            ViewBag.FriendlyIndexModuleName = this.FriendlyIndexModuleName;

            return model;
        }

        protected virtual List<ListColumn> GetColumns()
        {
            return null;
        }

        protected virtual SelectList GetSortList()
        {
            return null;
        }

        protected virtual IEnumerable<BreadcrumbItem> GetBreadcrumbItems()
        {
            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = this.FriendlyIndexModuleName, Link = "" });

            ViewBag.Breadcrumbs = items;

            return items;
        }

        protected virtual IEnumerable<BreadcrumbItem> GetDetailBreadcrumbItems(DetailMode mode)
        {
            string title = FrameworkResources.ResourceManager.GetString(mode.ToString()) + " " + this.FriendlyDetailModuleName;

            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = this.FriendlyDetailModuleName, Link = Url.Action("Index", this.controllerName) });
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.ResourceManager.GetString(mode.ToString()) + " " + this.FriendlyDetailModuleName, Link = "" });

            ViewBag.Breadcrumbs = items;
            ViewBag.Title = title;

            return items;
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetCsv()
        {
            byte[] data = TempData["DataCsv"] as byte[];
            string titleCsv = TempData["TitleCsv"] as string;

            return File(data, "text/csv", titleCsv + DateTime.Now.ToString("ddyyhhmmss") + ".csv");
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetPdf()
        {
            byte[] data = TempData["DataPdf"] as byte[];
            string title = TempData["TitlePdf"] as string;

            return File(data, "text/pdf", title + DateTime.Now.ToString("ddyyhhmmss") + ".pdf");
        }


        protected SelectList GetActiveStatus()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Text = FrameworkResources.All, Value = "-1", Selected = true });
            list.Add(new SelectListItem() { Text = FrameworkResources.Active, Value = "true", Selected = false });
            list.Add(new SelectListItem() { Text = FrameworkResources.Inactive, Value = "false", Selected = false });

            return new SelectList(list, "Value", "Text");
        }

        protected SelectList GetRoles()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Text = FrameworkResources.All, Value = "-1", Selected = true });
            list.Add(new SelectListItem() { Text = FrameworkResources.Admin, Value = UserRole.ADMIN.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = FrameworkResources.User, Value = UserRole.USER.ToString(), Selected = false });

            return new SelectList(list, "Value", "Text");
        }

        [HttpGet]
        public ContentResult GetStates(string value)
        {
            StateService stateService = new StateService();

            //Generate filter
            List<FilterOption> filters = new List<FilterOption>();

            FilterOption filterValue = new FilterOption()
            {

                Label = new string[] { "Name" },
                Operator = FilterOperator.Like,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = value
            };

            filters.Add(filterValue);

            FilterOption filterIsActive = new FilterOption()
            {

                Label = new string[] { "IsActive" },
                Operator = FilterOperator.Equal,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = true
            };

            filters.Add(filterIsActive);

            IEnumerable<State> states = stateService.Get(filters).Take(this.DeveloperSettings.AutoCompletePageSize);

            var results = states.Select(x => new
            {
                StateID = x.StateID,
                Name = x.Name,
            }).ToArray();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            String jsonResult = JsonConvert.SerializeObject(results, settings);

            return this.Content(jsonResult, "application/json");
        }

        [HttpGet]
        public ContentResult GetCities(string value, long stateID)
        {
            CityService cityService = new CityService();

            //Generate filter
            List<FilterOption> filters = new List<FilterOption>();

            FilterOption filterValue = new FilterOption()
            {

                Label = new string[] { "Name" },
                Operator = FilterOperator.Like,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = value
            };

            FilterOption filterState = new FilterOption()
            {

                Label = new string[] { "StateID" },
                Operator = FilterOperator.Equal,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = stateID
            };

            FilterOption filterIsActive = new FilterOption()
            {

                Label = new string[] { "IsActive" },
                Operator = FilterOperator.Equal,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = true
            };

            filters.Add(filterIsActive);
            filters.Add(filterValue);
            filters.Add(filterState);

            IEnumerable<City> cities = cityService.Get(filters).Take(this.DeveloperSettings.AutoCompletePageSize);

            var results = cities.Select(x => new
            {
                CityID = x.CityID,
                Name = x.Name,
            }).ToArray();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            String jsonResult = JsonConvert.SerializeObject(results, settings);

            return this.Content(jsonResult, "application/json");
        }

        protected bool CanInsert(string moduleName)
        {
            return this.CanCustom(moduleName, PrivilegeName.INSERT.ToString());
        }

        protected bool CanEdit(string moduleName)
        {
            return this.CanCustom(moduleName, PrivilegeName.EDIT.ToString());
        }

        protected bool CanDelete(string moduleName)
        {
            return this.CanCustom(moduleName, PrivilegeName.DELETE.ToString());
        }

        protected bool CanExport(string moduleName)
        {
            return this.IsLoginSuper() ? false :
                this.CanCustom(moduleName, PrivilegeName.EXPORT.ToString());
        }

        protected bool CanView(string moduleName)
        {
            return this.CanCustom(moduleName, PrivilegeName.VIEW.ToString());
        }

        protected virtual bool CanCustom(string moduleName, string additionalPrivilege)
        {
            bool result = false;

            if (this.loggedInUser != null)
            {
                if (this.IsLoginSuper())
                {
                    result = true;
                }
                else
                {
                    if (additionalPrivilege == PrivilegeName.VIEW.ToString() || additionalPrivilege == PrivilegeName.EXPORT.ToString())
                    {
                        result = this.loggedInUser.UserModulePrivileges.FirstOrDefault(x =>
                             x.ModulePrivilege.Module.Name == moduleName
                             && x.ModulePrivilege.Privilege.Name == additionalPrivilege
                             && x.PrivilegeState.Name == PrivilegeStateName.ALLOWED.ToString()) != null;
                    }
                }
            }

            return result;
        }

        #region Export


        [HttpPost]
        public JsonResult Export(List<FilterOption> filters)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            this.AddDefaultFilter(filters);

            this.controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string message = GeneralResources.ReportDisplayed;
            string status = "success";

            string basePath = Server.MapPath("~/TemplateFiles");
            string path = Path.Combine(basePath, "ExportTemplate.xlsx");

            string title = "Export" + this.controllerName;
            string dateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            string tempWorkPath = Path.Combine(basePath, title + dateTime + ".xlsx");
            string downloadPath = this.GetRootPath() + "/TemplateFiles/" + title + dateTime + ".xlsx";

            try
            {
                CommonUtilities.DeleteFiles(basePath, CommonUtilities.GetFilesName(basePath, title + "*"));
            }
            catch (Exception e)
            {

            }

            System.IO.File.Copy(path, tempWorkPath, true);

            List<String> columns = this.GetExportColumn();
            List<object[]> objectDatas = this.GetExportObject(filters);

            if (objectDatas.Count != 0)
            {
                this.UpdateExcelColumn(columns.ToArray(), tempWorkPath, this.controllerName);
                this.SaveDataToExcel(objectDatas, columns.ToArray(), tempWorkPath, this.controllerName);
            }
            else
            {
                status = "fail";
                message = FrameworkResources.NoDataAvailable;
            }

            return Json(new
            {
                Status = status,
                Message = message,
                Data = downloadPath
            });
        }

        public JsonResult DeleteExport(string filePath)
        {
            string basePath = Server.MapPath("~/TemplateFiles");
            string path = Path.Combine(basePath, "ExportTemplate.xlsx");
            string title = "Export" + this.controllerName;
            List<string> filepath = CommonUtilities.GetFilesName(basePath, title + "*");

            int posA = filePath.LastIndexOf("TemplateFiles/");
            int adjustedPosA = posA + 14;
            string thefilepath = filePath.Substring(adjustedPosA);

            try
            {
                CommonUtilities.DeleteFiles(basePath, CommonUtilities.GetFilesName(basePath, thefilepath));
            }
            catch (Exception e)
            {

            }

            return null;
        }

        protected virtual List<String> GetExportColumn()
        {
            return null;
        }


        protected virtual List<object[]> GetExportObject(List<FilterOption> filters)
        {
            return null;
        }

        protected void UpdateExcelColumn(string[] columnNames, string path, string tableName)
        {
            OleDbConnection conn = null;
            try
            {
                string strConn = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + path +
                    "; Extended Properties=\"Excel 12.0; HDR=YES;\"";

                conn = new OleDbConnection(strConn);

                string createCommand = "CREATE TABLE [" + tableName + "] (";
                createCommand += string.Join(", ", columnNames.Select(x => "[" + x + "] char(255)").ToArray());
                createCommand += ")";

                OleDbCommand createCommObj = new OleDbCommand(createCommand, conn);

                conn.Open();
                createCommObj.ExecuteNonQuery();
                conn.Close();

            }
            finally
            {
                if (conn != null && conn.State != ConnectionState.Closed)
                    conn.Close();
            }
        }

        protected void SaveDataToExcel(List<object[]> datas, string[] columnNames, string path, string tableName)
        {
            OleDbConnection conn = null;
            try
            {
                string strConn = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + path +
                        "; Extended Properties=\"Excel 12.0; HDR=YES;\"";

                conn = new OleDbConnection(strConn);
                conn.Open();

                string strExcel = "select * from [" + tableName + "$]";
                OleDbDataAdapter da = new OleDbDataAdapter(strExcel, strConn);
                DataSet ds = new DataSet();

                OleDbCommandBuilder builder = new OleDbCommandBuilder(da);

                //Use QuotePrefix and QuoteSuffix when generating InsertComment for builder.
                builder.QuotePrefix = "["; //Get Metacharactor in insert sentence (initial position).
                builder.QuoteSuffix = "]"; //Get Metacharactor in insert sentence (end position).

                string insertCommand = string.Format("INSERT INTO [{0}$] ({1}) values ({2})", tableName,
                    string.Join(", ", columnNames.Select(x => { return "[" + x + "]"; }).ToArray()),
                    string.Join(", ", columnNames.Select(x => { return "?"; }).ToArray()));

                da.InsertCommand = new OleDbCommand(insertCommand, conn);

                string updateCommand = string.Format("UPDATE [{0}$] SET [{1}] = ? WHERE {2}", tableName,
                    columnNames.First(),
                    string.Join("and ", columnNames.Skip(1).Select(x => { return "[" + x + "] = ?"; }).ToArray()));

                da.UpdateCommand = new OleDbCommand(updateCommand, conn);

                da.Fill(ds, "Table1");

                foreach (string columnName in columnNames)
                {
                    da.InsertCommand.Parameters.Add("@" + columnName, OleDbType.VarChar, 255, columnName);
                    da.UpdateCommand.Parameters.Add("@" + columnName, OleDbType.VarChar, 255, columnName);
                }

                foreach (object[] data in datas)
                {
                    ds.Tables["Table1"].Rows.Add(data);
                }

                da.Update(ds, "Table1");

                conn.Close();

            }
            finally
            {
                if (conn != null && conn.State != ConnectionState.Closed)
                    conn.Close();
            }
        }

        #endregion



    }
}