﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Hosting;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public class HomeController : BaseController
    {
        protected Navigation BannerNavItems { get; set; }

        public ActionResult Index()
        {
            string bannerFilePath = HostingEnvironment.MapPath("~/App_Data/Navigation/banner.xml");
            this.BannerNavItems = CommonUtilities.GetObjectFromXml(bannerFilePath, typeof(Navigation)) as Navigation;
            ViewBag.BannerNavItems = this.BannerNavItems;

            RegisterViewModel model = new RegisterViewModel();
            ViewBag.Title = GeneralResources.Home;

            return View(model);
        }

        public bool IsValid(string senderName, string senderEmail, string subject, string shortMessage)
        {
            if (string.IsNullOrEmpty(senderName))
            {
                ViewBag.ContactUsErrorText = string.Format(FrameworkResources.FieldRequired, FrameworkResources.Name);
                return false;
            }
            else if (string.IsNullOrEmpty(senderEmail))
            {
                ViewBag.ContactUsErrorText = string.Format(FrameworkResources.FieldRequired, FrameworkResources.Email);
                return false;
            }
            else if (!ValidateEmail(senderEmail))
            {
                ViewBag.ContactUsErrorText = string.Format(FrameworkResources.FieldInvalid, FrameworkResources.Email);
                return false;
            }
            else if (string.IsNullOrEmpty(subject))
            {
                ViewBag.ContactUsErrorText = string.Format(FrameworkResources.FieldRequired, "Subject");
                return false;
            }
            else if (string.IsNullOrEmpty(shortMessage))
            {
                ViewBag.ContactUsErrorText = string.Format(FrameworkResources.FieldRequired, "Message");
                return false;
            }

            return true;
        }

        public bool ValidateEmail(string email)
        {
            try
            {
                MailAddress mailAddress = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}