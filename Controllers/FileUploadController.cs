﻿using Newtonsoft.Json;
using ReportingBusinessService;
using ReportingResources;
using ReportingWeb.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using DomainModelFramework;

namespace ReportingWeb.Controllers
{
    public class FileUploadController : BaseController
    {
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase upload, string type, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            string newFileName = "";
            string message = "";

            if (upload != null && upload.ContentLength != 0)
            {
                string pathForSaving = Server.MapPath("~/Images/Articles/temp");

                try
                {
                    using (Image img = Image.FromStream(upload.InputStream))
                    {
                        newFileName = this.ReEncodeImage(img, pathForSaving, upload.FileName, ".png");
                    }

                    message = GeneralResources.UploadSuccess;
                }
                catch (Exception ex)
                {
                    message = ErrorResources.UploadFailed + ":" + ex.Message;
                }

            }

            FileUploadResponseModel model = new FileUploadResponseModel();
            model.ExtraData = CKEditorFuncNum;
            model.ImagePath = !string.IsNullOrEmpty(newFileName) ?
                this.GetRootPath() + "Images/Articles/temp/" + newFileName : "";
            model.Message = message;

            return View(model);
        }

        [HttpPost]
        public ActionResult UploadFileGenerateThumbnail(string folderName, string imageName)
        {
            HttpPostedFileBase myFile = Request.Files[0];
            bool isUploaded = false;
            string message = ErrorResources.UploadFailed;
            string[] folderComponents = folderName.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            string variant1 = folderComponents.Length > 1 ? folderComponents.Last() : "";

            string newFileName = "";

            if (myFile != null && myFile.ContentLength != 0)
            {
                string pathForSaving = Server.MapPath("~/" + folderName);

                try
                {
                    using (Image img = Image.FromStream(myFile.InputStream))
                    {
                        if (!string.IsNullOrEmpty(imageName))
                        {
                            newFileName = this.ReEncodeImage(img, pathForSaving, imageName, ".png");
                        }
                        else
                            newFileName = this.ReEncodeImage(img, pathForSaving, myFile.FileName, ".png");
                    }

                    string thumbsImageFolder = Path.Combine(pathForSaving, "thumbs");

                    if (!Directory.Exists(thumbsImageFolder))
                    {
                        Directory.CreateDirectory(thumbsImageFolder);
                    }

                    //Create thumbnail
                    this.ResizeImage(Path.Combine(thumbsImageFolder, newFileName),
                        Path.Combine(pathForSaving, newFileName),
                        0, this.DeveloperSettings.ImageThumbnailDisplayHeight);

                    isUploaded = true;
                    message = GeneralResources.UploadSuccess;
                }
                catch (Exception ex)
                {
                    message = ErrorResources.UploadFailed + ":" + ex.Message;
                }
            }
            return Json(new
            {
                isUploaded = isUploaded,
                message = message,
                fileName = newFileName,
                variant1 = variant1,
            }, "text/html");
        }
        [HttpGet]
        public ActionResult BrowseFile(string type, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            //TODO: need processing if used
            return View();
        }
        public ActionResult DropFile(HttpPostedFileBase upload, string ckCsrfToken)
        {
            bool isUploaded = false;
            string newFileName = "";
            string message = "";

            if (upload != null && upload.ContentLength != 0)
            {
                string pathForSaving = Server.MapPath("~/Images/Articles/temp/");

                try
                {
                    using (Image img = Image.FromStream(upload.InputStream))
                    {
                        Dictionary<string, string> results = this.SaveImage(img, pathForSaving, upload.FileName);
                        newFileName = results.FirstOrDefault().Key;
                    }

                    isUploaded = true;
                    message = GeneralResources.UploadSuccess;
                }
                catch (Exception ex)
                {
                    message = ErrorResources.UploadFailed + ":" + ex.Message;
                }

            }

            var result = new
            {
                uploaded = isUploaded ? "1" : "0",
                fileName = newFileName,
                url = !string.IsNullOrEmpty(newFileName) ?
                    this.GetRootPath() + "Images/Articles/temp/" + newFileName : "",
                error = new
                {
                    message = message
                }
            };

            JsonSerializerSettings settings = new JsonSerializerSettings();
            String jsonResult = JsonConvert.SerializeObject(result, settings);
            return this.Content(jsonResult, "application/json");
        }
        /*       public FileContentResult GetImage(string keyName)
               {
                   KeyValuePair<string, MemoryStream> result = CloudUtilities.ReadObjectData(keyName).FirstOrDefault();

                   if (result.Value.Length > 0)
                   {
                       return File(result.Value.ToArray(), result.Key);
                   }

                   return null;
               }*/
        private Dictionary<string, string> SaveImage(Image oldImage, string savePath, string fileName)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            string keyName = "";
            string contentType = "";

            if (this.CreateFolderIfNeeded(savePath))
            {
                string[] fileNames = fileName
                    .Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries)
                    .ToArray();

                Graphics gInput = Graphics.FromImage(oldImage);

                string extension = GetExtension(oldImage.RawFormat);

                if (!string.IsNullOrEmpty(extension))
                {
                    string newFileName = string.Join(".", fileNames.Take(fileNames.Length - 1).ToArray());
                    string tempGuid = Guid.NewGuid().ToString("N");
                    newFileName = string.Format("{0}_{1}{2}", newFileName, tempGuid, extension);
                    keyName = "Temp/" + newFileName;
                    using (var tempImage = new Bitmap(oldImage))
                    {
                        #region Save to local temp folder
                        string fullPath = Path.Combine(savePath, newFileName);
                        tempImage.Save(fullPath, oldImage.RawFormat);

                        this.CreateLog(LogPriority.HIGH, "ClientSaveImage", DateTime.Now + ": " + fullPath);

                        #endregion

                        #region save to S3 Cloud storage

                        //contentType = "image/" + extension.Replace(".", "");
                        //CloudUtilities.WritingAnObject(keyName, fullPath, contentType, null);
                        this.CreateLog(LogPriority.HIGH, "CloudSaveTempImage", DateTime.Now + ": " + keyName);

                        #endregion

                        result.Add(newFileName, Url.Action("GetImage", new { keyName = keyName, contentType = contentType }));
                    }
                }
                else
                {
                    throw new Exception(ErrorResources.JpgOnly);
                }

            }
            return result;
        }
        private string GetExtension(ImageFormat currentImgFormat)
        {
            string extension = "";

            if (ImageFormat.Jpeg.Equals(currentImgFormat))
            {
                extension = ".jpg";
            }
            else if (ImageFormat.Png.Equals(currentImgFormat))
            {
                extension = ".png";
            }
            //else if (ImageFormat.Gif.Equals(currentImgFormat))
            //{
            //    extension = ".gif";
            //}

            return extension;
        }
    }
}