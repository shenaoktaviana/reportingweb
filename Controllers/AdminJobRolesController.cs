﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    [Module(Value = "JobRoles")]
    public class AdminJobRolesController : BaseAdminController
    {
        private JobRoleService _serviceprop { get; set; }

        public AdminJobRolesController()
            : base()
        {
            this.controllerName = "AdminJobRoles";
            this.FriendlyIndexModuleName = GeneralResources.JobRoles;
            this.FriendlyDetailModuleName = GeneralResources.JobRole;
        }

        protected override string GetEntityType()
        {
            return "ReportingDomainModel.JobRole, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return this.GetModuleService().Get(filters, "Latest", 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            return this.GetModuleService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        protected override List<ListColumn> GetColumns()
        {
            List<ListColumn> columns = new List<ListColumn>();

            columns.Add(new ListColumn()
            {
                DisplayMode = ListColumnDisplayMode.CheckboxWithSelectAll,
                Width = "1%"
            });

            columns.Add(new ListColumn()
            {
                DisplayMode = ListColumnDisplayMode.Custom,
                CustomPartialName = "_ActiveMarkerPartial",
                Width = "5%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Name",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "Name",
                HeaderText = GeneralResources.Name,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Jobdesk",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "TrimmedJobdesk",
                HeaderText = GeneralResources.Jobdesk,
                Width = "15%"
            });

            columns.Add(new ListColumn()
            {
                Name = "TotalActiveUsers",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "TotalActiveUsers",
                HeaderText = GeneralResources.Active,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "TotalInactiveUsers",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "TotalInactiveUsers",
                HeaderText = GeneralResources.Inactive,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Parent",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "ParentJobRoleName",
                HeaderText = GeneralResources.Parent,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Actions",
                DisplayMode = ListColumnDisplayMode.Custom,
                Width = "8%",
                CustomPartialName = "_JobRoleActionPartial"
            });

            return columns;
        }

        protected override SelectList GetSortList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem() { Text = GeneralResources.LatestUpdated, Value = SortOption.LatestUpdated.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.LatestInserted, Value = SortOption.LatestInserted.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstUpdated, Value = SortOption.FirstUpdated.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.FirstInserted, Value = SortOption.FirstInserted.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.NameAscending, Value = SortOption.NameAscending.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.NameDescending, Value = SortOption.NameDescending.ToString(), Selected = false });

            return new SelectList(list, "Value", "Text");
        }

        #region GeneralMethods

        private JobRoleService GetModuleService()
        {
            if (this._serviceprop == null)
                this._serviceprop = new JobRoleService(this.GetLoggedInUserID());

            return this._serviceprop;
        }

        private string GetDisplayedData(JobRole model)
        {
            return model.Name;
        }

        private long GetModelID(JobRole model)
        {
            if (model != null)
                return model.JobRoleID;

            return 0;
        }

        #endregion

        #region Initialize

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            model.Filters = new List<ListFilterItem>() {
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Property, Label = new string[] { "Name" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "Name",
                    Text = GeneralResources.Name,
                    ControlType = ControlType.Textbox,
                    Width = "100%",
                    Alignment = Alignment.Right
                }
            };

            return model;
        }

        #endregion

        #region Index

        [AdminPrivilege]
        public override ActionResult Index(long? id)
        {
            return base.Index(id);
        }

        [HttpPost]
        public ActionResult Delete(long itemID)
        {
            string message = "";
            string status = "success";

            try
            {
                JobRole model = this.GetModuleService().GetBy(x => x.JobRoleID == itemID).FirstOrDefault();

                if (model.UserJobRoles.Count() > 0)
                {
                    message = string.Format(ErrorResources.CannotDeleteAlreadyUsed, this.GetDisplayedData(model));
                    status = "fail";
                }
                else
                {
                    this.GetModuleService().DeleteJobRole(model);
                    message += this.GetDisplayedData(model) + " " + FrameworkResources.Deleted;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message,
                    DisableHide = status == "success"
                });
        }

        #endregion

        #region Detail

        [AdminPrivilege(PrivilegeNames = new string[] { "INSERT", "EDIT" })]
        public ActionResult Detail(long? id)
        {
            JobRole model = this.SetUpModel(id);
            return View(model);
        }

        [AdminPrivilege]
        public ActionResult View(long? id)
        {
            ViewBag.CanEdit = false;

            JobRole model = this.SetUpModel(id);
            return View("Detail", model);
        }

        [HttpPost]
        public ActionResult Detail(JobRole model)
        {
            ViewBag.Breadcrumbs = this.GetDetailBreadcrumbItems(this.GetModelID(model) > 0 ? DetailMode.Edit : DetailMode.Insert);
            ViewBag.Title = (this.GetModelID(model) == 0 ? FrameworkResources.Edit : FrameworkResources.Insert) + " " + this.FriendlyDetailModuleName;

            Dictionary<string, string> message = new Dictionary<string, string>();

            try
            {
                if (this.IsValid(model))
                {
                    if (model.JobRoleID == 0)
                    {
                        this.GetModuleService().InsertJobRole(model);
                        message.Add("Success", this.GetDisplayedData(model) + " " + FrameworkResources.Inserted);
                    }
                    else
                    {
                        this.GetModuleService().UpdateJobRole(model);
                        message.Add("Success", this.GetDisplayedData(model) + " " + FrameworkResources.Updated);
                    }

                    TempData["Message"] = message;
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorDetail = CommonUtilities.GetExceptionMessage(e);
            }

            this.InitDefaultFunctions(this.GetModelID(model));

            return View(model);
        }

        private JobRole SetUpModel(long? id)
        {
            JobRole model = null;

            if (id.HasValue && id.Value > 0)
                model = this.GetModuleService().GetBy(x => x.JobRoleID == id.Value).FirstOrDefault();
            else
                model = new JobRole();

            this.InitDefaultFunctions(id);
            return model;
        }

        private bool IsValid(JobRole model)
        {
            bool isValid = true;

            if (!this.ModelState.IsValid)
                isValid = false;

            if (isValid)
            {
                JobRole existingData = this.GetModuleService().GetBy(x => x.JobRoleID != this.GetModelID(model) &&
                    x.Name.ToLower().Equals(model.Name.ToLower())).FirstOrDefault();

                if (existingData != null)
                {
                    isValid = false;
                    ViewBag.ErrorDetail = string.Format(ErrorResources.AlreadyExist, this.GetDisplayedData(model));
                }
            }

            return isValid;
        }

        #endregion

        #region Action

        [HttpPost]
        public ActionResult ChangeStatus(List<long> checkedIds, bool activeInactive)
        {
            string message = "";
            string status = "success";

            try
            {
                IEnumerable<JobRole> items = this.GetModuleService().GetBy(x => checkedIds.Contains(x.JobRoleID));

                string itemNames = "";

                foreach (JobRole item in items)
                {
                    item.IsActive = activeInactive;
                    itemNames += item.Name + ", ";
                }

                if (itemNames.Length > 2)
                    itemNames = itemNames.Substring(0, itemNames.Length - 2);

                items = this.GetModuleService().UpdateJobRoles(items);

                if (activeInactive)
                {
                    message = itemNames + " " + FrameworkResources.Activated;
                }
                else
                {
                    message = itemNames + " " + FrameworkResources.Deactivated;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message
                });
        }

        #endregion
    }
}