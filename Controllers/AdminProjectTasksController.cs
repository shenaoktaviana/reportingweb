﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using ReportingWeb.ActionFilters;
using ReportingWeb.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    [Module(Value = "ProjectTasks")]
    public class AdminProjectTasksController : BaseAdminController
    {
        private ProjectTaskService _serviceprop { get; set; }

        public AdminProjectTasksController()
            : base()
        {
            this.controllerName = "AdminProjectTasks";
            this.FriendlyIndexModuleName = GeneralResources.ProjectTasks;
            this.FriendlyDetailModuleName = GeneralResources.ProjectTask;
        }

        protected override string GetEntityType()
        {
            return "ReportingDomainModel.ProjectTask, ReportingDomainModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }

        protected override DataModel GetModelData(long? id, List<FilterOption> filters)
        {
            return this.GetModuleService().Get(filters, "Latest", 1, this.GeneralSettings.AdminPageSize);
        }

        protected override DataModel GetModelPagedData(int page, List<FilterOption> filters, string sort, long? indexID)
        {
            if (filters == null)
                filters = new List<FilterOption>();

            return this.GetModuleService().Get(filters, sort, page, this.GeneralSettings.AdminPageSize);
        }

        protected override List<ListColumn> GetColumns()
        {
            List<ListColumn> columns = new List<ListColumn>();

            columns.Add(new ListColumn()
            {
                DisplayMode = ListColumnDisplayMode.CheckboxWithSelectAll,
                Width = "1%"
            });

            columns.Add(new ListColumn()
            {
                DisplayMode = ListColumnDisplayMode.Custom,
                CustomPartialName = "_ActiveMarkerPartial",
                Width = "6%"
            });

            columns.Add(new ListColumn()
            {
                Name = "ProjectName",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "Project.Name",
                HeaderText = GeneralResources.Project,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Name",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "TrimmedNameWithIsRoutine",
                HeaderText = GeneralResources.TaskName,
                Width = "25%"
            });

            columns.Add(new ListColumn()
            {
                Name = "AssigneeNames",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "AssigneeNames",
                HeaderText = GeneralResources.Assignee,
                Width = "15%"
            });

            columns.Add(new ListColumn()
            {
                Name = "ParentName",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "TrimmedParentProjectTaskName",
                HeaderText = GeneralResources.ParentName,
                Width = "15%"
            });

            columns.Add(new ListColumn()
            {
                Name = "TargetDate",
                DisplayMode = ListColumnDisplayMode.Text,
                TextPropertyName = "TargetDate",
                HeaderText = GeneralResources.TargetDate,
                Width = "10%"
            });

            columns.Add(new ListColumn()
            {
                Name = "Actions",
                DisplayMode = ListColumnDisplayMode.Custom,
                Width = "8%",
                CustomPartialName = "_ProjectTaskActionPartial"
            });

            return columns;
        }

        protected override SelectList GetSortList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem() { Text = GeneralResources.LatestUpdated, Value = SortOption.LatestUpdated.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.LatestInserted, Value = SortOption.LatestInserted.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.NameAscending, Value = SortOption.NameAscending.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.NameDescending, Value = SortOption.NameDescending.ToString(), Selected = false });
            list.Add(new SelectListItem() { Text = GeneralResources.ProjectAscending, Value = SortOption.ProjectNameAscending.ToString(), Selected = true });
            list.Add(new SelectListItem() { Text = GeneralResources.ProjectDescending, Value = SortOption.ProjectNameDescending.ToString(), Selected = false });

            return new SelectList(list, "Value", "Text");
        }

        #region GeneralMethods

        private ProjectTaskService GetModuleService()
        {
            if (this._serviceprop == null)
                this._serviceprop = new ProjectTaskService(this.GetLoggedInUserID());

            return this._serviceprop;
        }

        private string GetDisplayedData(ProjectTask model)
        {
            return model.Name;
        }

        private long GetModelID(ProjectTask model)
        {
            if (model != null)
                return model.ProjectTaskID;

            return 0;
        }

        #endregion

        #region Initialize

        protected override ListModel Init()
        {
            ListModel model = base.Init();

            model.Filters = new List<ListFilterItem>() {
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "TaskName" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "TaskName",
                    Text = GeneralResources.Name,
                    ControlType = ControlType.Textbox,
                    Width = "50%",
                    Alignment = Alignment.Right
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "ProjectName" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "ProjectName",
                    Text = GeneralResources.Project,
                    ControlType = ControlType.Textbox,
                    Width = "50%",
                    Alignment = Alignment.Right
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "AssigneeName" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "AssigneeName",
                    Text = GeneralResources.Assignee,
                    ControlType = ControlType.Textbox,
                    Width = "50%",
                    Alignment = Alignment.Right
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.Custom, Label = new string[] { "ParentName" }, Operator = FilterOperator.Like, Unison = FilterUnison.And },
                    Name = "ParentName",
                    Text = GeneralResources.ParentName,
                    ControlType = ControlType.Textbox,
                    Width = "50%",
                    Alignment = Alignment.Right
                },
                new ListFilterItem()
                {
                    FilterOption = new FilterOption() { Type = FilterType.DateProperty, Label = new string[] { "TargetDate" }, Operator = FilterOperator.Equal, Unison = FilterUnison.And },
                    Name = "TargetDate",
                    Text = GeneralResources.TargetDate,
                    ControlType = ControlType.DatePicker,
                    Width = "50%",
                    Alignment = Alignment.Right
                }
            };

            return model;
        }

        #endregion

        #region Index

        [AdminPrivilege]
        public override ActionResult Index(long? id)
        {
            return base.Index(id);
        }

        [HttpPost]
        public ActionResult Delete(long itemID)
        {
            string message = "";
            string status = "success";

            try
            {
                ProjectTask model = this.GetModuleService().GetBy(x => x.ProjectTaskID == itemID).FirstOrDefault();

                if (model.ProjectTaskMemberDetails.Count() > 0 || model.ChildProjectTasks.Count() > 0)
                {
                    message = string.Format(ErrorResources.CannotDeleteAlreadyUsed, GeneralResources.ProjectTask);
                    status = "fail";
                }
                else
                {
                    this.GetModuleService().DeleteProjectTask(model);

                    message += this.GetDisplayedData(model) + " " + FrameworkResources.Deleted;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message,
                    DisableHide = status == "success"
                });
        }

        #endregion

        #region Detail

        [AdminPrivilege(PrivilegeNames = new string[] { "INSERT", "EDIT" })]
        public ActionResult Detail(long? id)
        {
            ProjectTask model = this.SetUpModel(id);
            return View(model);
        }

        [AdminPrivilege]
        public ActionResult View(long? id)
        {
            ViewBag.CanEdit = false;
            ProjectTask model = this.SetUpModel(id);
            return View("Detail", model);
        }

        [HttpPost]
        public ActionResult Detail(ProjectTask model)
        {
            long modelID = this.GetModelID(model);

            ViewBag.Breadcrumbs = this.GetDetailBreadcrumbItems(modelID > 0 ? DetailMode.Edit : DetailMode.Insert);
            ViewBag.Title = (modelID == 0 ? FrameworkResources.Edit : FrameworkResources.Insert) + " " + GeneralResources.Project;

            Dictionary<string, string> message = new Dictionary<string, string>();

            try
            {
                if (this.IsValid(model))
                {
                    if (model.ParentProjectTask != null)
                    {
                        model.ProjectID = model.ParentProjectTask.ProjectID;
                        model.ProjectName = model.ParentProjectTask.Project.Name;
                    }

                    if (model.IsClosed && model.ClosedDate == null)
                        model.ClosedDate = DateTime.Now;
                    else if (!model.IsClosed)
                        model.ClosedDate = null;

                    if (modelID == 0)
                    {
                        this.GetModuleService().InsertProjectTask(model);
                        message.Add("Success", model.Name + " " + FrameworkResources.Inserted);
                    }
                    else
                    {
                        this.GetModuleService().UpdateProjectTask(model);
                        message.Add("Success", model.Name + " " + FrameworkResources.Updated);
                    }

                    TempData["Message"] = message;
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorDetail = CommonUtilities.GetExceptionMessage(e);
            }

            this.InitDefaultFunctions(model.ProjectID);

            return View(model);
        }

        private ProjectTask SetUpModel(long? id)
        {
            ProjectTask model = null;

            if (id.HasValue && id.Value > 0)
            {
                model = this.GetModuleService().GetBy(x => x.ProjectTaskID == id.Value).FirstOrDefault();
                model.ProjectName = model.Project.Name;
                model.ParentName = model.ParentProjectTask != null ? model.ParentProjectTask.Name : string.Empty;
            }
            else
            {
                model = new ProjectTask();
                model.IsActive = true;
                model.Priority = "MEDIUM";
            }

            this.InitDefaultFunctions(id);
            return model;
        }

        private bool IsValid(ProjectTask model)
        {
            bool isValid = true;

            if (!this.ModelState.IsValid)
                isValid = false;

            if (model.ProjectID == 0)
            {
                isValid = false;
                this.ModelState.AddModelError("ProjectName", string.Format(ErrorResources.FieldRequired, GeneralResources.Project));
            }

            if (model.ParentTaskID == model.ProjectTaskID)
                this.ModelState.AddModelError("ParentProjectTask", string.Format(ErrorResources.FieldInvalid, GeneralResources.Parent));

            if (isValid)
            {
                long modelID = this.GetModelID(model);

                ProjectTask existingData = this.GetModuleService().GetBy(x => x.ProjectTaskID != modelID
                    && x.Name.ToLower().Trim() == model.Name.ToLower().Trim()).FirstOrDefault();

                if (existingData != null)
                {
                    isValid = false;
                    ViewBag.ErrorDetail = string.Format(ErrorResources.AlreadyExist, this.GetDisplayedData(existingData));
                }

                if (model.ParentTaskID != null)
                {
                    IEnumerable<ProjectTask> childTasks = this.GetModuleService().GetBy(x => x.ParentTaskID == model.ProjectTaskID);

                    if (childTasks.Count() > 0)
                    {
                        isValid = false;
                        this.ModelState.AddModelError("ParentProjectTask", string.Format(ErrorResources.CannotSelectParent));
                    }
                    else
                    {
                        ProjectTask parentTask = this.GetModuleService().GetBy(x => x.ProjectTaskID == model.ParentTaskID).FirstOrDefault();

                        if (parentTask != null)
                        {
                            if (parentTask.ParentTaskID != null)
                            {
                                isValid = false;
                                this.ModelState.AddModelError("ParentProjectTask", string.Format(ErrorResources.CannotSelectAsParent, this.GetDisplayedData(parentTask)));
                            }
                            else if (parentTask.ProjectTaskMemberDetails.Count() > 0)
                            {
                                isValid = false;
                                this.ModelState.AddModelError("ParentProjectTask", string.Format(ErrorResources.CannotSelectAsParentAlreadyUsedInTaskMember, this.GetDisplayedData(parentTask)));
                            }
                        }
                    }
                }
            }

            return isValid;
        }

        #endregion

        #region Action

        [HttpPost]
        public ActionResult ChangeStatus(List<long> checkedIds, bool activeInactive)
        {
            string message = "";
            string status = "success";

            try
            {
                IEnumerable<ProjectTask> items = this.GetModuleService().GetBy(x => checkedIds.Contains(x.ProjectTaskID));

                string itemNames = "";

                foreach (ProjectTask item in items)
                {
                    item.IsActive = activeInactive;
                    itemNames += item.Name + ", ";
                }

                if (itemNames.Length > 2)
                    itemNames = itemNames.Substring(0, itemNames.Length - 2);

                items = this.GetModuleService().UpdateProjectTasks(items);

                if (activeInactive)
                    message = itemNames + " " + FrameworkResources.Activated;
                else
                    message = itemNames + " " + FrameworkResources.Deactivated;
            }
            catch (Exception e)
            {
                message = e.Message;
                status = "fail";
            }

            return Json(
                new
                {
                    Status = status,
                    Message = message
                });
        }

        #endregion

        #region GetData

        [HttpGet]
        public ContentResult GetParentTasks(string term)
        {
            //Generate filter
            List<FilterOption> filters = new List<FilterOption>();

            FilterOption filterValue = new FilterOption()
            {
                Label = new string[] { "Name" },
                Operator = FilterOperator.Like,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = term
            };
            filters.Add(filterValue);

            FilterOption filterIsActive = new FilterOption()
            {
                Label = new string[] { "IsActive" },
                Operator = FilterOperator.Equal,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = true
            };
            filters.Add(filterIsActive);

            ProjectTaskService service = new ProjectTaskService();
            IEnumerable<ProjectTask> items = service.Get(filters).OrderBy(x => x.Project.Name).Take(this.DeveloperSettings.AutoCompletePageSize);

            var results = items.Select(x => new
            {
                itemID = x.ProjectTaskID,
                itemName = x.Project.Name + " - " + x.Name,
            }).ToArray();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            String jsonResult = JsonConvert.SerializeObject(results, settings);

            return this.Content(jsonResult, "application/json");
        }

        [HttpGet]
        public ContentResult GetProjects(string term)
        {
            //Generate filter
            List<FilterOption> filters = new List<FilterOption>();

            FilterOption filterValue = new FilterOption()
            {
                Label = new string[] { "Name" },
                Operator = FilterOperator.Like,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = term
            };
            filters.Add(filterValue);

            FilterOption filterIsActive = new FilterOption()
            {
                Label = new string[] { "IsActive" },
                Operator = FilterOperator.Equal,
                Type = FilterType.Property,
                Unison = FilterUnison.And,
                Value = true
            };
            filters.Add(filterIsActive);

            ProjectService service = new ProjectService();
            IEnumerable<Project> items = service.Get(filters).OrderBy(x => x.Name).Take(this.DeveloperSettings.AutoCompletePageSize);

            var results = items.Select(x => new
            {
                itemID = x.ProjectID,
                itemName = x.Code + " - " + x.Name,
            }).ToArray();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            String jsonResult = JsonConvert.SerializeObject(results, settings);

            return this.Content(jsonResult, "application/json");
        }

        #endregion
    }
}