﻿using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public class ReportsController : BaseAdminController
    {
        #region Reports Main Page
        public override ActionResult Index(long? id)
        {
            UserService userService = new UserService();
            User loggedInUser = userService.Get(new object[] { this.GetLoggedInUserID() });

            this.GenerateAlertViewbag();

            List<String> reportList = this.GetReportModulePrivilege();

            ViewBag.Breadcrumbs = this.GetReportsBreadcrumbs();
            ViewBag.Title = GeneralResources.Reports;
            return View(reportList);
        }

        #endregion

        protected List<String> GetReportModulePrivilege()
        {
            User loggedInUser = this.GetLoggedInUser();

            if (loggedInUser.Role == UserRole.SUPER.ToString() || loggedInUser.ParentID == null)
            {
                ModuleService moduleService = new ModuleService();
                List<String> reportList = moduleService.GetBy(x => x.Name.Contains("Report")).Select(x => x.Name).ToList();

                return reportList;
            }
            else
            {
                List<UserModulePrivilege> userModulePrivileges = loggedInUser.UserModulePrivileges.Where(x =>
                          x.ModulePrivilege.Module.Name.Contains("Report")
                          && x.ModulePrivilege.Privilege.Name == PrivilegeName.VIEW.ToString()
                          && x.PrivilegeState.Name == PrivilegeStateName.ALLOWED.ToString()).ToList();

                List<String> reportList = new List<string>();

                foreach (UserModulePrivilege userModulePrivilege in userModulePrivileges)
                {
                    reportList.Add(userModulePrivilege.ModulePrivilege.Module.Name.ToString());
                }
                return reportList;
            }
        }

        protected IEnumerable<BreadcrumbItem> GetReportsBreadcrumbs()
        {
            List<BreadcrumbItem> items = new List<BreadcrumbItem>();
            items.Add(new BreadcrumbItem() { Text = FrameworkResources.Home, Link = this.GetDefaultReturnPage() });
            items.Add(new BreadcrumbItem() { Text = "Report", Link = "" });

            return items;
        }
    }
}