﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReportingBusinessService;
using ReportingDomainModel;
using ReportingWeb.Models;
using Newtonsoft.Json;
using ReportingResources;

namespace ReportingWeb.Controllers
{
    public class CookiesController : BaseController
    {
        protected const string DEFAULT_REWARD_IMAGE = "default-rewards.png";

        [HttpPost]
        public JsonResult Recreate(long orderId)
        {
            HttpCookie cookie = new HttpCookie("Cart");

            //TODO recreate cookie, check duplicate

            cookie.Expires = DateTime.UtcNow.AddDays(7);
            Response.Cookies.Add(cookie);

            var result = new { status = "OK" };

            return Json(result);
        }

        //public void CreateLoginCookie(StoreType loginType)
        //{
        //    HttpCookie loginTypeCookie = new HttpCookie("LoginType");

        //    if (Request.Cookies.AllKeys.Contains("LoginType"))
        //    {
        //        loginTypeCookie = Request.Cookies["LoginType"];
        //    }
        //    else
        //    {
        //        loginTypeCookie = new HttpCookie("LoginType");
        //    }

        //    loginTypeCookie.Value = loginType.ToString();
        //    loginTypeCookie.Expires = DateTime.UtcNow.AddDays(14);

        //    this.Response.Cookies.Add(loginTypeCookie);
        //}

        public void RemoveCookie(string cookieName)
        {
            if (this.Request.Cookies.AllKeys.Contains(cookieName))
            {
                HttpCookie cookie = this.Request.Cookies[cookieName];
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.Response.Cookies.Add(cookie);
            }
        }
    }
}