﻿using DomainModelFramework;
using ReportingWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult Index(string value)
        {
            ErrorModel errorModel = new ErrorModel(string.Empty, null);

            if (!string.IsNullOrEmpty(value))
            {
                byte[] encodedValueBytes = Convert.FromBase64String(value);
                string encodedValues = Encoding.UTF8.GetString(encodedValueBytes);

                string[] valueArrays = encodedValues.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                string code = valueArrays[0];
                string msg = valueArrays[1];


                HttpErrorCode? errCode;
                msg = Server.UrlDecode(msg);

                if (!string.IsNullOrWhiteSpace(code))
                {
                    errCode = (HttpErrorCode)Enum.Parse(typeof(HttpErrorCode), code, true);
                }
                else
                {
                    errCode = null;
                }


                errorModel = new ErrorModel(msg, errCode);
            }

            return View(errorModel);


        }

        public ActionResult ErrorView()
        {
            ErrorModel model = (ErrorModel)TempData["ErrorModel"];

            return View(model);
        }
    }
}