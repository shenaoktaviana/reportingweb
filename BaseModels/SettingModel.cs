﻿using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb.BaseModels
{
    public class SettingModel
    {
        public SettingModel()
        {
            this.NonRequiredProperties = new List<string>();
            this.HiddenProperties = new List<string>();
            this.ImageProperties = new List<string>();
            this.TextAreaProperties = new List<string>();
            this.DateProperties = new List<string>();
            this.PhoneProperties = new List<string>();
            this.IntegerProperties = new List<string>();
            this.DropdownProperties = new Dictionary<string, List<SelectListItem>>();
        }
        public string CurrentTab { get; set; }
        public string Title { get; set; }
        public List<string> Styles { get; set; }
        public List<string> Scripts { get; set; }
        public BaseSettings Data { get; set; }
        public Type EntityType { get; set; }
        public string XMLPath { get; set; }
        public List<string> NonRequiredProperties { get; set; }
        public List<string> PhoneProperties { get; set; }
        public List<string> IntegerProperties { get; set; }
        public List<string> DecimalProperties { get; set; }
        public List<string> NegativeDecimalProperties { get; set; }
        public List<string> HiddenProperties { get; set; }
        public List<string> ImageProperties { get; set; }
        public List<string> TextAreaProperties { get; set; }
        public List<string> DateProperties { get; set; }
        public Dictionary<string, List<SelectListItem>> DropdownProperties { get; set; }
        public List<SettingCustomPropertyModel> CustomProperties { get; set; }
    }
}