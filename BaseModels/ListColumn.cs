﻿using DomainModelFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace ReportingWeb.Models
{
    public class ListColumn
    {
        public string Name { get; set; }

        public string HeaderText { get; set; }

        public ListColumnDisplayMode DisplayMode { get; set; }

        public string Width { get; set; }

        public string Text { get; set; }

        public string TextPropertyName { get; set; }

        public string CustomPartialName { get; set; }
    }
}