﻿using BusinessServiceFramework.Models;
using DomainModelFramework;
using ReportingWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ReportingWeb.Models
{
    public class DetailModel
    {
        /* Keeping just in case needed for reference / build
        public DetailModel()
        {
            this.NonRequiredProperties = new List<string>();
            this.HiddenProperties = new List<string>();
            this.ImageProperties = new List<string>();
            this.TextAreaProperties = new List<string>();
            this.TextEditorProperties = new List<string>();
            this.DateProperties = new List<string>();
            this.PhoneProperties = new List<string>();
            this.IntegerProperties = new List<string>();
            this.DropdownProperties = new Dictionary<string, List<SelectListItem>>();
        }
        public string Title { get; set; }
        public List<string> Styles { get; set; }
        public List<string> Scripts { get; set; }
        public Type EntityType { get; set; }
        public List<string> NonRequiredProperties { get; set; }
        public List<string> PhoneProperties { get; set; }
        public List<string> IntegerProperties { get; set; }
        public List<string> DecimalProperties { get; set; }
        public List<string> NegativeDecimalProperties { get; set; }
        public List<string> HiddenProperties { get; set; }
        public List<string> ImageProperties { get; set; }
        public List<string> TextAreaProperties { get; set; }
        public List<string> TextEditorProperties { get; set; }
        public List<string> DateProperties { get; set; }
        public Dictionary<string, List<SelectListItem>> DropdownProperties { get; set; }
        public Dictionary<string, string> CustomProperties { get; set; } 
        */
        
        public DetailMode Mode { get; set; }
        public string Title { get; set; }
        public List<string> Styles { get; set; }
        public List<string> Scripts { get; set; }
        public List<DetailPropertyModel> DetailPropertyModel { get; set; }
        public object Entity { get; set; }
    }
}