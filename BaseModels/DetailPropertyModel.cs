﻿using BusinessServiceFramework.Models;
using DomainModelFramework;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ReportingWeb.Models
{
    public class DetailPropertyModel
    {
        public string PropertyName { get; set; }
        public string TextPropertyName { get; set; }
        public bool IsRequired { get; set; } = true;
        public ControlType ControlType { get; set; } 
        public string CustomControlPartial { get; set; }//custom partial name
        public string AutoCompleteTextboxID { get; set; }//for autocomplete's custom textbox ID / name
        public string CustomAttributes { get; set; } //for adding custom attributes at control element
        public string TextCSSClasses { get; set; }//for defining css class at text element
        public string ControlCSSClasses { get; set; }//for defining css class at control element
        public SelectList Options { get; set; }//for dropdown
        public Alignment Alignment { get; set; }//option: Left, Right, Center
        public string Width { get; set; }//e.g. 10% or 100px
        public string AutoCompleteURL { get; set; }
        public List<ListColumn> GridColumns { get; set; }
    }
}