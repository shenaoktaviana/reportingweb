﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingWeb.BaseModels
{
    public class SettingCustomPropertyModel
    {
        public string Name { get; set; }
        public string RenderPartialName { get; set; }
    }
}
