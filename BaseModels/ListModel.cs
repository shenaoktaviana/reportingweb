﻿using BusinessServiceFramework.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ReportingWeb.Models
{
    public class ListModel
    {
        public string Title { get; set; }
        public List<string> Styles { get; set; }
        public List<string> Scripts { get; set; }
        public string CustomFilterButtonsPartialName { get; set; }
        public List<ListFilterItem> Filters { get; set; }
        public SelectList SortOptions { get; set; }
        public bool CanInsert { get; set; }
        public bool CanActivate { get; set; }
        public bool CanDelete { get; set; }
        public bool CanExport { get; set; }
        public string CustomButtonsPartialName { get; set; }
        public bool HasSubData { get; set; }
        public string CustomSubDataPartialName { get; set; }
        public List<ListColumn> Columns { get; set; }

        public DataModel Data { get; set; }
    }
}