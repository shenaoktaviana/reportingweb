﻿using DomainModelFramework;

namespace ReportingWeb.Models
{
    public class ErrorModel
    {
        private string _errorMessage;
        private string _message;
        private HttpErrorCode? _code;
        public ErrorModel(string errorMessage, HttpErrorCode? code)
        {
            this._errorMessage = errorMessage;
            this._code = code;

            if (code.HasValue)
            {
                switch (code)
                {
                    case HttpErrorCode.BadRequest:
                        this._message = FrameworkResources.BadRequestDesc;
                        break;
                    case HttpErrorCode.Conflict:
                        this._message = FrameworkResources.ConflictDesc;
                        break;
                    case HttpErrorCode.Forbidden:
                        this._message = FrameworkResources.ForbiddenDesc;
                        break;
                    case HttpErrorCode.InternalServerError:
                        this._message = FrameworkResources.InternalServerErrorDesc;
                        break;
                    case HttpErrorCode.NotFound:
                        this._message = FrameworkResources.NotFoundDesc;
                        break;
                    case HttpErrorCode.Unauthorized:
                        this._message = FrameworkResources.UnauthorizedDesc;
                        break;
                    case HttpErrorCode.ServiceUnavailable:
                        this._message = FrameworkResources.ServiceUnavailableDesc;
                        break;

                }
            }
            else
            {
                this._message = FrameworkResources.GeneralErrorDesc;
            }
        }

        public string Message
        {
            get
            {
                return this._message;
            }
            private set { }
        }
        public string ErrorMessage
        {
            get
            {
                return this._errorMessage;
            }
            private set { }
        }
        public HttpErrorCode? Code
        {
            get
            {
                return this._code;
            }
            private set { }
        }

    }
}