﻿using BusinessServiceFramework.Models;
using DomainModelFramework;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ReportingWeb.Models
{
    public class ListFilterItem
    {
        public FilterOption FilterOption { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public ControlType ControlType { get; set; }
        public string ControlCSSClasses { get; set; } //space separated value
        public string CustomControlPartial { get; set; }
        public SelectList Options { get; set; }
        public string CSSClasses { get; set; } //space separated value
        public string Width { get; set; } // {0} %, {0} px
        public Alignment Alignment { get; set; }
        public string AutoCompleteURL { get; set; }
    }
}