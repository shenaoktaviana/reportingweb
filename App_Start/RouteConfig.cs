﻿using ReportingWeb.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReportingWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
              name: "LocalizedLanguageView",
              url: "{lang}/Languages/ViewLanguage/{id}",
              defaults: new
              {
                  controller = "Languages",
                  action = "View",
              },
              constraints: new
              {
                  id = @"\d+",
                  lang = new LanguageConstraint(),
              }
          );

            routes.MapRoute(
              name: "LocalizedLanguageInsert",
              url: "{lang}/Languages/InsertLanguage",
              defaults: new
              {
                  controller = "Languages",
                  action = "Detail",
                  id = UrlParameter.Optional
              },
              constraints: new
              {
                  lang = new LanguageConstraint(),
              }
          );

            routes.MapRoute(
                name: "LanguageEdit",
                url: "Languages/EditLanguage/{id}",
                defaults: new
                {
                    controller = "Languages",
                    action = "Detail"
                },
                constraints: new
                {
                    id = @"\d+"
                }
            );

            routes.MapRoute(
                name: "LanguageView",
                url: "Languages/ViewLanguage/{id}",
                defaults: new
                {
                    controller = "Languages",
                    action = "View"
                },
                constraints: new
                {
                    id = @"\d+"
                }
            );

            routes.MapRoute(
               name: "LanguageInsert",
               url: "Languages/InsertLanguage",
               defaults: new
               {
                   controller = "Languages",
                   action = "Detail",
                   id = UrlParameter.Optional
               }
           );

            routes.MapRoute(
              name: "LocalizedLogin",
              url: "{lang}/Account/Login",
              defaults: new
              {
                  controller = "Account",
                  action = "Login",
                  returnUrl = UrlParameter.Optional
              },
              constraints: new
              {
                  lang = new LanguageConstraint(),
              }
          );

            routes.MapRoute(
              name: "Login",
              url: "Account/Login",
              defaults: new
              {
                  controller = "Account",
                  action = "Login",
                  returnUrl = UrlParameter.Optional
              }
          );

            routes.MapRoute(
              name: "LocalizedHome",
              url: "{lang}/Home/Index",
              defaults: new
              {
                  controller = "Home",
                  action = "Index",
                  returnUrl = UrlParameter.Optional
              },
              constraints: new
              {
                  lang = new LanguageConstraint(),
              }
          );

            routes.MapRoute(
              name: "Home",
              url: "Home/Index",
              defaults: new
              {
                  controller = "Home",
                  action = "Index",
                  returnUrl = UrlParameter.Optional
              }
          );
           
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "DefaultLocalized",
                url: "{lang}/{controller}/{action}/{id}",
                constraints: new { lang = new LanguageConstraint() },
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                 name: "Default",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                name: "404-PageNotFound",
                // This will handle any non-existing urls
                url: "{*url}",
                // that handles all your custom errors
                defaults: new { controller = "Error", action = "Index" }
            );
        }
    }
}
