﻿using BusinessServiceFramework;
using ReportingBusinessService.Models;
using System.Web;
using System.Web.Hosting;
using System.Web.Optimization;

namespace ReportingWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            Info info = CommonUtilities.GetObjectFromXml(HostingEnvironment.MapPath("~/App_Data/Info.xml"), typeof(Info)) as Info;
            string theme = info.Theme;

#if RELEASE
            BundleTable.EnableOptimizations = true;
#elif DEBUG
            BundleTable.EnableOptimizations = false;
#endif

            bundles.Add(new StyleBundle("~/Content/css")
             .Include("~/Content/plugin/bootstrap.css",
                     "~/Content/plugin/bootstrap-ms.css",
                     "~/Content/site.css",
                     "~/Content/plugin/font-awesome.css",
                     "~/Content/flags.css",
                     "~/Content/themes/" + theme + "/layout.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui")
               .Include("~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css")
                .Include("~/Content/themes/base/jquery-ui.css")
                .Include("~/Content/themes/base/jquery-ui.structure.css")
                .Include("~/Content/themes/base/jquery.ui.core.css")
                .Include("~/Content/themes/base/jquery.ui.resizable.css")
                .Include("~/Content/themes/base/jquery.ui.selectable.css")
                .Include("~/Content/themes/base/jquery.ui.accordion.css")
                .Include("~/Content/themes/base/jquery.ui.autocomplete.css")
                .Include("~/Content/themes/base/jquery.ui.button.css")
                .Include("~/Content/themes/base/jquery.ui.dialog.css")
                .Include("~/Content/themes/base/jquery.ui.slider.css")
                .Include("~/Content/themes/base/jquery.ui.tabs.css")
                .Include("~/Content/themes/base/jquery.ui.datepicker.css")
                .Include("~/Content/themes/base/jquery.ui.progressbar.css")
                .Include("~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/datetimepicker/css")
              .Include("~/Content/themes/base/jquery.datetimepicker.css"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker")
               .Include("~/Scripts/plugin/jquery.datetimepicker.full.js"));

            bundles.Add(new StyleBundle("~/Content/chosen/css")
                .Include("~/Content/plugin/chosen.css"));

            bundles.Add(new ScriptBundle("~/bundles/chosen")
               .Include("~/Scripts/plugin/chosen.jquery.js"));

            bundles.Add(new StyleBundle("~/Content/admincss")
               .Include("~/Content/themes/" + theme + "/admin.css"));

            bundles.Add(new ScriptBundle("~/bundles/common")
              .Include("~/Scripts/common.js",
                        "~/Scripts/plugin/accounting.js",
                        "~/Scripts/helper.js",
                        "~/Scripts/plugin/jquery.dialog.js"));

            bundles.Add(new ScriptBundle("~/bundles/file-uploader-base")
               .Include("~/Scripts/plugin/jquery.fileupload.js")
               .Include("~/Scripts/plugin/jquery.fileupload-ui.js")
               .Include("~/Scripts/plugin/jquery.iframe-transport.js"));

            bundles.Add(new ScriptBundle("~/bundles/paging")
               .Include("~/Scripts/plugin/jquery.paging.js",
                        "~/Scripts/base.paging.js"));

            bundles.Add(new ScriptBundle("~/bundles/menuslider")
                .Include("~/Scripts/plugin/jquery.bxslider.js"));

            bundles.Add(new StyleBundle("~/Content/menuslidercss")
                .Include("~/Content/plugin/bxslider.css"));

            bundles.Add(new ScriptBundle("~/bundles/imageupload")
                .Include("~/Scripts/image-upload.js"));

            bundles.Add(new StyleBundle("~/Content/webgrid/css")
                .Include("~/Content/webgrid.css"));

            bundles.Add(new ScriptBundle("~/bundles/adminlist")
              .Include("~/Scripts/admin-list.js"));

            bundles.Add(new StyleBundle("~/Content/adminsettings")
               .Include("~/Content/admin-settings.css"));

            bundles.Add(new ScriptBundle("~/bundles/adminsettings")
                .Include("~/Scripts/admin-settings.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminrewardsettings")
                .Include("~/Scripts/admin-reward-settings.js"));

            bundles.Add(new ScriptBundle("~/bundles/login")
              .Include("~/Scripts/login.js"));

            bundles.Add(new StyleBundle("~/Content/login/css")
                .Include("~/Content/login.css"));

            bundles.Add(new ScriptBundle("~/bundles/register")
                .Include("~/Scripts/register.js"));

            bundles.Add(new StyleBundle("~/Content/register/css")
                .Include("~/Content/register.css"));

            bundles.Add(new ScriptBundle("~/bundles/forgotpassword")
              .Include("~/Scripts/forgot-password.js"));

            bundles.Add(new StyleBundle("~/Content/forgotpassword/css")
                .Include("~/Content/forgot-password.css"));

            bundles.Add(new ScriptBundle("~/bundles/resetpassword")
              .Include("~/Scripts/reset-password.js"));

            bundles.Add(new StyleBundle("~/Content/resetpassword/css")
                .Include("~/Content/reset-password.css"));

            bundles.Add(new StyleBundle("~/Content/error")
                .Include("~/Content/error.css"));

            bundles.Add(new ScriptBundle("~/bundles/error")
                .Include("~/Scripts/error.js"));

            bundles.Add(new ScriptBundle("~/bundles/languages")
                .Include("~/Scripts/languages.js"));

            bundles.Add(new StyleBundle("~/Content/languages/css")
                .Include("~/Content/languages.css"));

            bundles.Add(new ScriptBundle("~/bundles/language")
                .Include("~/Scripts/language.js"));

            bundles.Add(new StyleBundle("~/Content/language/css")
                .Include("~/Content/language.css"));

            bundles.Add(new ScriptBundle("~/bundles/publisher")
               .Include("~/Scripts/publisher.js"));

            bundles.Add(new ScriptBundle("~/bundles/permalink-handler")
               .Include("~/Scripts/permalink-handler.js"));

            bundles.Add(new StyleBundle("~/Content/home/css")
                .Include("~/Content/home.css"));

            bundles.Add(new ScriptBundle("~/bundles/home")
               .Include("~/Scripts/home.js"));

            bundles.Add(new ScriptBundle("~/bundles/layout")
               .Include("~/Scripts/layout.js"));
            
            bundles.Add(new StyleBundle("~/Content/common/css")
                .Include("~/Content/common.css"));
                
            bundles.Add(new StyleBundle("~/Content/reports/css")
                .Include("~/Content/reports.css"));

            bundles.Add(new ScriptBundle("~/bundles/weeklyreports")
               .Include("~/Scripts/weekly-reports.js"));

            bundles.Add(new ScriptBundle("~/bundles/memberreports")
               .Include("~/Scripts/member-reports.js"));

            bundles.Add(new ScriptBundle("~/bundles/dailyscrumreports")
               .Include("~/Scripts/daily-scrum-reports.js"));

            bundles.Add(new ScriptBundle("~/bundles/projectreports")
               .Include("~/Scripts/project-reports.js"));

            bundles.Add(new ScriptBundle("~/bundles/projecttask")
                .Include("~/Scripts/project-task.js"));

            bundles.Add(new ScriptBundle("~/bundles/projecttaskmember")
                .Include("~/Scripts/project-task-member.js"));

            bundles.Add(new ScriptBundle("~/bundles/projecttaskmembers")
                .Include("~/Scripts/project-task-members.js"));

            bundles.Add(new StyleBundle("~/Content/projecttaskmembers/css")
                .Include("~/Content/project-task-members.css"));

            bundles.Add(new ScriptBundle("~/bundles/dailyscrum")
                .Include("~/Scripts/daily-scrum.js"));
        }
    }
}
