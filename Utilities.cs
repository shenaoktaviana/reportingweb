﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ReportingWeb
{
    public static class Utilities
    {
        public static string GetColumnValue(this DataRow row, string columnName)
        {
            string colValue = "";
            if (row.Table.Columns.Contains(columnName) && row[columnName] != DBNull.Value)
                colValue = row[columnName].ToString();

            return colValue;
        }

        public static object GetRouteParameterValue(this HttpRequestBase request, string parameterName)
        {
            return request.RequestContext.RouteData.Values["id"];
        }

        public static string RenderPartialViewToString(Controller thisController, string viewName, object model)
        {
            return RenderPartialViewToString(thisController, viewName, model, thisController.ViewData);
        }

        public static string RenderPartialViewToString(Controller thisController, string viewName, object model, ViewDataDictionary viewData)
        {
            // assign the model of the controller from which this method was called to the instance of the passed controller (a new instance, by the way)
            viewData.Model = model;

            // initialize a string builder
            using (StringWriter sw = new StringWriter())
            {
                // find and load the view or partial view, pass it through the controller factory
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(thisController.ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(thisController.ControllerContext, viewResult.View, viewData, thisController.TempData, sw);

                // render it
                viewResult.View.Render(viewContext, sw);

                //return the razorized view/partial-view as a string
                return sw.ToString();
            }
        }

        //public static string RenderViewToString(string controllerName, string viewName, object model)
        //{
        //    PanoramaWeb.Controllers.FakeController fakeController = new PanoramaWeb.Controllers.FakeController();

        //    ViewDataDictionary viewData = fakeController.ViewData;
        //    viewData.Model = model;
        //    viewData.Add("IsFake", true);

        //    using (var writer = new StringWriter())
        //    {
        //        var routeData = new RouteData();
        //        routeData.Values.Add("controller", controllerName);
        //        var fakeControllerContext = new ControllerContext(new HttpContextWrapper(new HttpContext(new HttpRequest(null, Utilities.RootUrl(), null), new HttpResponse(null))), routeData, fakeController);
        //        var razorViewEngine = new RazorViewEngine();
        //        var razorViewResult = razorViewEngine.FindView(fakeControllerContext, viewName, "", false);

        //        var viewContext = new ViewContext(fakeControllerContext, razorViewResult.View, viewData, new TempDataDictionary(), writer);
        //        razorViewResult.View.Render(viewContext, writer);
        //        return writer.ToString();
        //    }
        //}

        public static int GetBSGridColumnWidth(int count)
        {
            return count > 12 ? 1 : (int)Math.Ceiling((double)12 / count);
        }

        public static string RootUrl()
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (!appRootUrl.EndsWith("/")) //a virtual
            {
                appRootUrl += "/";
            }

            appRootUrl = appRootUrl.Substring(0, appRootUrl.Length - 1);

            string rootPath = host + appRootUrl;

            return rootPath;
        }

        public static string GetHash(string signature)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(signature);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }

            return hashString;
        }

        public static string GetAlphabet(int index, int digitMax = 7)
        {
            string alphabets = "abcdefghijklmnopqrstuvwxyz";
            int baseAlphabets = alphabets.Count();

            if (index <= 0)
            {
                return alphabets.First().ToString();
            }
            else if (index <= baseAlphabets)
            {
                return alphabets.Select((alphabet, idx) => new { Alphabet = alphabet, Idx = idx }).First(x => x.Idx == index).Alphabet.ToString();
            }

            var sb = new StringBuilder().Append(' ', digitMax);
            int current = index;
            int offset = digitMax;
            while (current > 0)
            {
                sb[--offset] = alphabets[--current % baseAlphabets];
                current /= baseAlphabets;
            }
            return sb.ToString(offset, digitMax - offset);
        }
    }
}