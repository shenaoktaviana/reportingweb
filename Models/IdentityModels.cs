﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ReportingDomainModel;
using System;

namespace ReportingWeb.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() { }

        public ApplicationUser(User user)
        {
            this.Id = user.UserID.ToString();
            this.Email = user.Email;
            this.EmailConfirmed = true;
            this.PasswordHash = user.Password;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.LastName = string.Empty;
            this.PhoneNumber = user.Phone;
            this.Role = user.Role;
            this.LoginType = user.LoginType;
            this.IsActive = user.IsActive;
            this.SecurityStamp = user.Email + user.Password;
            this.FullName = user.FullName;
            this.UserName = user.Email;
        }

        public User User
        {
            get
            {
                return new User()
                {
                    UserID = long.Parse(this.Id),
                    Email = this.Email,
                    Password = this.PasswordHash,
                    FirstName = this.FirstName,
                    LastName = this.LastName,
                    Phone = this.PhoneNumber,
                    Role = this.Role,
                    LoginType = this.LoginType,
                    IsActive = true
                };
            }
            private set
            {
            }

        }

        public DateTime? DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string OtherPhone { get; set; }
        public string Role { get; set; }
        public string LoginType { get; set; }
        public bool IsActive { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}