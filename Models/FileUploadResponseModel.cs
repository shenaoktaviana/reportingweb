﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportingDomainModel;

namespace ReportingWeb.Models
{
    public class FileUploadResponseModel
    {
        public string ExtraData { get; set; }
        public string ImagePath { get; set; }
        public string Message { get; set; }
    }
}