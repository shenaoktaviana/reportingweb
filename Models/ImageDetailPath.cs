﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingWeb.Models
{
    public class ImageDetailPath
    {
        public int LanguageID { get; set; }
        public string FolderName { get; set; }
    }
}