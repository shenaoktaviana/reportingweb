﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingWeb.Models
{
    public class IWTPayload
    {
        [JsonProperty(PropertyName = "iss")]
        public string Issuer { get; set; }

        [JsonProperty(PropertyName = "aud")]
        public string Audience { get; set; }

        [JsonProperty(PropertyName = "exp")]
        public long ExpiredTime { get; set; }

        [JsonProperty(PropertyName = "usrid")]
        public long UserID { get; set; }
    }
}