﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingWeb.Models
{
    public class IWTHeader
    {
        [JsonProperty(PropertyName = "alg")]
        public string Algorithm { get; set; }

        [JsonProperty(PropertyName = "typ")]
        public string Type { get; set; }
    }
}