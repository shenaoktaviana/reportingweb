﻿using DomainModelFramework;
using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReportingWeb.Models
{
    public class UserModel
    {
        public User User { get; set; }

        [Display(ResourceType = typeof(FrameworkResources), Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(FrameworkResources), Name = "ConfirmPassword")]
        [Compare("Password", ErrorMessage = null, ErrorMessageResourceName = "PasswordDontMatch", ErrorMessageResourceType = typeof(FrameworkResources))]
        public string ConfirmPassword { get; set; }
    }
}