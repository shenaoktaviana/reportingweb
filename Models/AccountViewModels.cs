﻿using DomainModelFramework;
using ReportingResources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReportingWeb.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Display(ResourceType = typeof(FrameworkResources), Name = "Email")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(FrameworkResources), Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(ResourceType = typeof(FrameworkResources), Name = "RememberMe")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Display(ResourceType = typeof(FrameworkResources), Name = "Email")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "FieldInvalid", ErrorMessageResourceType = typeof(FrameworkResources))]
        [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        public string Email { get; set; }

        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength =8)]
        [RegularExpression("^(?=.*\\d)(?=.*[a-zA-Z])(?=.*[\\W_]).{8,}$", ErrorMessageResourceType = typeof(ErrorResources), ErrorMessage = null, ErrorMessageResourceName = "PasswordCriteria")]
        [Display(ResourceType = typeof(FrameworkResources), Name = "Password")]
        [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(ResourceType = typeof(FrameworkResources), Name = "ConfirmPassword")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Display(ResourceType = typeof(FrameworkResources), Name = "Name")]
        [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        public string FirstName { get; set; }

        [Display(ResourceType = typeof(FrameworkResources), Name = "Name")]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(FrameworkResources), Name = "Phone")]
        [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        public string Phone { get; set; }

        //[Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        //[Range(typeof(bool), "true", "true", ErrorMessage = "You must agree to the Terms and Conditions.")]
        public bool AgreeTC { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Display(ResourceType = typeof(FrameworkResources), Name = "Email")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "FieldInvalid", ErrorMessageResourceType = typeof(FrameworkResources))]
        [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        public string Email { get; set; }

        [Display(ResourceType = typeof(FrameworkResources), Name = "Password")]
        [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(FrameworkResources), Name = "ConfirmPassword")]
        [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        [Compare("Password", ErrorMessage = null, ErrorMessageResourceName = "PasswordDontMatch", ErrorMessageResourceType = typeof(FrameworkResources))]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Display(ResourceType = typeof(FrameworkResources), Name = "Email")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "FieldInvalid", ErrorMessageResourceType = typeof(FrameworkResources))]
        [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
        public string Email { get; set; }
    }
}
